//
//  CommonMethods.swift
//  SchoolEasy
//
//  Created by W@rrior on 20/11/15.
//  Copyright © 2015 Organization. All rights reserved.
//

import UIKit
import ReachabilitySwift
import SwiftLoader

public class CommonMethods: NSObject, QBRTCClientDelegate, IncomingCallViewControllerDelegate {
    static let sharedInstance: CommonMethods = {
        let instance = CommonMethods()
        instance.initialize()
        return instance
    }()
    
    var isConnected : Bool = false
    var isLoggedIn  : Bool = false
    
    var reachability: Reachability!
    var config      : SwiftLoader.Config = SwiftLoader.Config()
    var userModel   : User? = nil
    var qb_user     : QBUUser? = nil
    
    var dataSource  : UsersDataSource!
    var session     : QBRTCSession?
    
    
    var usrState    :Int = 0
    var referralCode:String = ""
    
    func initialize () {
        setUpReachability()
        QBRTCClient.instance().addDelegate(self)
    }
    
    func setUpSwiftLoader () {
        config.size = 120
        config.titleTextColor = .clearColor()
        config.backgroundColor = .clearColor()
        config.spinnerLineWidth = 3.0
        config.spinnerColor = BLUE_COLOR
        config.foregroundColor = .blackColor()
        config.foregroundAlpha = 0.5
        SwiftLoader.setConfig(config)
    }
    
    // MARK: - Get UserID To Call Receiver
    
    func getReciverDetailForClient(session: QBRTCSession) {
        let clientID = session.initiatorID
        self.getClientEmailWithID(clientID, session: session)
    }
    
    func getClientEmailWithID(intId : NSNumber, session: QBRTCSession) {
        let intIdValue = UInt(intId)
        QBRequest.userWithID(intIdValue, successBlock: {(response, detail) in
            print(detail?.login)
            self.getClientEmailDetail((detail?.login)!, session: session)
        }) { (error) in
            print("Error Message")
        }
    }
    
    func getClientEmailDetail(usersClientEmail : String, session: QBRTCSession) {
        self.setData(CommonMethods.sharedInstance.qb_user!, usersClientEmail: usersClientEmail, session: session)
    }
    
    func setData(loginUserDetails : QBUUser,usersClientEmail : String, session: QBRTCSession) {
        QBRequest.logInWithUserLogin(usersClientEmail, password: KQB_USER_DEFAULT_PASSWORD,
                                     successBlock: { (response, loginReceiverDetail) in
                                        print("login receiver sccess")
                                        print(loginReceiverDetail?.login);
                                        print(loginReceiverDetail?.fullName);
                                        self.dataSource = UsersDataSource.init(currentUser: loginReceiverDetail!)
                                        let usersArray : [QBUUser] = [loginUserDetails, loginReceiverDetail!]
                                        self.dataSource.setUsers(usersArray)
                                        self.dataSource.selectUserForCall(loginReceiverDetail!)
                                        CommonMethods.sharedInstance.showHudWithTitle(STR_LOADING)
                                        CommonMethods.sharedInstance.hideHud()
                                        self.callManuallyDelegate(session)
            }, errorBlock: { (error) in
                print("error")
        })
    }
    
    func callManuallyDelegate(session: QBRTCSession) {
        QBRTCAudioSession.instance().initializeWithConfigurationBlock({(configuration: QBRTCAudioSessionConfiguration) -> Void in
            // adding bluetooth and airplay support
            if #available(iOS 10.0, *) {
                configuration.categoryOptions = [.AllowBluetooth, .AllowBluetoothA2DP, .AllowAirPlay]
            } else {
                configuration.categoryOptions = [configuration.categoryOptions, AVAudioSessionCategoryOptions.AllowBluetooth]
            }
            
            if session.conferenceType == QBRTCConferenceType.Video {
                // setting mode to video chat to enable airplay audio and speaker only
                configuration.mode = AVAudioSessionModeVideoChat
            }
        })
        let ctrl = IncomingCallViewController(nibName: "IncomingCallViewController", bundle : nil)
        ctrl.delegate = self
        ctrl.session = session
        ctrl.usersDatasource = self.dataSource
        appDel!.navCtrl!.pushViewController(ctrl, animated: true)
    }
    
    // MARK: - QBWebRTCChatDelegate
    
    public func didReceiveNewSession(session: QBRTCSession, userInfo: [String : String]?) {
        if (self.session != nil) {
            session.rejectCall(["reject": "busy"])
            return
        }
        self.session = session
        self.getReciverDetailForClient(session)
    }
    
    public func sessionDidClose(session: QBRTCSession) {
        if session == self.session {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(UInt64(1.5) * NSEC_PER_SEC)), dispatch_get_main_queue(), {
                self.session = nil
                print(self.session)
                appDel!.navCtrl!.view.userInteractionEnabled = true
                appDel?.connectUserWithQuickbloxChat()
                appDel!.navCtrl!.popViewControllerAnimated(true)
                CommonMethods.sharedInstance.hideHud()
            })
        }
    }
    
    public func incomingCallViewController(vc: IncomingCallViewController, didAcceptSession session: QBRTCSession) {
        let storyboard = UIStoryboard(name: "Call", bundle: nil)
        let ctrl = storyboard.instantiateViewControllerWithIdentifier("CallViewController") as! CallViewController
        ctrl.session = session
        ctrl.usersDatasource = self.dataSource
        ctrl.loginUserDetails = CommonMethods.sharedInstance.qb_user!
        let nav = UINavigationController(rootViewController: ctrl)
        nav.modalTransitionStyle = .CrossDissolve
        CommonMethods.sharedInstance.hideHud()
        appDel?.connectUserWithQuickbloxChat()
        appDel!.navCtrl!.presentViewController(nav, animated: false) {
            appDel!.navCtrl!.popViewControllerAnimated(true)
        }
    }
    
    public func incomingCallViewController(vc: IncomingCallViewController, didRejectSession session: QBRTCSession) {
        session.rejectCall(nil)
        appDel?.connectUserWithQuickbloxChat()
        CommonMethods.sharedInstance.hideHud()
    }
    
    func saveUser() {
        let data = NSKeyedArchiver.archivedDataWithRootObject(userModel!)
        NSUserDefaults.standardUserDefaults().setObject(data, forKey: kUserKey)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    func loadUser() -> User {
        if let data = NSUserDefaults.standardUserDefaults().objectForKey(kUserKey) as? NSData {
            userModel = NSKeyedUnarchiver.unarchiveObjectWithData(data) as? User
        }
        return userModel!
    }
    
    func setUserState(currentState: Int) {
        switch currentState {
        case USER_STATE.NOT_LOGGED_IN.rawValue:
            isLoggedIn = false
            userModel = nil
            break
        case USER_STATE.LOGGED_IN.rawValue:
            isLoggedIn = true
            break
        default:
            print("default")
        }
        NSUserDefaults.standardUserDefaults().setInteger(currentState, forKey: kUserState)
        NSUserDefaults.standardUserDefaults().synchronize()
        usrState = currentState
    }
    
    func nsDateFromString(string: String, usingDateFormat dateFormat: String) -> NSDate {
        var formatter: NSDateFormatter? = nil
        if formatter == nil {
            formatter = NSDateFormatter()
        }
        formatter!.dateFormat = dateFormat
        let date: NSDate = formatter!.dateFromString(string)!
        return date
    }
    
    func nsStringFromDate(date: NSDate, andToFormatString formatString: String) -> String {
        var formatter: NSDateFormatter? = nil
        if formatter == nil {
            formatter = NSDateFormatter()
        }
        formatter!.dateFormat = formatString
        let dateString: String = formatter!.stringFromDate(date)
        return dateString
    }
    
    // MARK: - Reachability Setup
    
    func setUpReachability () {
        do {
            reachability = try Reachability.reachabilityForInternetConnection()
        } catch {
            print("Unable to create Reachability")
            return
        }
        NSNotificationCenter.defaultCenter().addObserver(self,
                                                         selector: #selector(CommonMethods.reachabilityChanged(_:)),
                                                         name: ReachabilityChangedNotification,
                                                         object: reachability)
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    func reachabilityChanged(note: NSNotification) {
        
        reachability = note.object as! Reachability
        
        if reachability.isReachable() {
            if reachability.isReachableViaWiFi() {
                isConnected = true
                print("Reachable via WiFi")
            } else {
                isConnected = true
                print("Reachable via Cellular")
            }
        } else {
            isConnected = false
            print("Not reachable")
        }
    }
    
    func showHudWithTitle(titleStr:String) {
        SwiftLoader.show(title: titleStr, animated: true)
    }
    
    func hideHud() {
        SwiftLoader.hide()
    }
    
    func printAllFonts() {
        let fontFamilies:NSArray = UIFont.familyNames()
        
        for i in 0 ..< fontFamilies.count {
            let fontFamily: NSString = fontFamilies.objectAtIndex(i) as! NSString;
            let fontNames: NSArray = UIFont.fontNamesForFamilyName(fontFamilies.objectAtIndex(i) as! String)
            print("\(fontFamily)  \(fontNames)");
        }
    }
    
    func showAlertWithMessage(message: String, withController viewCtr: UIViewController) {
        
        let  alertController = UIAlertController(title: APP_NAME, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            print("Handle Ok logic here")
        }))
        
        viewCtr.presentViewController(alertController, animated: true, completion: nil)
    }
    
    func JSONStringify(value: AnyObject,prettyPrinted:Bool = false) -> String{
        let options = prettyPrinted ? NSJSONWritingOptions.PrettyPrinted : NSJSONWritingOptions(rawValue: 0)
        if NSJSONSerialization.isValidJSONObject(value) {
            do {
                let data = try NSJSONSerialization.dataWithJSONObject(value, options: options)
                if let string = NSString(data: data, encoding: NSUTF8StringEncoding) {
                    return string as String
                }
            } catch {
                print("error")
                //Access error here
            }
        }
        return ""
    }
    
    func popToLast(aClass: AnyClass, fromNavigationController navController: UINavigationController, animated: Bool) {
        for index in (0 ..< navController.viewControllers.count).reverse() {
            let vc: UIViewController = navController.viewControllers[index]
            if (vc.isKindOfClass(aClass)) {
                navController.popToViewController(vc, animated: animated)
            }
        }
    }
}
