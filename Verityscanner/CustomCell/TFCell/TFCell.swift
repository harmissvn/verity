//
//  TFCell.swift
//  Verityscanner
//
//  Created by Hardik Dineshchandra Mistry on 26/09/16.
//  Copyright © 2016 certified. All rights reserved.
//

import UIKit
import JVFloatLabeledTextField

class TFCell: UITableViewCell {
    
    @IBOutlet weak var textField: JVFloatLabeledTextField!
    @IBOutlet weak var constRightPadding: NSLayoutConstraint!
    @IBOutlet weak var constLeftPadding: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.textField.floatingLabelFont = kSystemBoldFonts(10.0)
        self.textField.floatingLabelYPadding = 3.0
        
        self.textField.layer.cornerRadius = 5.0
        self.textField.layer.borderColor = UIColor.darkGrayColor().CGColor
        self.textField.layer.borderWidth = 1.0
        //self.textField.addLeftPadding()
        
//        self.textField.layer.masksToBounds = false
//        self.textField.layer.shadowRadius = 3.0
//        self.textField.layer.shadowColor = UIColor.blackColor().CGColor
//        self.textField.layer.shadowOffset = CGSizeMake(1.0, 1.0)
//        self.textField.layer.shadowOpacity = 1.0
    }

}
