//
//  CertificationCell.swift
//  Verityscanner
//
//  Created by Hardik Dineshchandra Mistry on 14/09/16.
//  Copyright © 2016 certified. All rights reserved.
//

import UIKit

class CertificationCell: UITableViewCell {

    @IBOutlet weak var lblPlaceholder: UILabel!
    @IBOutlet weak var imgCertificate: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
