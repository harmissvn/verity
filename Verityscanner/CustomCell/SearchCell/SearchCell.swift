//
//  SearchCell.swift
//  Verityscanner
//
//  Created by Hardik Dineshchandra Mistry on 12/09/16.
//  Copyright © 2016 certified. All rights reserved.
//

import UIKit

class SearchCell: UITableViewCell {

    @IBOutlet weak var lblPlaceholder: UILabel!
    @IBOutlet weak var tvValue: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //Initialization code
        self.lblPlaceholder.font = kSystemRegularFonts(17.0)
        self.tvValue.font = kSystemRegularFonts(17.0)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.contentView.setNeedsLayout()
        self.contentView.layoutIfNeeded()
        self.lblPlaceholder.preferredMaxLayoutWidth = CGRectGetWidth(self.lblPlaceholder.frame)
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
