//
//  ReviewCell.swift
//  Verityscanner
//
//  Created by harmis on 25/02/17.
//  Copyright © 2017 certified. All rights reserved.
//

import UIKit
import Cosmos
class ReviewCell: UITableViewCell {

    @IBOutlet var lblDesc: UILabel!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblname: UILabel!
    @IBOutlet var lbldate: UILabel!
    @IBOutlet var viewRating: CosmosView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
