//
//  HomeCell.swift
//  SchoolEasy
//
//  Created by W@rrior on 19/11/15.
//  Copyright © 2015 Organization. All rights reserved.
//

import UIKit

class HomeCell: UICollectionViewCell {
  
    @IBOutlet var contView : UIView!
    @IBOutlet var lblTitle : UILabel!
    @IBOutlet var imgView  : UIImageView!
    
    override func awakeFromNib() {
        
    }
}
