//
//  ProductCell.swift
//  Verityscanner
//
//  Created by Hardik Dineshchandra Mistry on 01/09/16.
//  Copyright © 2016 certified. All rights reserved.
//

import UIKit

class ProductCell: UITableViewCell {

    @IBOutlet weak var viewContainer: UIView!
    @IBOutlet weak var lblUPCCode: UILabel!
    @IBOutlet weak var lblProductName: UILabel!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var imgProduct: UIImageView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        viewContainer.layer.cornerRadius = 5.0
        imgProduct.layer.cornerRadius = 5.0
        
        self.lblUPCCode.font = kSystemRegularFonts(17.0)
        self.lblProductName.font = kSystemRegularFonts(16.0)
        self.lblCategory.font = kSystemRegularFonts(16.0)
        
        /*imgProduct.layer.shadowColor = UIColor.blackColor().CGColor
        imgProduct.layer.shadowOffset = CGSizeMake(0, 1)
        imgProduct.layer.shadowOpacity = 1
        imgProduct.layer.shadowRadius = 1.0
        imgProduct.clipsToBounds = false
        imgProduct.layer.borderWidth = 1.0;
        imgProduct.layer.borderColor = UIColor.whiteColor().CGColor
        */
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.contentView.setNeedsLayout()
        self.contentView.layoutIfNeeded()
        self.lblUPCCode.preferredMaxLayoutWidth = CGRectGetWidth(self.lblUPCCode.frame)
        self.lblProductName.preferredMaxLayoutWidth = CGRectGetWidth(self.lblProductName.frame)
        self.lblCategory.preferredMaxLayoutWidth = CGRectGetWidth(self.lblCategory.frame)
    }

    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
    
}
