//
//  USDACell.swift
//  Verityscanner
//
//  Created by Chinkal on 5/19/17.
//  Copyright © 2017 certified. All rights reserved.
//

import UIKit

class USDACell: UITableViewCell {

    @IBOutlet var lblTitle  : UILabel!
    @IBOutlet var lblDesc   : UILabel!
    @IBOutlet var lblDate   : UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}
