//
//  SocialCell.swift
//  Verityscanner
//
//  Created by Hardik Dineshchandra Mistry on 14/10/16.
//  Copyright © 2016 certified. All rights reserved.
//

import UIKit

class SocialCell: UICollectionReusableView {

    @IBOutlet weak var btnSocial        : UIButton!
    @IBOutlet var btnUSDARecalls        : UIButton!
    
    @IBOutlet weak var imgSocial        : UIImageView!
    @IBOutlet var imgUSDARecalls        : UIImageView!
    
    @IBOutlet weak var imgNotification  : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
