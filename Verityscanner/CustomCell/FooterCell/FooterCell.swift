//
//  FooterCell.swift
//  Verityscanner
//
//  Created by harmis on 25/07/17.
//  Copyright © 2017 certified. All rights reserved.
//

import UIKit

class FooterCell: UICollectionViewCell {
    
    @IBOutlet var contView: UIView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var imgView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
