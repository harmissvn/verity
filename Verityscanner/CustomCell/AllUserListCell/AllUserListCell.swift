//
//  AllUserListCell.swift
//  Verityscanner
//
//  Created by harmis on 28/07/17.
//  Copyright © 2017 certified. All rights reserved.
//

import UIKit

class AllUserListCell: UITableViewCell {
    
    @IBOutlet var      lblName    : UILabel!
    @IBOutlet weak var imgView    : UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let userImage = UIImage.fontAwesomeIconWithCode("fa-user", textColor: UIColor.blackColor(), size: CGSize(width: 30, height: 30), backgroundColor: UIColor.clearColor())
        self.imgView.image = userImage
        self.imgView.layer.cornerRadius = self.imgView.frame.size.width/2
        self.imgView.layer.borderWidth  = 1.0
        self.imgView.layer.borderColor = UIColor.blackColor().CGColor
    }
}
