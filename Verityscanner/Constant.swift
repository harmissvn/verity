//
//  Constant.swift
//  SchoolEasy
//
//  Created by Wrrior on 18/11/15.
//  Copyright © 2015 Organization. All rights reserved.
//

import Foundation
import UIKit

//Screen Constants 

let SCREEN_WIDTH = UIScreen.mainScreen().bounds.size.width
let SCREEN_HEIGHT = UIScreen.mainScreen().bounds.size.height
let SCREEN_MAX_LENGTH = max(SCREEN_WIDTH, SCREEN_HEIGHT)
let SCREEN_MIN_LENGTH = min(SCREEN_WIDTH, SCREEN_HEIGHT)

let IS_IPHONE_4_OR_LESS =  UIDevice.currentDevice().userInterfaceIdiom == .Phone && SCREEN_MAX_LENGTH < 568.0
let IS_IPHONE_5 = UIDevice.currentDevice().userInterfaceIdiom == .Phone && SCREEN_MAX_LENGTH == 568.0
let IS_IPHONE_6 = UIDevice.currentDevice().userInterfaceIdiom == .Phone && SCREEN_MAX_LENGTH == 667.0
let IS_IPHONE_6P = UIDevice.currentDevice().userInterfaceIdiom == .Phone && SCREEN_MAX_LENGTH == 736.0


// Actual keywords which we search in Google Places Api to determine the nearest location
let strKroger           = "kroger"
let strCostco           = "costco%20wholesale"
let strPublix           = "publix"
let strWawa             = "wawa"
let strWalmart          = "walmart"
let strTrader           = "trader%20joe"
let strThefresh         = "the%20fresh%20market"
let strWholefoods       = "whole%20foods"
let strStartBucks       = "startbucks"
let strChipotle         = "chipotle%20grill"
let strAlfalfas         = "alfalfas"
let strCosmoProf        = "cosmoprof"
let strCvsPharmacy      = "cvs%20pharmacy"
let strDunkinDonuts     = "dunkin%20donuts"
let strGNCLiveWell      = "gnc%20live%20well"
let strPaneraBread      = "panera%20bread"
let strPetSupermarket   = "pet%20supermarket"
let strSaloncentric     = "salon%20centric"
let strWalgreens        = "walgreens"

// Auto scan key used to store Auto Scan On/Off value
let AUTOSCAN            = "AUTOSCAN"

let IS_IPAD = UIDevice.currentDevice().userInterfaceIdiom == .Pad
let IS_IPHONE = UIDevice.currentDevice().userInterfaceIdiom == .Phone

// Universal App name
let APP_NAME = "Verity Scanning"

// Login keys to manage user's login session
let kUserState = "userState"
let kUserKey = "loggedInUser"

// Generic constants for application
let NEW_DATE_FORMAT             = "dd-MM-yyyy"
let DATE_FORMAT                 = "MM-dd-yyyy" //2015-12-03
let MMYYYY_FORMAT               = "MM/yyyy" //01/2016
let GRADE_DATE_FORMAT           = "dd, MMMM yyyy" //15, Nov 2015
let CHECK_IN_DATE_FORMAT        = "YYYY-MM-dd HH:mm:ss" //2015-09-04 07:43:16
let CHECK_IN_TIME_FORMAT        = "hh:mma"
let SEM_DATE_FORMAT             = "MM/dd/yyyy"
let WRIST_TIME_FORMAT           = "hh:mm a"
let PICKER_DATE_FORMAT          = "MMMM dd, yyyy, HH:mm a"
let REGISTERED_DATE_FORMAT      = "MM/dd"
let REGISTERED_TIME_FORMAT      = "hh:mma"
let TIME_FORMAT                 = "hh:mm a"

let STR_OK              = "Ok"
let STR_LOADING         = "Loading..."
let SUCCESS             = "success"
let MESSAGE             = "message"
let INVALID_RESPONSE    = "Invalid response from server."
let NO_INTERNET         = "You're not connected to the internet.\nPlease connect and retry."
let NO_RESPONSE         = "No Response From Server."

// Firebase events name and description
let SCAN_BUTTON_EVENT = "Scan_button_analytics"
let SEARCH_BUTTON_EVENT = "Search_button_analytics"
let ABOUT_US_BUTTON_EVENT = "About_us_button_analytics"
let SCANING_EVENT = "Scaninng_analytics"

let FB_LOGIN_EVENT = "Facebook_login_analytics"
let GOOGLE_LOGIN_EVENT = "Google_login_analytics"
let TW_LOGIN_EVENT = "Twitter_login_analytics"

let SCAN_BUTTON_EVENT_NAME = "Scan button analytics"
let SEARCH_BUTTON_EVENT_NAME = "Search button analytics"
let ABOUT_BUTTON_EVENT_NAME = "About us button analytics"
let SCANING_EVENT_NAME = "Scaninng analytics"

let FB_LOGIN_EVENT_NAME = "Facebook login analytics"
let GOOGLE_LOGIN_EVENT_NAME = "Google login analytics"
let TW_LOGIN_EVENT_NAME = "Twitter login analytics"

let appDel = UIApplication.sharedApplication().delegate as? AppDelegate

// Notifications Name
let kLoginNotification = "LoginNotification"

// Google Ad-Mob
let AD_MOB_APP_ID   = "ca-app-pub-6103044640428504~2495270871"
let AD_MOB_UNIT_ID  = "ca-app-pub-6103044640428504/3108779275"

//let TERMS_URL = "https://www.certified.bz/index.php/faq3/terms-and-conditions"
let PRIVACY_POLICY_URL = "https://www.iubenda.com/privacy-policy/7766075"
let TERMS_URL = "https://s3-us-west-1.amazonaws.com/verity.app/TermsAndConditions.html"

//let GOOGLE_PLACE_KEY = "AIzaSyCCHSZnG9BfX_xWA5_qqtRyQEocFRmimIU"
let GOOGLE_PLACE_KEY = "AIzaSyBHMVfcKqZC0gQky3zgP5r6fEUWo9bEBcc"
let DISTANCE_METERS = "100"

let ANDROID_CLIENT_ID = "874376514949-ggbaeujlgb0g6rhijno7db2aadh0kf51.apps.googleusercontent.com"
//let ANDROID_CLIENT_ID="874376514949-s8o7q0ve7k0gft81jnd1lbo9n94gj28c.apps.googleusercontent.com"

// Color
func RGB(red:CGFloat, green:CGFloat, blue:CGFloat, alpha:CGFloat) -> UIColor {
    return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
}

let BLUE_COLOR = RGB(0, green: 187, blue: 249, alpha: 1)
let GREEN_COLOR = RGB(38, green: 139, blue: 0, alpha: 1)
let TAB_BLUE_COLOR = RGB(83, green: 183, blue: 244, alpha: 1)
let TOP_BLUE_COLOR = RGB(84, green: 184, blue: 244, alpha: 1)

// UIImage Macros
func IMAGE_NAMED(imgName:String) -> UIImage {
    return UIImage(named: imgName)!
}

// Automatic fonts calculation based on device type
let FONTS_6_PLUS = (IS_IPHONE_6P) ? 1.1 : 1
let IPAD_FONTS = ((IS_IPAD) ? 1.5 : 1)

func kSystemRegularFonts(fontSize:CGFloat) -> UIFont {
    return UIFont.systemFontOfSize(fontSize * CGFloat(FONTS_6_PLUS) * CGFloat(IPAD_FONTS))
}

func kSystemBoldFonts(fontSize:CGFloat) -> UIFont {
    return UIFont.boldSystemFontOfSize(fontSize * CGFloat(FONTS_6_PLUS) * CGFloat(IPAD_FONTS))
}

//let kRegularFont(fontSize) [UIFont fontWithName:kUbuntuRegularName size: (fontSize * FONTS_6_PLUS * IPAD_FONTS)]
//let kLightFont(fontSize) [UIFont fontWithName:kUbuntuLightName size: (fontSize * FONTS_6_PLUS * IPAD_FONTS)]
//let kMediumFont(fontSize) [UIFont fontWithName:kUbuntuMediumName size: (fontSize * FONTS_6_PLUS * IPAD_FONTS)]
//let kCustomFont(fontName, fontSize) [UIFont fontWithName: fontName size: (fontSize * FONTS_6_PLUS * IPAD_FONTS)]

enum USER_STATE : Int {
    case NOT_LOGGED_IN = 0
    case LOGGED_IN = 1
}

// QuickBlox Password

let KQB_USER_DEFAULT_PASSWORD = "123456789"

class Constants {
    
    class var QB_USERS_ENVIROMENT: String {
        
        #if DEBUG
            return "Verity"
        #elseif QA
            return "qbqa"
        #else
            assert(false, "Not supported build configuration")
            return ""
        #endif
        
    }
}


let kChatPresenceTimeInterval:NSTimeInterval = 45
let kDialogsPageLimit:UInt = 100
let kMessageContainerWidthPadding:CGFloat = 40.0

// QuickBlox Details

let kAPPLICATION_ID = 60556
let kAUTH_KEY       = "wHFNW3rTDVDAHbv"
let kAUTH_SECRET    = "zDw33GJcs2fGKm-"
let kACCOUNT_KEY    = "iaKpK7oKiNgtwtCs2zsc"


//let kAPPLICATION_ID = 56466
//let kAUTH_KEY       = "Gzb-yTjycgBCgfz"
//let kAUTH_SECRET    = "Lu4nVHq6Etnz464"
//let kACCOUNT_KEY    = "mH2FVK5yjnAi47mgsfEu"



// QuickBlox Call And Video

let KQB_RING_THICK_NESS              : CGFloat        = 1.0   // kQBRingThickness: CGFloat = 1.0
let KQB_ANSWER_TIME_INTERVAL         : NSTimeInterval = 60.0  // let kQBAnswerTimeInterval = 60.0
let KQB_RTC_DISCONNECT_TIME_INTERVAL : NSTimeInterval = 30.0  // let kQBRTCDisconnectTimeInterval = 30.0
let KQB_DIALING_TIME_INTERVAL        : NSTimeInterval = 5.0  // let kQBDialingTimeInterval = 5.0

var COUNTRY_CODE            = "countryCode"
var COUNTRY_NAME            = "countryName"
var COUNTRY_LANGUAGE        = "countrylanguage"
let CONFIRM_ONLINE          = "CONFIRM_ONLINE"

//Live URL
let MAIN_URL = "https://t.certified.bz/index.php?option=com_jewebservices&task="

let DYNAMIC                 = MAIN_URL + "dynamic"
let LOGIN                   = MAIN_URL + "login"
let REGISTER                = MAIN_URL + "register"
let INVITES                 = MAIN_URL + "invite"
let REPORT                  = MAIN_URL + "contact"
let ADVERTISMENT            = MAIN_URL + "advertisement"
let FORGOT_PASSWORD         = MAIN_URL + "forget_password"

let ADD_NEW_PRODUCT_URL     = MAIN_URL + "addNewProduct"
let GIVE_RATING             = MAIN_URL + "giveRating"
let REVIEW_LIST_URL         = MAIN_URL + "reviewListing"
let GIVE_REVIEW_URL         = MAIN_URL + "giveReview"
let SEARCH_PRODUCT_URL      = MAIN_URL + "search"
let SIGNIN_URL              = MAIN_URL + "signin"
let FORGOT_PASS_URL         = MAIN_URL + "forgotPassword"
let LOGIN_URL               = MAIN_URL + "normalLogin"

let CHECK_PRODUCT_URL       = "https://t.certified.bz/search_product_status.php?searchword="

let UPDATE_URL              = MAIN_URL + "updateProfile"
let GET_RSS_FEED_URL        = MAIN_URL + "getRssFeed"

//getRssFeed
