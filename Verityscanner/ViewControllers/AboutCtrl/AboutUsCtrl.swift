//
//  AboutUsCtrl.swift
//  Verityscanner
//
//  Created by Hardik Dineshchandra Mistry on 31/08/16.
//  Copyright © 2016 certified. All rights reserved.
// #3BB9F9 , backgroundColor , #ffffff

import UIKit
import Firebase

class AboutUsCtrl: UIViewController {
    
    @IBOutlet weak var navView   : UIView!
    @IBOutlet weak var viewHeader: UIView!
    @IBOutlet var viewHeaderIpad : UIView!
    
    @IBOutlet var txtViewCertifiedIpad: UITextView!
    @IBOutlet var txtViewAddress1Ipad : UITextView!
    @IBOutlet var txtViewMapIpad      : UITextView!
    @IBOutlet var txtViewPhoneIpad    : UITextView!
    @IBOutlet var txtViewBzIpad       : UITextView!
    @IBOutlet var txtViewDetailsIpad  : UITextView!
    
    @IBOutlet var txtViewAddress2Ipad: UITextView!
    @IBOutlet weak var txtViewCertified: UITextView!
    @IBOutlet weak var txtViewAddress1 : UITextView!
    @IBOutlet weak var txtViewAddress2 : UITextView!
    @IBOutlet weak var txtViewMap      : UITextView!
    @IBOutlet weak var txtViewPhone    : UITextView!
    @IBOutlet weak var txtViewBz       : UITextView!
    @IBOutlet weak var txtViewDetails  : UITextView!
    
    @IBOutlet weak var tblAbout  : UITableView!
    @IBOutlet weak var btnBack   : UIButton!
    @IBOutlet weak var lblTitle  : UILabel!
    @IBOutlet var lblTitleIpad   : UILabel!
    
    var strBackLogo : String = ""
    var rowsArray = [Row]()
    
    let PRIVACY_POLICY         = "Privacy Policy"
    let TERMS_OF               = "Terms of Service"
    
    var txtColorValue : String = ""
    
    // MARK: - UIView Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setFireBase()
        setFirebaseStorage()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        sizeHeaderToFit()
    }
    
    // MARK: - All Functions
    
    func setFirebaseStorage() {
        let storageRef = FIRStorage.storage().reference()
        if IS_IPAD {
            self.strBackLogo = "Verity/Common/back@2x.png"
        } else {
            self.strBackLogo = "Verity/Common/back.png"
        }
        //For HomeLogo
        let homeLogoReference = storageRef.child(self.strBackLogo)
        homeLogoReference.dataWithMaxSize((1 * 1024 * 1024)) { (data, error) in
            if error == nil {
                self.btnBack.imageView?.contentMode = UIViewContentMode.ScaleAspectFit
                self.btnBack.setImage(UIImage(data: data!), forState: .Normal)
            } else {
                print(error?.localizedDescription)
            }
        }
    }
    
    func setFireBase() {
        self.setupDefaultConfig()
        self.fetchRemoteConfig()
    }
    
    func hexStringToUIColor(hex:String) -> UIColor {
        var cString:String = hex.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet() as NSCharacterSet).uppercaseString
        if (cString.hasPrefix("#")) {
            cString = cString.substringFromIndex(cString.startIndex.advancedBy(1))
        }
        if ((cString.characters.count) != 6) {
            return UIColor.grayColor()
        }
        var rgbValue:UInt32 = 0
        NSScanner(string: cString).scanHexInt(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func afterFireBaseCall() {
        let afterFireBase = FIRRemoteConfig.remoteConfig()
        let aboutUsText        = afterFireBase.configValueForKey("aboutUsText").stringValue
        let privacyPolicyText  = afterFireBase.configValueForKey("privacyPolicyText").stringValue
        let termsofServiceText = afterFireBase.configValueForKey("termsofServiceText").stringValue
        //let aboutUsTextData    = afterFireBase.configValueForKey("aboutUsTextData").stringValue
        
        let txtViewCertified   = afterFireBase.configValueForKey("aboutUsTxtViewCertified").stringValue
        let txtViewAddress1    = afterFireBase.configValueForKey("aboutUsTxtViewAddress1").stringValue
        let txtViewAddress2    = afterFireBase.configValueForKey("aboutUsTxtViewAddress2").stringValue
        let txtViewMap         = afterFireBase.configValueForKey("aboutUsTxtViewMap").stringValue
        let txtViewPhone       = afterFireBase.configValueForKey("aboutUsTxtViewPhone").stringValue
        let txtViewBz          = afterFireBase.configValueForKey("aboutUsTxtViewBz").stringValue
        let txtViewDesc        = afterFireBase.configValueForKey("aboutUsTxtViewDesc").stringValue
        
        let backgroundColorValue = afterFireBase.configValueForKey("aboutUsBackgroundColor").stringValue!
        self.view.backgroundColor = hexStringToUIColor(backgroundColorValue)
         let headerColorValue = afterFireBase.configValueForKey("aboutUsHeaderColor").stringValue!
        self.navView.backgroundColor = hexStringToUIColor(headerColorValue)
        
        self.txtColorValue       = afterFireBase.configValueForKey("aboutUsTextColor").stringValue!
        let txtColor             = hexStringToUIColor(txtColorValue)
        if IS_IPAD {
            self.lblTitleIpad.textColor  = txtColor
            self.lblTitleIpad.text = aboutUsText
        } else {
            self.lblTitle.textColor  = txtColor
            self.lblTitle.text = aboutUsText
        }
        
        setUI(privacyPolicyText!, strTermsofServiceText: termsofServiceText!, strTxtViewCertified : txtViewCertified!, strTxtViewAddress1 : txtViewAddress1!, strTxtViewAddress2 : txtViewAddress2!, strTxtViewMap : txtViewMap!, strTxtViewPhone : txtViewPhone!, strTxtViewBz : txtViewBz!, strTxtViewDesc : txtViewDesc!)
    }
    
    func setupDefaultConfig() {
        let defaultValues = [
            "aboutUsText"       : "About Us",
            "privacyPolicyText" : "Privacy Policy",
            "termsofServiceText": "Terms of Service",
            "aboutUsTextColor"  : "#3BB9F9",
            "aboutUsHeaderColor": "#ffffff",
            "aboutUsBackgroundColor": "#ffffff"
        ]
        FIRRemoteConfig.remoteConfig().setDefaults(defaultValues)
        afterFireBaseCall()
    }
    
    func fetchRemoteConfig() {
        var expirationDuration : NSTimeInterval = 3600
        if FIRRemoteConfig.remoteConfig().configSettings.isDeveloperModeEnabled {
            expirationDuration = 0.0
        }
        
        FIRRemoteConfig.remoteConfig().fetchWithExpirationDuration(expirationDuration, completionHandler: {(status, error) -> Void in
            if (error == nil) {
                print("Config fetched!",status)
                self.afterFireBaseCall()
                self.sizeHeaderToFit()
                FIRRemoteConfig.remoteConfig().activateFetched()
            }
            else {
                print("Config not fetched")
                print("Error \(error!.localizedDescription)")
            }
        })
    }
    
    func setUI(strPrivacyPolicyText : String , strTermsofServiceText : String, strTxtViewCertified : String , strTxtViewAddress1 : String, strTxtViewAddress2 : String, strTxtViewMap : String, strTxtViewPhone : String, strTxtViewBz : String, strTxtViewDesc : String) {
        
        rowsArray.removeAll()
        
        let txtColorCertrified          = hexStringToUIColor(txtColorValue)
        let txtColorAddress1           = hexStringToUIColor(txtColorValue)
        let txtColorAddress2           = hexStringToUIColor(txtColorValue)
        let txtColorMap                = hexStringToUIColor(txtColorValue)
        let txtColorPhone              = hexStringToUIColor(txtColorValue)
        let txtColorBz                 = hexStringToUIColor(txtColorValue)
        let txtColorDesc               = hexStringToUIColor(txtColorValue)
        
        if IS_IPAD {
            self.tblAbout.tableHeaderView = viewHeaderIpad
            self.txtViewCertifiedIpad.textColor = txtColorCertrified
            self.txtViewCertifiedIpad.text      = strTxtViewCertified
            
            self.txtViewAddress1Ipad.text      = strTxtViewAddress1
            self.txtViewAddress1Ipad.textColor = txtColorAddress1
            
            self.txtViewAddress2Ipad.textColor = txtColorAddress2
            self.txtViewAddress2Ipad.text      = strTxtViewAddress2
            
            self.txtViewMapIpad.textColor      = txtColorMap
            self.txtViewMapIpad.text           = strTxtViewMap
            
            self.txtViewPhoneIpad.textColor    = txtColorPhone
            self.txtViewPhoneIpad.text         = strTxtViewPhone
            
            self.txtViewBzIpad.textColor       = txtColorBz
            self.txtViewBzIpad.text            = strTxtViewBz
            
            self.txtViewDetailsIpad.textColor  = txtColorDesc
            self.txtViewDetailsIpad.text       = strTxtViewDesc
        } else {
            self.tblAbout.tableHeaderView = viewHeader
            self.txtViewCertified.textColor = txtColorCertrified
            self.txtViewCertified.text      = strTxtViewCertified
            
            self.txtViewAddress1.text      = strTxtViewAddress1
            self.txtViewAddress1.textColor = txtColorAddress1
            
            self.txtViewAddress2.textColor = txtColorAddress2
            self.txtViewAddress2.text      = strTxtViewAddress2
            
            self.txtViewMap.textColor      = txtColorMap
            self.txtViewMap.text           = strTxtViewMap
            
            self.txtViewPhone.textColor    = txtColorPhone
            self.txtViewPhone.text         = strTxtViewPhone
            
            self.txtViewBz.textColor       = txtColorBz
            self.txtViewBz.text            = strTxtViewBz
            
            self.txtViewDetails.textColor  = txtColorDesc
            self.txtViewDetails.text       = strTxtViewDesc
        }
        
        let row0: [String : AnyObject] = ["placeholder": strPrivacyPolicyText, "value":"" ]
        let row1: [String : AnyObject] = ["placeholder": strTermsofServiceText, "value":"" ]
        
        let tempArray = [row0, row1]
        for i in 0 ..< tempArray.count {
            let temp = Row().initWithDictionary(tempArray[i])
            rowsArray.append(temp)
        }
        tblAbout.separatorInset  = UIEdgeInsetsZero
        tblAbout.layoutMargins   = UIEdgeInsetsZero
        tblAbout.tableFooterView = UIView()
        tblAbout.reloadData()
    }
    
    func sizeHeaderToFit() {
        if IS_IPAD {
            txtViewDetailsIpad.layoutIfNeeded()
            
            viewHeaderIpad.setNeedsLayout()
            viewHeaderIpad.layoutIfNeeded()
            
            let contentSize = txtViewDetailsIpad.intrinsicContentSize()
            let height: CGFloat = contentSize.height
            var frame: CGRect = viewHeaderIpad.frame
            frame.size.height = height + 300
            viewHeaderIpad.frame = frame
            
            tblAbout.tableHeaderView = viewHeaderIpad
        } else {
            txtViewDetails.layoutIfNeeded()
            viewHeader.setNeedsLayout()
            viewHeader.layoutIfNeeded()
            
            let contentSize = txtViewDetails.intrinsicContentSize()
            let height: CGFloat = contentSize.height
            var frame: CGRect = viewHeader.frame
            frame.size.height = height + 230
            viewHeader.frame = frame
            
            tblAbout.tableHeaderView = viewHeader
        }
        tblAbout.tableFooterView = UIView()
    }
    
    func openPrivacyPolicy() {
        let viewCtrl = WebviewCtrl(nibName: "WebviewCtrl", bundle: nil)
        viewCtrl.strURL = PRIVACY_POLICY_URL
        viewCtrl.titleStr = PRIVACY_POLICY
        self.navigationController?.pushViewController(viewCtrl, animated: true)
    }
    
    func openTermsAndConditions() {
        let viewCtrl = WebviewCtrl(nibName: "WebviewCtrl", bundle: nil)
        viewCtrl.strURL = TERMS_URL
        viewCtrl.titleStr = TERMS_OF
        self.navigationController?.pushViewController(viewCtrl, animated: true)
    }
    
    // MARK: - UITableView Methods and Functions
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowsArray.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "AboutCell")
        let curRow = rowsArray[indexPath.row]
        cell.accessoryType = .DisclosureIndicator
        cell.backgroundColor = UIColor.clearColor()
        
        cell.textLabel?.font = kSystemRegularFonts(17.0)
        
        cell.textLabel?.text      = curRow.placeholder
        let txtColor              = hexStringToUIColor(txtColorValue)
        cell.textLabel?.textColor = txtColor
        cell.separatorInset       = UIEdgeInsetsZero
        cell.layoutMargins        = UIEdgeInsetsZero
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        let curRow = rowsArray[indexPath.row]
        if curRow.placeholder == PRIVACY_POLICY {
            openPrivacyPolicy()
        } else {
            openTermsAndConditions()
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return IS_IPAD ? 60 : 44
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.min
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
