//
//  SubmitReviewCtrl.swift
//  Verityscanner
//
//  Created by harmis on 25/02/17.
//  Copyright © 2017 certified. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Alamofire
import SwiftyJSON
import JVFloatLabeledTextField

class SubmitReviewCtrl: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate {
    
    @IBOutlet var txtTitle         : JVFloatLabeledTextField!
    @IBOutlet var txtSelectRating  : JVFloatLabeledTextField!
    
    @IBOutlet var viewTitle        : UIView!
    @IBOutlet var viewSelectRating : UIView!
    
    @IBOutlet var txtView          : IQTextView!
    @IBOutlet var btnSubmit        : UIButton!
    @IBOutlet var pickerValue    : UIPickerView!
    
    var request        : Alamofire.Request?
    var curProductData:Product? = nil
    
    var dicProducts = Product()
    var intSelectedRating : Int = 0
    
    var aryPicker = ["Very Poor", "Fair", "Average", "Good", "Excellent"]
    
    // MARK: - UIView Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    // MARK: - All Functions
    
    func isValid() -> Bool {
        if self.txtTitle.text == "" {
            CommonMethods.sharedInstance.showAlertWithMessage("Please enter title.", withController: self)
            return false
        } else if self.txtView.text == "" {
            CommonMethods.sharedInstance.showAlertWithMessage("Please enter description.", withController: self)
            return false
        } else if self.txtSelectRating.text == "" {
            CommonMethods.sharedInstance.showAlertWithMessage("Please select rating.", withController: self)
            return false
        }
        return true
    }
    
    func setUI() {
        self.btnSubmit.layer.cornerRadius = 3.0
        self.btnSubmit.addShadowView()
        
        viewTitle.layer.cornerRadius = 3.0
        viewTitle.layer.borderWidth = 1.0
        viewTitle.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        viewSelectRating.layer.cornerRadius = 3.0
        viewSelectRating.layer.borderWidth = 1.0
        viewSelectRating.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        txtView.layer.cornerRadius = 3.0
        txtView.layer.borderWidth = 1.0
        txtView.layer.borderColor = UIColor.lightGrayColor().CGColor
    }
    
    func alertAction(strMessage : String, dicProduct : Product) {
        let alertController = UIAlertController(title: APP_NAME, message: strMessage, preferredStyle:UIAlertControllerStyle.Alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default)
        { action -> Void in
        NSNotificationCenter.defaultCenter().postNotificationName("addDicProductNotification", object: dicProduct)
            self.navigationController?.popViewControllerAnimated(true)
            })
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    
    // MARK: - IBAction Methods and Functions
    
    @IBAction func btnSubmitAction(sender : UIButton) {
        if (isValid()) {
            if (CommonMethods.sharedInstance.isConnected) {
                
                /*
                 reviewTitle
                 reviewText
                 userId
                 productId
                 */
                
                print(CommonMethods.sharedInstance.userModel?.userId)
                
                let parameters = [
                    "reviewTitle" : self.txtTitle.text,
                    "reviewText"  : self.txtView.text,
                    "userId"      : CommonMethods.sharedInstance.userModel?.userId,
                    "productId"   : curProductData!.productId,
                    "reviewValue" : String(self.intSelectedRating)
                ]
                
                print(parameters)
                
                self.request = Alamofire.request(.POST, GIVE_REVIEW_URL, parameters: parameters, encoding:.JSON).validate().responseJSON { response in
                    print("--------- Request URL - %@", response.request?.URL)
                    
                    switch response.result {
                    case .Success:
                        if let value = response.result.value {
                            let json = JSON(value)
                            print("JSON: \(json)")
                            if (json["success"].numberValue == 1) {
                                if let curDic = json["result"].dictionary {
                                    self.dicProducts = Product().initWithDictionary(curDic)
                                    self.alertAction("Send review successfully.", dicProduct: self.dicProducts)
                                }
                            } else {
                                if let message = json["message"].string {
                                    CommonMethods.sharedInstance.showAlertWithMessage(message, withController: self)
                                } else {
                                    CommonMethods.sharedInstance.showAlertWithMessage(INVALID_RESPONSE, withController: self)
                                }
                            }
                        }
                    case .Failure(let error):
                        print(error)
                        if let data = response.data {
                            print("Response data: \(NSString(data: data, encoding: NSUTF8StringEncoding)!)")
                            if error.code != NSURLErrorCancelled {
                                CommonMethods.sharedInstance.showAlertWithMessage(error.localizedDescription, withController: self)
                            }
                        }
                    }
                }
            } else {
                CommonMethods.sharedInstance.showAlertWithMessage(NO_INTERNET, withController: self)
            }
        }
    }
    
    // MARK: - UIPickerView Datasource & Delegate Methods
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.aryPicker.count
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.aryPicker[row]
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        //        let selectedName = self.aryPicker[row]
        //        self.txtSelectRating.text = selectedName
        //        let selectedIndexOf = self.aryPicker.indexOf(selectedName)
        //        self.intSelectedRating = selectedIndexOf! + 1
        //        print(self.intSelectedRating)
        doneAction(self.txtSelectRating)
    }
    
    // MARK: - TextField Delegate Methods And Fuction
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if (textField.tag == 2) {
            pickerValue.tag = textField.tag
            pickerValue.delegate = self
            pickerValue.dataSource = self
            textField.inputView = pickerValue
        } else {
            textField.inputView = nil
            pickerValue.delegate = nil
            pickerValue.dataSource = nil
        }
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if let swRange = textField.text!.rangeFromNSRange(range) {
            let newString = textField.text!.stringByReplacingCharactersInRange(swRange, withString: string)
            if (textField.tag == 2) {
                return false
            }
            self.txtSelectRating.text = newString as String
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder();
        return true;
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        doneAction(textField)
    }
    
    func doneAction (textField : UITextField?) {
        if (textField!.tag == 2) {
            let curSelectedRating = self.aryPicker[pickerValue.selectedRowInComponent(0)]
            self.txtSelectRating.text = curSelectedRating
            let selectedIndexOf = self.aryPicker.indexOf(curSelectedRating)
            self.intSelectedRating = selectedIndexOf! + 1
            print(self.intSelectedRating)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
