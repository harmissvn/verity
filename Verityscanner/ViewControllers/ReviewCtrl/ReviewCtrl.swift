//
//  ReviewCtrl.swift
//  Verityscanner
//
//  Created by harmis on 25/02/17.
//  Copyright © 2017 certified. All rights reserved.
//

import UIKit
import SwiftyJSON
import Alamofire

class ReviewCtrl: UIViewController {
    
    @IBOutlet var tblReview : UITableView!
    
    var strProductId : String = ""
    var request        : Alamofire.Request?
    
    let cellIdentifier = "ReviewCell"
    var aryReviewList = [AllReview]()
    
    // MARK: - UIView Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        serviceCall()
        setUI()
    }
    
    // MARK: - All Functions
    
    func setUI() {
        
        tblReview.estimatedRowHeight = 46.0
        tblReview.rowHeight = UITableViewAutomaticDimension

        tblReview.registerNib(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
    }
    
    func serviceCall() {
        if (CommonMethods.sharedInstance.isConnected) {
            print(CommonMethods.sharedInstance.userModel?.userId)
            
            let parameters = [
                "productId"   : strProductId
            ]
            
            print(parameters)
            
            self.request = Alamofire.request(.POST, REVIEW_LIST_URL, parameters: parameters, encoding:.JSON).validate().responseJSON { response in
                print("--------- Request URL - %@", response.request?.URL)
                
                switch response.result {
                case .Success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        print("JSON: \(json)")
                        if (json["success"].numberValue == 1) {
                            if (json["result"].exists()) {
                                let aryData = json["result"].array
                                for i in 0 ..< aryData!.count {
                                    let dicData = aryData![i].dictionary
                                    let item = AllReview().initWithDictionary(dicData!)
                                    self.aryReviewList.append(item)
                                }
                                self.tblReview.reloadData()
                            } else {
                                CommonMethods.sharedInstance.showAlertWithMessage(INVALID_RESPONSE, withController: self)
                            }
                        } else {
                            if let message = json["message"].string {
                                CommonMethods.sharedInstance.showAlertWithMessage(message, withController: self)
                            }
                        }
                    }
                case .Failure(let error):
                    print(error)
                    if let data = response.data {
                        print("Response data: \(NSString(data: data, encoding: NSUTF8StringEncoding)!)")
                        if error.code != NSURLErrorCancelled {
                            CommonMethods.sharedInstance.showAlertWithMessage(error.localizedDescription, withController: self)
                        }
                    }
                }
            }
        } else {
            CommonMethods.sharedInstance.showAlertWithMessage(NO_INTERNET, withController: self)
        }
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.aryReviewList.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! ReviewCell
        
        let curRow = self.aryReviewList[indexPath.row]
        
        cell.lblTitle.text = curRow.reviewTitle
        cell.lblname.text  = curRow.userName
        cell.lblDesc.text  = curRow.reviewText
        cell.lbldate.text  = curRow.reviewDate
        
        return cell
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.min
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.min
    }
    
}
