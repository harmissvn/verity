//
//  LoginCtrl.swift
//  Verityscanner
//
//  Created by harmis on 30/08/16.
//  Copyright © 2016 certified. All rights reserved.


import UIKit
import Firebase
import FirebaseAuth
import FBSDKLoginKit
import FBSDKCoreKit
import Accounts
import GoogleSignIn
import TwitterKit
import SafariServices
import Alamofire
import SwiftyJSON
import Quickblox
import QuickbloxWebRTC

//enum SOCIAL_ACC : Int {
//    case GOOGLE = 0
//    case FACEBOOK = 1
//    case TWITTER = 2
//}

class LoginCtrl: UIViewController,FBSDKLoginButtonDelegate, GIDSignInUIDelegate, GIDSignInDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var facebookLoginButton: UIButton!
    @IBOutlet weak var googleSignInButton : UIButton!
    @IBOutlet weak var twitterloginButton : UIButton!
    @IBOutlet weak var btnLoginUser       : UIButton!
    @IBOutlet weak var btnLoginManager    : UIButton!
    
    @IBOutlet var btnBack: UIButton!
    @IBOutlet weak var viewHeader         : UIView!
    @IBOutlet weak var viewFooter         : UIView!
    @IBOutlet weak var viewFooterUser     : UIView!
    @IBOutlet weak var viewFooterManager  : UIView!
    
    @IBOutlet weak var tblLogin           : UITableView!
    @IBOutlet var segmnetCtrl             : UISegmentedControl!
    @IBOutlet var heightLblManager        : NSLayoutConstraint!
    
    var rowsArray = [Row]()
    
    let EMAIL     = "Email"
    let PASSWORD  = "Password"
    
    let cellIdentifier = "TFCell"
    
    // MARK: - UIView Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setTableView()
        setUI()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if (CommonMethods.sharedInstance.usrState == USER_STATE.LOGGED_IN.rawValue) {
            if (CommonMethods.sharedInstance.userModel?.socialType == "0") {
                GIDSignIn.sharedInstance().signInSilently()
            } else {
                let viewCtrl = HomeCtrl(nibName: "HomeCtrl", bundle: nil)
                self.navigationController?.pushViewController(viewCtrl, animated: true)
            }
        }
        let pass: Row = rowsArray[1]
        pass.value = ""
        
        let indexPath = NSIndexPath(forRow:1, inSection:0)
        let cell : TFCell? = tblLogin.cellForRowAtIndexPath(indexPath) as! TFCell?
        cell?.textField.text = ""
        tblLogin.reloadData()
    }
    
    // MARK: - All Functions
    
    func setUI() {
        GIDSignIn.sharedInstance().delegate   = self
        GIDSignIn.sharedInstance().uiDelegate = self
        
        self.viewFooterUser.hidden    = false
        self.viewFooterManager.hidden = true
        btnLoginUser.layer.cornerRadius = 5.0
        btnLoginUser.addShadowView()
        googleSignInButton.addShadowView()
        facebookLoginButton.addShadowView()
        twitterloginButton.addShadowView()
    }
    
    func setTableView() {
        tblLogin.tableHeaderView = viewHeader
        tblLogin.tableFooterView = viewFooter
        tblLogin.registerNib(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        
        rowsArray.removeAll()
        
        let row0: [String : AnyObject] = ["placeholder": EMAIL,"value": ""]
        let row1: [String : AnyObject] = ["placeholder": PASSWORD,"value": ""]
        
        let tempArray = [row0, row1]
        for i in 0 ..< tempArray.count {
            let temp = Row().initWithDictionary(tempArray[i])
            rowsArray.append(temp)
        }
        tblLogin.reloadData()
    }
    
    func CallQuickBloxLogin() {
        let newUser = QBUUser()
        newUser.login = CommonMethods.sharedInstance.userModel?.email
        newUser.password = KQB_USER_DEFAULT_PASSWORD
        newUser.fullName = CommonMethods.sharedInstance.userModel?.name
        newUser.tags = ["Verity"]
        QBRequest.signUp(newUser, successBlock: { (response, user) in
            print(response)
            appDel!.connectUserWithQuickbloxChat()
            print("user Successfully login in chat.")
        }) { (error) in
            if error.error?.error?.localizedDescription == "Request failed: client error (422)" {
                print("user already exists in quick blox database.")
                appDel!.connectUserWithQuickbloxChat()
            }
        }
    }
    
    @IBAction func segmntAction(sender:UISegmentedControl) {
        switch segmnetCtrl.selectedSegmentIndex {
        case 0:
            btnLoginUser.layer.cornerRadius = 5.0
            btnLoginUser.addShadowView()
            self.viewFooterUser.hidden    = false
            self.viewFooterManager.hidden = true
            self.heightLblManager.constant  = 0.0
            self.tblLogin.reloadData()
            break
        case 1:
            btnLoginManager.layer.cornerRadius = 5.0
            btnLoginManager.addShadowView()
            self.viewFooterUser.hidden    = true
            self.viewFooterManager.hidden = false
            self.heightLblManager.constant  = 45.0
            self.tblLogin.reloadData()
            break
        default:
            break;
        }
    }
    
    // MARK: - Facebook Login Delegate Methods
    
    @IBAction func btnFacebookClicked(_: UIButton) {
        //Firebase Log Event
        FIRAnalytics.logEventWithName(FB_LOGIN_EVENT, parameters: [
            "name": FB_LOGIN_EVENT_NAME,
            "full_text": ""
            ])
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logInWithReadPermissions(["email", "user_birthday", "public_profile"], fromViewController: self) { (result, error) -> Void in
            
            CommonMethods.sharedInstance.showHudWithTitle(STR_LOADING)
            
            if let isCancelled = result?.isCancelled {
                if isCancelled {
                    CommonMethods.sharedInstance.hideHud()
                    return
                }
            }
            
            if error != nil {
                CommonMethods.sharedInstance.hideHud()
                CommonMethods.sharedInstance.showAlertWithMessage(error.localizedDescription, withController: self)
                return
            }
            
            guard let currentAccessToken = FBSDKAccessToken.currentAccessToken()?.tokenString,
                let credential : FIRAuthCredential? = FIRFacebookAuthProvider.credentialWithAccessToken(currentAccessToken)
                else {
                    CommonMethods.sharedInstance.hideHud()
                    CommonMethods.sharedInstance.showAlertWithMessage("Facebook access token not available.", withController: self)
                    return
            }
            
            self.getFacebookUserDetailsFromAccesstoken(currentAccessToken, credential: credential)
        }
    }
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        CommonMethods.sharedInstance.showHudWithTitle(STR_LOADING)
        
        if result.isCancelled {
            CommonMethods.sharedInstance.hideHud()
            return
        }
        
        if error != nil {
            CommonMethods.sharedInstance.hideHud()
            CommonMethods.sharedInstance.showAlertWithMessage(error.localizedDescription, withController: self)
            return
        }
        
        guard let currentAccessToken = FBSDKAccessToken.currentAccessToken()?.tokenString,
            let credential : FIRAuthCredential? = FIRFacebookAuthProvider.credentialWithAccessToken(currentAccessToken)
            else {
                CommonMethods.sharedInstance.hideHud()
                CommonMethods.sharedInstance.showAlertWithMessage("Facebook access token not available.", withController: self)
                return
        }
        
        getFacebookUserDetailsFromAccesstoken(currentAccessToken, credential: credential)
        
    }
    
    func getFacebookUserDetailsFromAccesstoken(accessToken:String, credential:FIRAuthCredential?) {
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email, birthday"])
        
        graphRequest.startWithCompletionHandler { (comnnection, object, error) in
            if error != nil {
                CommonMethods.sharedInstance.hideHud()
                CommonMethods.sharedInstance.showAlertWithMessage(error.localizedDescription, withController: self)
                return
            }
            
            if object != nil {
                let userDetails = NSMutableDictionary()
                userDetails.setValue(SOCIAL_ACC.FACEBOOK.rawValue, forKey: "socialType")
                userDetails.setValue("", forKey: "profileImage")
                
                if let imageURL = object.valueForKey("picture")?.valueForKey("data")?.valueForKey("url") as? String {
                    //Download image from imageURL
                    userDetails.setValue(imageURL, forKey: "profileImage")
                }
                
                userDetails.setValue( object["name"] ?? "", forKey: "name")
                userDetails.setValue(object["email"] ?? "", forKey: "email")
                userDetails.setValue(SOCIAL_ACC.FACEBOOK.rawValue, forKey: "socialType")
                userDetails.setValue("YES", forKey: "isSocial")
                userDetails.setValue("", forKey: "password")
                userDetails.setValue("", forKey: "birthDay")
                userDetails.setValue("", forKey: "mobileNumber")
                userDetails.setValue("", forKey: "firebaseId")
                self.loginWithFirebase(credential!, forAccount: SOCIAL_ACC.FACEBOOK.rawValue, userDetails:userDetails )
                print("Facebook user details = \(userDetails)")
            } else {
                CommonMethods.sharedInstance.hideHud()
                CommonMethods.sharedInstance.showAlertWithMessage("Facebook Error: unable to get user details.", withController: self)
            }
        }
    }
    
    func loginButtonWillLogin(loginButton: FBSDKLoginButton!) -> Bool {
        return true
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        print("User logged out of facebook")
    }
    
    // MARK: - Google Login Delegate Methods
    
    @IBAction func btnGoogleClicked(_: UIButton) {
        //Firebase Log Event
        FIRAnalytics.logEventWithName(GOOGLE_LOGIN_EVENT, parameters: [
            "name": GOOGLE_LOGIN_EVENT_NAME,
            "full_text": ""
            ])
        GIDSignIn.sharedInstance().signIn()
    }
    
    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!, withError error: NSError?) {
        CommonMethods.sharedInstance.showHudWithTitle(STR_LOADING)
        
        if error != nil {
            CommonMethods.sharedInstance.hideHud()
            if (error!.code != -5) {
                CommonMethods.sharedInstance.showAlertWithMessage(error!.localizedDescription, withController: self)
            }
            return
        }
        
        guard let googleUser = user,
            let  authentication : GIDAuthentication? = googleUser.authentication,
            let credential : FIRAuthCredential? = FIRGoogleAuthProvider.credentialWithIDToken(authentication!.idToken,accessToken: authentication!.accessToken)
            else {
                CommonMethods.sharedInstance.hideHud()
                CommonMethods.sharedInstance.showAlertWithMessage("Google Error: unable to get user details.", withController: self)
                return
        }
        
        let userDetails = NSMutableDictionary()
        let profileImage = user.profile.imageURLWithDimension(400).absoluteString
        userDetails.setValue(user.profile.name, forKey: "name")
        userDetails.setValue(user.profile.email ?? "", forKey: "email")
        userDetails.setValue(SOCIAL_ACC.GOOGLE.rawValue, forKey: "socialType")
        userDetails.setValue(profileImage, forKey: "profileImage")
        userDetails.setValue("", forKey: "password")
        userDetails.setValue("", forKey: "birthDay")
        userDetails.setValue("", forKey: "mobileNumber")
        userDetails.setValue("YES", forKey: "isSocial")
        userDetails.setValue("", forKey: "firebaseId")
        
        self.loginWithFirebase(credential!, forAccount: SOCIAL_ACC.GOOGLE.rawValue, userDetails:userDetails )
        print("Google user details = \(userDetails)")
    }
    
    func signIn(signIn: GIDSignIn!, didDisconnectWithUser user: GIDGoogleUser!, withError error: NSError!) {
    }
    
    func signIn(signIn: GIDSignIn!, presentViewController viewController: UIViewController!) {
        self.presentViewController(viewController, animated: true, completion: nil)
    }
    
    func signIn(signIn: GIDSignIn!, dismissViewController viewController: UIViewController!) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
        CommonMethods.sharedInstance.hideHud()
    }
    
    // MARK: - Twitter Login Delegate Methods
    
    
    @IBAction func btnTwitterClicked(_: UIButton) {
        //let store = Twitter.sharedInstance().sessionStore
        //let sessions = store.existingUserSessions()
        
        let store = Twitter.sharedInstance().sessionStore
        if let userID = store.session()?.userID {
            store.logOutUserID(userID)
        }
        
        //Firebase Log Event
        FIRAnalytics.logEventWithName(TW_LOGIN_EVENT, parameters: [
            "name": TW_LOGIN_EVENT_NAME,
            "full_text": ""
            ])
        
        Twitter.sharedInstance().logInWithCompletion {
            (session, error) -> Void in
            if error != nil && error!.code != 1 {
                CommonMethods.sharedInstance.showAlertWithMessage(error!.localizedDescription, withController: self)
                return
            }
            
            if (session != nil) {
                guard let credential : FIRAuthCredential? = FIRTwitterAuthProvider.credentialWithToken(session!.authToken, secret: session!.authTokenSecret)
                    else {
                        CommonMethods.sharedInstance.hideHud()
                        CommonMethods.sharedInstance.showAlertWithMessage("Twitter Error: unable to get user details.", withController: self)
                        return
                }
                //Get twitter user details
//                self.getTwitterUserDetails(credential)
            }
        }
    }
    
    
//    func getTwitterUserDetails(credential:FIRAuthCredential?) {
//        
//        dispatch_async(dispatch_get_main_queue(), { () -> Void in
//            CommonMethods.sharedInstance.showHudWithTitle(STR_LOADING)
//        })
//        
//        let client = TWTRAPIClient.clientWithCurrentUser()
//        let request = client.URLRequestWithMethod("GET",
//                                                  URL: "https://api.twitter.com/1.1/account/verify_credentials.json",
//                                                  parameters: ["include_email": "true", "skip_status": "true"],
//                                                  error: nil)
//        
//        client.sendTwitterRequest(request) { response, data, connectionError in
//            
//            if connectionError != nil {
//                dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                    CommonMethods.sharedInstance.hideHud()
//                    CommonMethods.sharedInstance.showAlertWithMessage(connectionError?.localizedDescription ?? "Twitter: Unknown Error", withController: self)
//                })
//                return
//            }
//            
//            guard let responseData = data else {
//                print("Error: did not receive data")
//                dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                    CommonMethods.sharedInstance.hideHud()
//                    CommonMethods.sharedInstance.showAlertWithMessage(INVALID_RESPONSE, withController: self)
//                })
//                return
//            }
//            
//            // Parse the result as JSON, since that's what the API provides
//            let post: NSDictionary
//            do {
//                post = try NSJSONSerialization.JSONObjectWithData(responseData,
//                                                                  options: []) as! NSDictionary
//            } catch  {
//                print("error trying to convert data to JSON")
//                let datastring = NSString(data: responseData, encoding: NSUTF8StringEncoding)
//                print(datastring)
//                
//                dispatch_async(dispatch_get_main_queue(), { () -> Void in
//                    CommonMethods.sharedInstance.hideHud()
//                    CommonMethods.sharedInstance.showAlertWithMessage(INVALID_RESPONSE, withController: self)
//                })
//                return
//            }
//            //Now we have the post, let's just print it to prove we can access it
//            print("The response is: " + CommonMethods.sharedInstance.JSONStringify(post, prettyPrinted: true))
//            
//            let userDetails = NSMutableDictionary()
//            userDetails.setValue(SOCIAL_ACC.TWITTER.rawValue, forKey: "socialType")
//            userDetails.setValue(post["name"] ?? "", forKey: "name")
//            userDetails.setValue(post["email"] ?? "", forKey: "email")
//            userDetails.setValue(post["profile_image_url_https"] ?? "", forKey: "profileImage")
//            userDetails.setValue("YES", forKey: "isSocial")
//            userDetails.setValue("", forKey: "password")
//            userDetails.setValue("", forKey: "birthDay")
//            userDetails.setValue("", forKey: "mobileNumber")
//            userDetails.setValue("", forKey: "firebaseId")
//            self.loginWithFirebase(credential!, forAccount: SOCIAL_ACC.TWITTER.rawValue, userDetails:userDetails )
//            print("Twitter user details = \(userDetails)")
//        }
//    }
    
    
    // MARK: - Login with Firebase and CRM
    
    func loginWithFirebase(credential:FIRAuthCredential, forAccount:Int, userDetails:NSDictionary?) {
        if (isValidUserDetails(userDetails!)) {
            FIRAuth.auth()?.signInWithCredential(credential) { (user, error) in
                if error != nil {
                    CommonMethods.sharedInstance.hideHud()
                    CommonMethods.sharedInstance.showAlertWithMessage(error!.localizedDescription, withController: self)
                    return
                }
                self.userWithFireBaseUser(user, forAccount: forAccount, userDetails: userDetails)
            }
        } else {
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                CommonMethods.sharedInstance.hideHud()
            })
        }
    }
    
    func userWithFireBaseUser(firUser:FIRUser?, forAccount : Int, userDetails:NSDictionary?) {
        //Call CRM login api
        if (CommonMethods.sharedInstance.isConnected) {
            CommonMethods.sharedInstance.showHudWithTitle(STR_LOADING)
            print(userDetails)
            Alamofire.request(.POST, SIGNIN_URL, parameters: userDetails! as? [String : AnyObject], encoding:.JSON).validate().responseJSON { response in
                print("--------- Request URL - %@", response.request?.URL)
                CommonMethods.sharedInstance.hideHud()
                
                switch response.result {
                case .Success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        print("JSON: \(json)")
                        if (json["success"].numberValue == 1) {
                            if let userData = json["profile"].dictionary {
                                let myuser = User().initWithDictionary(userData)
                                CommonMethods.sharedInstance.userModel = myuser
                                CommonMethods.sharedInstance.setUserState(USER_STATE.LOGGED_IN.rawValue)
                                CommonMethods.sharedInstance.saveUser()
                                CommonMethods.sharedInstance.hideHud()
                                self.CallQuickBloxLogin()
                                let ctrl = HomeCtrl(nibName: "HomeCtrl", bundle: nil)
                                self.navigationController?.pushViewController(ctrl, animated: true)
                            }
                        } else {
                            if let message = json["message"].string {
                                CommonMethods.sharedInstance.showAlertWithMessage(message, withController: self)
                            } else {
                                CommonMethods.sharedInstance.showAlertWithMessage(INVALID_RESPONSE, withController: self)
                            }
                        }
                    }
                case .Failure(let error):
                    print(error)
                    
                    if let data = response.data {
                        print("Response data: \(NSString(data: data, encoding: NSUTF8StringEncoding)!)")
                        //let strError = String(data: data, encoding: NSUTF8StringEncoding)!
                        CommonMethods.sharedInstance.showAlertWithMessage(error.localizedDescription, withController: self)
                    }
                }
                
            }
        } else {
            CommonMethods.sharedInstance.showAlertWithMessage(NO_INTERNET, withController: self)
        }
    }
    
    func isValidUserDetails(userDetails:NSDictionary) -> Bool {
        let email = userDetails["email"] as! String
        if email.characters.count == 0 {
            CommonMethods.sharedInstance.showAlertWithMessage("Please give access to your email address for login.", withController: self)
            return false
        }
        return true
    }
    
    // MARK: - UITableView Methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowsArray.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! TFCell
        cell.selectionStyle = .None
        
        cell.constLeftPadding.constant = 30
        cell.constRightPadding.constant = 30
        
        cell.textField.tag = indexPath.row
        cell.textField.keyboardType = .Default
        cell.textField.secureTextEntry = false
        cell.textField.delegate = self
        
        let curRow = rowsArray[indexPath.row]
        
        if (curRow.placeholder == EMAIL) {
            cell.textField.keyboardType = .EmailAddress
        } else if (curRow.placeholder == PASSWORD) {
            cell.textField.secureTextEntry = true
        }
        
        cell.textField?.placeholder = curRow.placeholder
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    // MARK: - IBAction Methods and Functions
    
    func isValid() -> Bool {
        let email: Row = rowsArray[0]
        let pass: Row = rowsArray[1]
        
        if email.value.characters.count == 0 {
            CommonMethods.sharedInstance.showAlertWithMessage("Please enter your email address.", withController: self)
            return false
        }
        else if !email.value.isValidEmail() {
            CommonMethods.sharedInstance.showAlertWithMessage("Please enter valid email address.", withController: self)
            return false
        }
        else if pass.value.characters.count == 0 {
            CommonMethods.sharedInstance.showAlertWithMessage("Please enter password.", withController: self)
            return false
        }
        return true
    }
    
    @IBAction func btnBackAction(sender: UIButton) {
        btnbackClicked()
    }
    
    @IBAction func btnSignUpAction(sender: UIButton) {
        let ctrl = RegisterCtrl(nibName: "RegisterCtrl", bundle: nil)
        self.navigationController?.pushViewController(ctrl, animated: true)
    }
    
    @IBAction func btnForgotPasswordClicked(_: AnyObject) {
        let alertController:UIAlertController = UIAlertController(title: "Forgot Password", message: "Please enter your email address", preferredStyle: .Alert)
        
        var forgotPasswordTf: UITextField?
        
        let ok = UIAlertAction(title: "Ok", style: .Default, handler: { (action) -> Void in
            self.callForgotPassApi((alertController.textFields?.first)!)
        })
        ok.enabled = false
        
        let cancel = UIAlertAction(title: "Cancel", style: .Cancel) { (action) -> Void in
        }
        
        alertController.addAction(cancel)
        alertController.addAction(ok)
        
        alertController.addTextFieldWithConfigurationHandler { (textField) -> Void in
            // Enter the textfiled customization code here.
            forgotPasswordTf = textField
            forgotPasswordTf?.placeholder = "Enter email address"
            forgotPasswordTf?.addTarget(self, action:#selector(LoginCtrl.alertTextFieldDidChange(_:)), forControlEvents:.EditingChanged)
        }
        presentViewController(alertController, animated: true, completion: nil)
    }
    
    func alertTextFieldDidChange(textField: UITextField) {
        let alertController:UIAlertController = (self.presentedViewController as? UIAlertController)!
        let email: UITextField = (alertController.textFields?.first)!
        let okAction: UIAlertAction = (alertController.actions.last)!
        okAction.enabled = (email.text?.isValidEmail())!
    }
    
    func callForgotPassApi(textField: UITextField) {
        if (CommonMethods.sharedInstance.isConnected) {
            CommonMethods.sharedInstance.showHudWithTitle("Loading...")
            print("Parameter - " + textField.text!)
            
            Alamofire.request(.POST, FORGOT_PASS_URL, parameters: ["email": textField.text!], encoding:.JSON).validate().responseJSON { response in
                print("--------- Request URL - %@", response.request?.URL)
                CommonMethods.sharedInstance.hideHud()
                
                switch response.result {
                case .Success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        print("JSON: \(json)")
                        if (json["success"].numberValue == 1) {
                            let message = json["message"].stringValue
                            CommonMethods.sharedInstance.showAlertWithMessage(message, withController: self)
                        } else {
                            let message = json["message"].stringValue
                            CommonMethods.sharedInstance.showAlertWithMessage(message, withController: self)
                        }
                    }
                case .Failure(let error):
                    CommonMethods.sharedInstance.showAlertWithMessage(error.localizedDescription, withController: self)
                }
            }
        } else {
            CommonMethods.sharedInstance.showAlertWithMessage(NO_INTERNET, withController: self)
        }
        
    }
    
    func firebaseNormalLogin(email: String, pass:String) {
        CommonMethods.sharedInstance.showHudWithTitle(STR_LOADING)
        FIRAuth.auth()?.signInWithEmail(email, password: pass, completion: { (firUser:FIRUser?, error:NSError?) in
            if error != nil {
                CommonMethods.sharedInstance.hideHud()
                CommonMethods.sharedInstance.showAlertWithMessage(error!.localizedDescription, withController: self)
                return
            }
            self.callLoginApi(email, pass: pass)
        })
    }
    
    func callLoginApi(email: String, pass:String) {
        let userDetails = NSMutableDictionary()
        userDetails.setValue(pass, forKey: "password")
        userDetails.setValue(email, forKey: "email")
        
        Alamofire.request(.POST, LOGIN_URL, parameters: (userDetails as NSDictionary) as? [String : AnyObject], encoding:.JSON).validate().responseJSON { response in
            print("--------- Request URL - %@", response.request?.URL)
            CommonMethods.sharedInstance.hideHud()
            
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print("JSON: \(json)")
                    if (json["success"].numberValue == 1) {
                        if let userData = json["profile"].dictionary {
                            let myuser = User().initWithDictionary(userData)
                            CommonMethods.sharedInstance.userModel = myuser
                            CommonMethods.sharedInstance.setUserState(USER_STATE.LOGGED_IN.rawValue)
                            CommonMethods.sharedInstance.saveUser()
                            CommonMethods.sharedInstance.hideHud()
                            self.CallQuickBloxLogin()
                            let ctrl = HomeCtrl(nibName: "HomeCtrl", bundle: nil)
                            self.navigationController?.pushViewController(ctrl, animated: true)
                        } else {
                            CommonMethods.sharedInstance.showAlertWithMessage(INVALID_RESPONSE, withController: self)
                        }
                    } else {
                        if let message = json["message"].string {
                            CommonMethods.sharedInstance.showAlertWithMessage(message, withController: self)
                        } else {
                            CommonMethods.sharedInstance.showAlertWithMessage(INVALID_RESPONSE, withController: self)
                        }
                    }
                }
            case .Failure(let error):
                print(error)
                if let data = response.data {
                    print("Response data: \(NSString(data: data, encoding: NSUTF8StringEncoding)!)")
                    //let strError = String(data: data, encoding: NSUTF8StringEncoding)!
                    CommonMethods.sharedInstance.showAlertWithMessage(error.localizedDescription, withController: self)
                }
            }
        }
    }
    
    @IBAction func btnLoginClicked(_: AnyObject) {
        if (isValid()) {
            if (CommonMethods.sharedInstance.isConnected) {
                let email: Row = rowsArray[0]
                let pass: Row = rowsArray[1]
                firebaseNormalLogin(email.value, pass: pass.value)
            } else {
                CommonMethods.sharedInstance.showAlertWithMessage(NO_INTERNET, withController: self)
            }
        }
    }
    
    // MARK: - UITextFieldDelegate Methods
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
        if let swRange = textField.text!.rangeFromNSRange(range) {
            let newString = textField.text!.stringByReplacingCharactersInRange(swRange, withString: string)
            let curRow = rowsArray[textField.tag]
            curRow.value = newString as String
        }
        return true
    }
    
    func textFieldShouldClear(textField: UITextField) -> Bool {
        let curRow = rowsArray[textField.tag]
        curRow.value = "" as NSString as String
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

