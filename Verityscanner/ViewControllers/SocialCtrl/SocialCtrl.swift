//
//  SocialCtrl.swift
//  Verityscanner
//
//  Created by Hardik Dineshchandra Mistry on 17/10/16.
//  Copyright © 2016 certified. All rights reserved.
//

import UIKit
import Firebase
import FirebaseInvites

class SocialCtrl: UIViewController, FIRInviteDelegate {

    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblReferenceCode: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblReferenceCode.text = "REF - \(CommonMethods.sharedInstance.userModel!.referalCode ?? "")"
    }

    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func btnShareClicked(_: AnyObject) {
        if let invite = FIRInvites.inviteDialog() {
            invite.setInviteDelegate(self)
            // NOTE: You must have the App Store ID set in your developer console project
            // in order for invitations to successfully be sent.
            
            // A message hint for the dialog. Note this manifests differently depending on the
            // received invation type. For example, in an email invite this appears as the subject.
            
            //invite.setMessage("Want to know where your food comes from? Organic,NON-GMO, HALAL, KOSHER, any Recalls Verity Scanning")
            invite.setMessage("Do you want to know more about your food or other products? Download Verity Scanning and find out!")
            // Title for the dialog, this is what the user sees before sending the invites.
            invite.setTitle("Certified, Inc. introduces Verity")
            let deeplinkStr = "https://tqk28.app.goo.gl/?link=\(CommonMethods.sharedInstance.userModel!.referalCode ?? "")&apn=com.certified.verityscanning"
            NSLog("\n-------------------- Old Deep Link = %@", deeplinkStr)
        
            //https://example.app.goo.gl/?link=https://www.example.com/someresource&apn=com.example.android&amv=3&al=exampleapp://someresource&ibi=com.example.ios&isi=1234567&ius=exampleapp

            //https://tqk28.app.goo.gl/?link=\(CommonMethods.sharedInstance.userModel!.referalCode ?? "")&apn=com.certified.verityscanning
            
            
            let dynamicUrl = "https://tqk28.app.goo.gl/?link=https://t.certified.bz/index.php&apn=com.certified.verityscanning&ibi=com.certified.verityscanning&isi=1124462403&ius=dynamicScheme&refCode=\(CommonMethods.sharedInstance.userModel!.referalCode ?? "")"
            NSLog("\n-------------------- New Deep Link = %@", dynamicUrl)
            
            invite.setDeepLink(dynamicUrl)
            invite.setCallToActionText("Install!")
            invite.setCustomImage("https://s3-us-west-1.amazonaws.com/verity.app/full_size_V.jpg")
            
            let targetApplication = FIRInvitesTargetApplication.init()
            targetApplication.androidClientID = ANDROID_CLIENT_ID
            invite.setOtherPlatformsTargetApplication(targetApplication)
            invite.open()
        }
    }
    
    func inviteFinishedWithInvitations(invitationIds: [AnyObject], error: NSError?) {
        if let error = error {
            print("Failed: " + error.localizedDescription)
            if (error.code != -402) {
                CommonMethods.sharedInstance.showAlertWithMessage(error.localizedDescription, withController: self)
            }
        } else {
            print("Invitations sent")
            NSLog("Invitation id's = %@", invitationIds)
            CommonMethods.sharedInstance.showAlertWithMessage("Invitation sent successfully.", withController: self)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
