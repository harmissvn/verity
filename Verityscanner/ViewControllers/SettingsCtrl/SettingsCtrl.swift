//
//  SettingsCtrl.swift
//  Verityscanner
//
//  Created by Hardik Dineshchandra Mistry on 31/08/16.
//  Copyright © 2016 certified. All rights reserved.
//

import UIKit
import Alamofire

import Firebase
import FBSDKLoginKit

class SettingsCtrl: UIViewController {
    
    @IBOutlet weak var btnSignOut : UIButton!
    
    @IBOutlet weak var navView    : UIView!
    @IBOutlet weak var viewFooter: UIView!
    @IBOutlet var viewFooterIpad : UIView!

    @IBOutlet weak var tblSettings: UITableView!
    @IBOutlet weak var lblTitle   : UILabel!
    
    @IBOutlet var lblTitleIpad   : UILabel!

    @IBOutlet var txtViewCertifiedIpad: UITextView!
    @IBOutlet var txtViewAddress1Ipad : UITextView!
    @IBOutlet var txtViewMapIpad      : UITextView!
    @IBOutlet var txtViewPhoneIpad    : UITextView!
    @IBOutlet var txtViewBzIpad       : UITextView!
    @IBOutlet var txtViewDetailsIpad  : UITextView!
    
    @IBOutlet var txtViewAddress2Ipad: UITextView!
    @IBOutlet weak var txtViewCertified: UITextView!
    @IBOutlet weak var txtViewAddress1 : UITextView!
    @IBOutlet weak var txtViewAddress2 : UITextView!
    @IBOutlet weak var txtViewMap      : UITextView!
    @IBOutlet weak var txtViewPhone    : UITextView!
    @IBOutlet weak var txtViewBz       : UITextView!
    @IBOutlet weak var txtViewDetails  : UITextView!

    var strBackLogo : String = ""
    var rowsArray = [Row]()
    
    var NAME                   = "Name"
    var BIRTHDAY               = "Birthday"
    var MOBILE_NO              = "Mobile Number"
    var EMAIL                  = "Email"
    var PRIVACY_POLICY         = "Privacy Policy"
    var TERMS_OF               = "Terms of Service"
    
    var txtColorValue : String = ""
    var txtColorAboutValue : String = ""
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setFireBase()
        setUI()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        sizeHeaderToFit()
    }

    // MARK: - All Functions
    
    func setUI() {
        btnSignOut.layer.cornerRadius = 5.0
        
        navView.backgroundColor = TOP_BLUE_COLOR
        //tblSettings.tableFooterView = viewFooter
    }
    
    func setFireBase() {
        self.setupDefaultConfig()
        self.fetchRemoteConfig()
    }
    
    func hexStringToUIColor(hex:String) -> UIColor {
        var cString:String = hex.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet() as NSCharacterSet).uppercaseString
        if (cString.hasPrefix("#")) {
            cString = cString.substringFromIndex(cString.startIndex.advancedBy(1))
        }
        if ((cString.characters.count) != 6) {
            return UIColor.grayColor()
        }
        var rgbValue:UInt32 = 0
        NSScanner(string: cString).scanHexInt(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func setupDefaultConfig() {
        let defaultValues = [
            "settingTxt"              : "Settings",
            "settingTxtName"          : "Name",
            "settingTxtBirthday"      : "Birthday",
            "settingTxtMobileNumber"  : "Mobile Number",
            "settingTxtEmail"         : "Email",
            "privacyPolicyText"       : "Privacy Policy",
            "termsofServiceText"      : "Terms of Service",
            "aboutUsTextColor"        : "#3BB9F9",
            "aboutUsBackgroundColor"  : "#ffffff",
            "settingTxtColor"         : "#000000",
            "settingTxtSignColor"     : "#ffffff",
            "settingSignOutBackgColor": "#dc2932",
            "aboutUsText"             : "About Us",
            "aboutUsHeaderColor"      : "#ffffff",
            ]
        FIRRemoteConfig.remoteConfig().setDefaults(defaultValues)
        afterFireBaseCall()
    }
    
    func fetchRemoteConfig() {
        var expirationDuration : NSTimeInterval = 3600
        if FIRRemoteConfig.remoteConfig().configSettings.isDeveloperModeEnabled {
            expirationDuration = 0.0
        }
        
        FIRRemoteConfig.remoteConfig().fetchWithExpirationDuration(expirationDuration, completionHandler: {(status, error) -> Void in
            if (error == nil) {
                print("Config fetched!",status)
                self.afterFireBaseCall()
                self.sizeHeaderToFit()
                FIRRemoteConfig.remoteConfig().activateFetched()
            }
            else {
                print("Config not fetched")
                print("Error \(error!.localizedDescription)")
            }
        })
    }
    
    func afterFireBaseCall() {
        let afterFireBase   = FIRRemoteConfig.remoteConfig()
        self.NAME           = afterFireBase.configValueForKey("settingTxtName").stringValue!
        self.BIRTHDAY       = afterFireBase.configValueForKey("settingTxtBirthday").stringValue!
        self.MOBILE_NO      = afterFireBase.configValueForKey("settingTxtMobileNumber").stringValue!
        self.EMAIL          = afterFireBase.configValueForKey("settingTxtEmail").stringValue!
        self.PRIVACY_POLICY = afterFireBase.configValueForKey("privacyPolicyText").stringValue!
        self.TERMS_OF       = afterFireBase.configValueForKey("termsofServiceText").stringValue!
        self.txtColorValue  = afterFireBase.configValueForKey("settingTxtColor").stringValue!
        //self.lblTitle.text  = afterFireBase.configValueForKey("settingTxt").stringValue!
        let strSignOut      = afterFireBase.configValueForKey("settingSignOut").stringValue!
        
        let aboutUsText        = afterFireBase.configValueForKey("aboutUsText").stringValue
        let privacyPolicyText  = afterFireBase.configValueForKey("privacyPolicyText").stringValue
        let termsofServiceText = afterFireBase.configValueForKey("termsofServiceText").stringValue
        //let aboutUsTextData    = afterFireBase.configValueForKey("aboutUsTextData").stringValue
        self.txtColorAboutValue  = afterFireBase.configValueForKey("aboutUsTextColor").stringValue!

        let txtViewCertified   = afterFireBase.configValueForKey("aboutUsTxtViewCertified").stringValue
        let txtViewAddress1    = afterFireBase.configValueForKey("aboutUsTxtViewAddress1").stringValue
        let txtViewAddress2    = afterFireBase.configValueForKey("aboutUsTxtViewAddress2").stringValue
        let txtViewMap         = afterFireBase.configValueForKey("aboutUsTxtViewMap").stringValue
        let txtViewPhone       = afterFireBase.configValueForKey("aboutUsTxtViewPhone").stringValue
        let txtViewBz          = afterFireBase.configValueForKey("aboutUsTxtViewBz").stringValue
        let txtViewDesc        = afterFireBase.configValueForKey("aboutUsTxtViewDesc").stringValue

        let btnSignBackColor = afterFireBase.configValueForKey("settingSignOutBackgColor").stringValue!
        let btnSignTxtColor  = afterFireBase.configValueForKey("settingTxtSignColor").stringValue!
        let backgroundColorValue     = afterFireBase.configValueForKey("aboutUsBackgroundColor").stringValue!
        let litleColor          = afterFireBase.configValueForKey("aboutUsTextColor").stringValue!
        
        self.view.backgroundColor    = hexStringToUIColor(backgroundColorValue)
        //self.navView.backgroundColor = hexStringToUIColor(backgroundColorValue)


        self.view.backgroundColor = hexStringToUIColor(backgroundColorValue)
        //let headerColorValue = afterFireBase.configValueForKey("aboutUsHeaderColor").stringValue!
        //self.navView.backgroundColor = hexStringToUIColor(headerColorValue)
        
        let txtColor             = hexStringToUIColor(litleColor)
        self.lblTitle.textColor  = txtColor

        if IS_IPAD {
            self.lblTitleIpad.textColor  = txtColor
            self.lblTitleIpad.text = aboutUsText
        } else {
            self.lblTitle.textColor  = txtColor
            self.lblTitle.text = aboutUsText
        }

        btnSignOut.setTitle(strSignOut, forState: UIControlState.Normal)
        self.btnSignOut.backgroundColor = hexStringToUIColor(btnSignBackColor)
        self.btnSignOut.titleLabel!.textColor = hexStringToUIColor(btnSignTxtColor)
        
        setAboutUI(privacyPolicyText!, strTermsofServiceText: termsofServiceText!, strTxtViewCertified : txtViewCertified!, strTxtViewAddress1 : txtViewAddress1!, strTxtViewAddress2 : txtViewAddress2!, strTxtViewMap : txtViewMap!, strTxtViewPhone : txtViewPhone!, strTxtViewBz : txtViewBz!, strTxtViewDesc : txtViewDesc!)

    }
    
    func setAboutUI(strPrivacyPolicyText : String , strTermsofServiceText : String, strTxtViewCertified : String , strTxtViewAddress1 : String, strTxtViewAddress2 : String, strTxtViewMap : String, strTxtViewPhone : String, strTxtViewBz : String, strTxtViewDesc : String) {
        
        //rowsArray.removeAll()
        
        let txtColorCertrified         = hexStringToUIColor(txtColorAboutValue)
        let txtColorAddress1           = hexStringToUIColor(txtColorAboutValue)
        let txtColorAddress2           = hexStringToUIColor(txtColorAboutValue)
        let txtColorMap                = hexStringToUIColor(txtColorAboutValue)
        let txtColorPhone              = hexStringToUIColor(txtColorAboutValue)
        let txtColorBz                 = hexStringToUIColor(txtColorAboutValue)
        let txtColorDesc               = hexStringToUIColor(txtColorAboutValue)
        
        if IS_IPAD {
            self.txtViewCertifiedIpad.textColor = txtColorCertrified
            self.txtViewCertifiedIpad.text      = strTxtViewCertified
            
            self.txtViewAddress1Ipad.text      = strTxtViewAddress1
            self.txtViewAddress1Ipad.textColor = txtColorAddress1
            
            self.txtViewAddress2Ipad.textColor = txtColorAddress2
            self.txtViewAddress2Ipad.text      = strTxtViewAddress2
            
            self.txtViewMapIpad.textColor      = txtColorMap
            self.txtViewMapIpad.text           = strTxtViewMap
            
            self.txtViewPhoneIpad.textColor    = txtColorPhone
            self.txtViewPhoneIpad.text         = strTxtViewPhone
            
            self.txtViewBzIpad.textColor       = txtColorBz
            self.txtViewBzIpad.text            = strTxtViewBz
            
            self.txtViewDetailsIpad.textColor  = txtColorDesc
            self.txtViewDetailsIpad.text       = strTxtViewDesc
        } else {
            self.txtViewCertified.textColor = txtColorCertrified
            self.txtViewCertified.text      = strTxtViewCertified
            
            self.txtViewAddress1.text      = strTxtViewAddress1
            self.txtViewAddress1.textColor = txtColorAddress1
            
            self.txtViewAddress2.textColor = txtColorAddress2
            self.txtViewAddress2.text      = strTxtViewAddress2
            
            self.txtViewMap.textColor      = txtColorMap
            self.txtViewMap.text           = strTxtViewMap
            
            self.txtViewPhone.textColor    = txtColorPhone
            self.txtViewPhone.text         = strTxtViewPhone
            
            self.txtViewBz.textColor       = txtColorBz
            self.txtViewBz.text            = strTxtViewBz
            
            self.txtViewDetails.textColor  = txtColorDesc
            self.txtViewDetails.text       = strTxtViewDesc
        }
        
        loadUserData()
        
        //let row0: [String : AnyObject] = ["placeholder": strPrivacyPolicyText, "value":"" ]
        //let row1: [String : AnyObject] = ["placeholder": strTermsofServiceText, "value":"" ]
        //
        //let tempArray = [row0, row1]
        //for i in 0 ..< tempArray.count {
        //let temp = Row().initWithDictionary(tempArray[i])
        //rowsArray.append(temp)
        //}
        //tblSettings.separatorInset  = UIEdgeInsetsZero
        //tblSettings.layoutMargins   = UIEdgeInsetsZero
        //tblSettings.tableFooterView = UIView()
        //tblSettings.reloadData()
    }
    
    func loadUserData() {
        rowsArray.removeAll()
        let row0: [String : AnyObject] = ["placeholder": NAME, "value": CommonMethods.sharedInstance.userModel?.name ?? ""]
        let row1: [String : AnyObject] = ["placeholder": BIRTHDAY,"value": CommonMethods.sharedInstance.userModel?.birthDay ?? ""]
        let row2: [String : AnyObject] = ["placeholder": MOBILE_NO,"value": CommonMethods.sharedInstance.userModel?.mobileNumber ?? ""]
        let row3: [String : AnyObject] = ["placeholder": EMAIL,"value": CommonMethods.sharedInstance.userModel?.email ?? ""]
        let row4: [String : AnyObject] = ["placeholder": PRIVACY_POLICY, "value": ""]
        let row5: [String : AnyObject] = ["placeholder": TERMS_OF,"value": ""]
        
        let tempArray = [row0, row1, row2, row3, row4, row5]
        for i in 0 ..< tempArray.count {
            let temp = Row().initWithDictionary(tempArray[i])
            rowsArray.append(temp)
        }
        
        tblSettings.separatorInset  = UIEdgeInsetsZero
        tblSettings.layoutMargins   = UIEdgeInsetsZero
        tblSettings.tableHeaderView = UIView()
        tblSettings.reloadData()
    }

    func sizeHeaderToFit() {
        if IS_IPAD {
            txtViewDetailsIpad.layoutIfNeeded()
            
            viewFooterIpad.setNeedsLayout()
            viewFooterIpad.layoutIfNeeded()
            
            let contentSize = txtViewDetailsIpad.intrinsicContentSize()
            let height: CGFloat = contentSize.height
            var frame: CGRect = viewFooterIpad.frame
            frame.size.height = height + 300
            viewFooterIpad.frame = frame
            
            tblSettings.tableFooterView = viewFooterIpad
        } else {
            txtViewDetails.layoutIfNeeded()
            viewFooter.setNeedsLayout()
            viewFooter.layoutIfNeeded()
            
            let contentSize = txtViewDetails.intrinsicContentSize()
            let height: CGFloat = contentSize.height
            var frame: CGRect = viewFooter.frame
            frame.size.height = height + 230
            viewFooter.frame = frame
            
            tblSettings.tableFooterView = viewFooter
        }
        tblSettings.tableHeaderView = UIView()
    }
    
    // MARK: - UITableView Methods and Functions
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowsArray.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return IS_IPAD ? 60 : 44
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "settingsCell")
        let curRow = rowsArray[indexPath.row]
        
        cell.accessoryType = .DisclosureIndicator
        
        if (curRow.placeholder == EMAIL) {
            cell.accessoryType = .None
        }
        cell.textLabel?.font = kSystemRegularFonts(17.0)
        cell.detailTextLabel?.font = kSystemRegularFonts(17.0)
        
        cell.textLabel?.text = curRow.placeholder
        cell.textLabel?.textColor = hexStringToUIColor(self.txtColorValue)
        cell.detailTextLabel?.text = curRow.value
        cell.detailTextLabel?.textColor = BLUE_COLOR
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let curRow = rowsArray[indexPath.row]
        switch curRow.placeholder {
        case NAME:self.openEditProfile(curRow.value, placeHolder: curRow.placeholder)
        case BIRTHDAY:self.openEditProfile(curRow.value, placeHolder: curRow.placeholder)
        case MOBILE_NO:self.openEditProfile(curRow.value, placeHolder: curRow.placeholder)
        case PRIVACY_POLICY:openPrivacyPolicy()
        case TERMS_OF:openTermsAndConditions()
        default:print("something wrong")
            
        }
    }
    func openEditProfile(value:String,placeHolder:String){
        let editCtrl = EditProfileCtrl(nibName: "EditProfileCtrl", bundle: nil)
        editCtrl.value = value
        editCtrl.placeHolder = placeHolder
        editCtrl.completionHandler = {
            self.loadUserData()
        }
        self.navigationController?.pushViewController(editCtrl, animated: true)
    }
    
    func openPrivacyPolicy() {
        let viewCtrl = WebviewCtrl(nibName: "WebviewCtrl", bundle: nil)
        viewCtrl.strURL = PRIVACY_POLICY_URL
        viewCtrl.titleStr = PRIVACY_POLICY
        self.navigationController?.pushViewController(viewCtrl, animated: true)
    }
    
    func openTermsAndConditions() {
        let viewCtrl = WebviewCtrl(nibName: "WebviewCtrl", bundle: nil)
        viewCtrl.strURL = TERMS_URL
        viewCtrl.titleStr = TERMS_OF
        self.navigationController?.pushViewController(viewCtrl, animated: true)
    }
    
    // MARK: - IBAction Methods and Functions
    
    @IBAction func btnSignOutClicked(_: AnyObject) {
        let  alertController = UIAlertController(title: APP_NAME, message:"Are you sure you want to Sign Out?", preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addAction(UIAlertAction(title: "Yes", style: .Default, handler: { (action: UIAlertAction!) in
            
            CommonMethods.sharedInstance.setUserState(USER_STATE.NOT_LOGGED_IN.rawValue)
            
            //Firebase Logout
            try! FIRAuth.auth()!.signOut()
            
            //Facebook Logout
            let loginManager = FBSDKLoginManager()
            loginManager.logOut()
            
            //Google Logout
            GIDSignIn.sharedInstance().signOut()
            
            //Twitter Logout
            let store = Twitter.sharedInstance().sessionStore
            if let userID = store.session()?.userID {
                store.logOutUserID(userID)
            }
            
            CommonMethods.sharedInstance.popToLast(LoginCtrl.classForCoder(), fromNavigationController: (appDel?.navCtrl)!, animated: false)
        }))
        
        alertController.addAction(UIAlertAction(title: "No", style: .Default, handler: { (action: UIAlertAction!) in
        }))
        
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
