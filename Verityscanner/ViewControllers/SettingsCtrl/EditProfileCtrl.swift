//
//  EditProfileCtrl.swift
//  Verityscanner
//
//  Created by Hardik Dineshchandra Mistry on 31/08/16.
//  Copyright © 2016 certified. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class EditProfileCtrl: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var btnSave: UIButton!
    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var datePicker : UIDatePicker!
    
    var completionHandler : (() -> ())?
    
    let NAME                   = "Name"
    let BIRTHDAY               = "Birthday"
    let MOBILE_NO              = "Mobile Number"
    
    var placeHolder: String = ""
    var value: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navView.addDropShadowToView()
        self.textField.addLeftPaddingView()
        
        textField.layer.cornerRadius = 5.0
        
        self.textField.placeholder = self.placeHolder
        self.textField.text = self.value
        
        if placeHolder == BIRTHDAY {
            textField.inputView = datePicker
            self.datePicker.datePickerMode = .Date
            textField.addDoneOnKeyboardWithTarget(self, action: #selector(EditProfileCtrl.doneClicked))
        } else if (placeHolder == MOBILE_NO) {
            textField.keyboardType = .PhonePad
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        self.textField.becomeFirstResponder()
    }
    
    // MARK: - IBAction Methods and Functions
    
    func isValid() -> Bool {
        switch  self.placeHolder {
        case NAME:
            if textField.text!.characters.count == 0 {
                CommonMethods.sharedInstance.showAlertWithMessage("Please enter name.", withController: self)
                return false
            }
        case BIRTHDAY:
            if textField.text!.characters.count == 0 {
                CommonMethods.sharedInstance.showAlertWithMessage("Please select your birthday.", withController: self)
                return false
            }
        case MOBILE_NO:
            if textField.text!.characters.count == 0 {
                CommonMethods.sharedInstance.showAlertWithMessage("Please enter mobile number.", withController: self)
                return false
            }
        default: print("Something wrong")
        }
        return true
    }
    
    @IBAction func btnSaveClicked(_: AnyObject) {
        if isValid() {
            var parameters = [String:String]()
            switch  self.placeHolder {
            case NAME:
                parameters = ["userId":CommonMethods.sharedInstance.userModel!.userId,
                              "name":textField!.text!,
                              "birthDay":CommonMethods.sharedInstance.userModel!.birthDay,
                              "mobileNumber":CommonMethods.sharedInstance.userModel!.mobileNumber]
            case BIRTHDAY:
                let dateStr = CommonMethods.sharedInstance.nsStringFromDate(datePicker.date, andToFormatString:DATE_FORMAT)
                parameters = ["userId":CommonMethods.sharedInstance.userModel!.userId,
                              "name":CommonMethods.sharedInstance.userModel!.name,
                              "birthDay":dateStr,
                              "mobileNumber":CommonMethods.sharedInstance.userModel!.mobileNumber]
            case MOBILE_NO:
                parameters = ["userId":CommonMethods.sharedInstance.userModel!.userId,
                              "name":CommonMethods.sharedInstance.userModel!.name,
                              "birthDay":CommonMethods.sharedInstance.userModel!.birthDay,
                              "mobileNumber":textField!.text!]
            default: print("Something wrong")
            }
            
            print(parameters)
            
            self.getData(UPDATE_URL, withParameter: parameters) { (result) in
                if (result["success"].numberValue == 1) {
                    
                    if let userData = result["profile"].dictionary {
                        let myuser = User().initWithDictionary(userData)
                        CommonMethods.sharedInstance.userModel = myuser
                        CommonMethods.sharedInstance.setUserState(USER_STATE.LOGGED_IN.rawValue)
                        CommonMethods.sharedInstance.saveUser()
                        CommonMethods.sharedInstance.hideHud()
                        self.completionHandler!()
                        self.navigationController?.popViewControllerAnimated(true)
                    } else {
                        CommonMethods.sharedInstance.hideHud()
                        CommonMethods.sharedInstance.showAlertWithMessage(INVALID_RESPONSE, withController: self)
                    }
                } else {
                    CommonMethods.sharedInstance.hideHud()
                    if let message = result["message"].string {
                        CommonMethods.sharedInstance.showAlertWithMessage(message, withController: self)
                    } else {
                        CommonMethods.sharedInstance.showAlertWithMessage(INVALID_RESPONSE, withController: self)
                    }
                }
            }
        }
    }
    
    func doneClicked() {
        textField.resignFirstResponder()
        let dateStr = CommonMethods.sharedInstance.nsStringFromDate(datePicker.date, andToFormatString:DATE_FORMAT)
        textField.text = dateStr
    }
    
    @IBAction func datePickerPicked(_:UIDatePicker) {
        let dateStr = CommonMethods.sharedInstance.nsStringFromDate(datePicker.date, andToFormatString:DATE_FORMAT)
        textField.text = dateStr
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
