//
//  PopUpScanCtrl.swift
//  Verityscanner
//
//  Created by Chinkal on 5/22/17.
//  Copyright © 2017 certified. All rights reserved.
//

import UIKit
import Firebase
import ZBarSDK
import PopupController

extension ZBarSymbolSet: SequenceType {
    public func generate() -> NSFastGenerator {
        return NSFastGenerator(self)
    }
}

class PopUpScanCtrl: UIViewController , PopupContentViewController, ZBarReaderDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet var btnScan        : UIButton!
    @IBOutlet var btnDetection   : UIButton!
    @IBOutlet var viewMain       : UIView!
    
    var isProductScanned : Bool = false
    var isScanStart      : Bool = false
    var isScanBarcode    : Bool = false
    var isBackHome       : Bool = false
    
    var binaryImageData  : String = ""
    var strEmpty         : String = ""
    
    var popupctrl    : UIPopoverController? = nil
    var imagePicker  : UIImagePickerController? = nil
    var ZBarReader   : ZBarReaderViewController?
    
    var closeHandler: ((str : String) -> Void)?
    
    // MARK: - UIView Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagePicker = UIImagePickerController()
        self.imagePicker?.delegate = self
        setUI()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if (NSUserDefaults.standardUserDefaults().valueForKey(AUTOSCAN) as! Bool) == true {
            self.isScanStart = true
        } else {
            self.isScanStart = false
        }
    }
    
    // MARK: - PopController Methods and Functions
    
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSizeMake((SCREEN_WIDTH-40), 101)
    }
    
    // MARK: - All Functions
    
    func setUI() {
        self.viewMain.layer.cornerRadius = 10
        self.viewMain.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.8)
        
    }
    
    @IBAction func btnBarCodeScanAction(sender: UIButton) {
        //Firebase Log Event
        FIRAnalytics.logEventWithName(SCAN_BUTTON_EVENT, parameters: [
            "name": SCAN_BUTTON_EVENT_NAME,
            "full_text": ""
            ])
        
        if (self.ZBarReader == nil) {
            self.ZBarReader = ZBarReaderViewController()
        }
        self.isScanBarcode = true
        self.ZBarReader?.readerDelegate = self
        self.ZBarReader?.scanner.setSymbology(ZBAR_UPCA, config: ZBAR_CFG_ENABLE, to: 1)
        self.ZBarReader?.readerView.zoom = 1.0
        self.ZBarReader?.modalInPopover = false
        self.ZBarReader?.showsZBarControls = false
        navigationController?.pushViewController(self.ZBarReader!, animated:true)
    }
    
    @IBAction func btnOCRScanAction(sender: UIButton) {
        self.imagePicker!.allowsEditing = false
        
        let alert = UIAlertController(title: APP_NAME, message: nil , preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let libButton = UIAlertAction(title: "Photo Album", style: UIAlertActionStyle.Default) { (alert) -> Void in
            self.imagePicker!.sourceType = .PhotoLibrary
            self.imagePicker!.editing = false
            self.presentViewController(self.imagePicker!, animated: true, completion: nil)
        }
        
        if (UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
            let cameraButton = UIAlertAction(title: "Take a picture", style: UIAlertActionStyle.Default) { (alert) -> Void in
                self.imagePicker!.sourceType = .Camera
                self.imagePicker!.editing = false
                self.presentViewController(self.imagePicker!, animated: true, completion: nil)
            }
            alert.addAction(cameraButton)
        } else {
            print("Camera not available")
        }
        
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) { (alert) -> Void in
        }
        if (IS_IPAD) {
            // show action sheet
            alert.popoverPresentationController!.sourceRect = sender.frame;
            alert.popoverPresentationController!.sourceView = sender;
        }
        alert.addAction(libButton)
        alert.addAction(cancelButton)
        self.presentViewController(alert, animated: true,completion: nil)
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        
        if isScanBarcode == true {
            guard let symbols = info[ZBarReaderControllerResults] as? ZBarSymbolSet else { return }
            for symbol in symbols {
                if let symbol = symbol as? ZBarSymbol, let strBarcodeNumber = symbol.data {
                    // Now you can use data (String)
                    print(strBarcodeNumber)
                    if self.isScanStart == true {
                        print(self.isProductScanned)
                        if self.isProductScanned == false {
                            self.isProductScanned = true
                            self.getServiceDetail(strBarcodeNumber)
                        }
                    }
                }
            }
        } else {
            if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
                
                // Base64 encode the image and create the request
                self.binaryImageData = base64EncodeImage(pickedImage)
                CommonMethods.sharedInstance.showHudWithTitle(STR_LOADING)
                
                let ctrl = OCRScanCtrl(nibName: "OCRScanCtrl", bundle: nil)
                ctrl.strBinary = self.binaryImageData
                ctrl.scanImgUrl = pickedImage
                self.navigationController?.pushViewController(ctrl, animated: true)
                self.closeHandler!(str: self.strEmpty)
            }
        }
        imagePicker!.dismissViewControllerAnimated(true, completion: { _ in })
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func resizeImage(newSize: CGSize, toSize image: UIImage) -> NSData {
        UIGraphicsBeginImageContext(newSize)
        image.drawInRect(CGRectMake(0, 0, newSize.width, newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        let resizedImage = UIImagePNGRepresentation(newImage!)
        UIGraphicsEndImageContext()
        return resizedImage!
    }
    
    /// Networking
    
    func base64EncodeImage(image: UIImage) -> String {
        var imagedata = UIImagePNGRepresentation(image)
        
        // Resize the image if it exceeds the 2MB API limit
        if imagedata!.length > 2097152 {
            let oldSize = image.size
            let newSize = CGSizeMake(800, oldSize.height / oldSize.width * 800)
            imagedata = resizeImage(newSize, toSize: image)
        }
        
        let base64String = imagedata!.base64EncodedStringWithOptions(NSDataBase64EncodingOptions.EncodingEndLineWithCarriageReturn)
        return base64String
    }
    
    func getServiceDetail(str:String) {
        
        //Firebase Log Event
        FIRAnalytics.logEventWithName(SCANING_EVENT, parameters: [
            "name": SCANING_EVENT_NAME,
            "full_text": ""
            ])
        
        dispatch_async(dispatch_get_main_queue(),{
            self.getDataGet(CHECK_PRODUCT_URL + str , withParameter: nil) { (result) in
                if result["status"].stringValue == "true" {
                    let ctrl = SearchCtrl(nibName: "SearchCtrl", bundle: nil)
                    ctrl.searchText =  str
                    ctrl.completionHandler = {
                        self.isProductScanned = false
                    }
                    self.navigationController?.pushViewController(ctrl, animated: true)
                    self.closeHandler!(str: self.strEmpty)
                    
                } else {
                    let ctrl = UPCCtrl(nibName: "UPCCtrl", bundle: nil)
                    ctrl.searchText =  str
                    self.isProductScanned = false
                    self.isBackHome = true
                    ctrl.isBackHome = self.isBackHome
                    self.navigationController?.pushViewController(ctrl, animated: true)
                    self.closeHandler!(str: self.strEmpty)
                }
            }
        })
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}
