//
//  AllUserListCtrl.swift
//  Verityscanner
//
//  Created by harmis on 28/07/17.
//  Copyright © 2017 certified. All rights reserved.
//

import UIKit

class AllUserListCtrl: UIViewController {
    
    @IBOutlet var tblAllUsers : UITableView!
    
    var aryUsers    = [QBUUser]()
    
    var dialog      : QBChatDialog?
    var users       : [QBUUser] = []
    var dataSource  : UsersDataSource!
    var session     : QBRTCSession?
    
    var strSelectedEmail : String = ""
    
    let AllUserListCellIdentifier = "AllUserListCell"
    
    // MARK: - View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.retrieveAllUsersFromPage(1)
        setUI()
    }
    
    // MARK: - All Functions
    
    func retrieveAllUsersFromPage(page: UInt) {
        QBRequest.usersForPage(QBGeneralResponsePage(currentPage: page, perPage: 100), successBlock: { (response, QBGeneralResponsePage, users) in
            self.aryUsers = users!
            for i in 0 ..< users!.count {
                let removeEmail = users![i]
                if CommonMethods.sharedInstance.userModel?.email == "adam@certified.bz" {
                    if (removeEmail.login == "adam@certified.bz") {
                        self.aryUsers.removeAtIndex(i)
                        print(self.aryUsers.count)
                    }
                }
            }
            self.tblAllUsers.reloadData()
        }) { (error) in
            print(error)
        }
    }
    
    func setUI() {
        tblAllUsers.registerNib(UINib(nibName: AllUserListCellIdentifier, bundle: nil), forCellReuseIdentifier: AllUserListCellIdentifier)
    }
    
    // MARK: - Chat All Functions
    
    func openChat(strEmail : String) {
        appDel!.connectUserWithQuickbloxChat()
        if (CommonMethods.sharedInstance.isConnected) {
            CommonMethods.sharedInstance.showHudWithTitle(STR_LOADING)
            self.getUsersWithID(strEmail)
            appDel?.registerForRemoteNotificationChat()
        } else {
            CommonMethods.sharedInstance.showAlertWithMessage(NO_INTERNET, withController: self)
        }
    }
    
    func getUsersWithID(strEmail : String) {
        ServicesManager.instance().usersService.getUsersWithLogins([strEmail]).continueWithBlock { (task: BFTask) -> AnyObject? in
            print(task.result?.count)
            print(task)
            if task.result?.count ?? 0 > 0 {
                guard let users = ServicesManager.instance().sortedUsers() else {
                    print("No cached users")
                    return nil
                }
                self.setupUsers(users, strEmail: strEmail)
            } else {
                CommonMethods.sharedInstance.showHudWithTitle("SA_STR_LOADING_USERS".localized)
                // Downloading users from Quickblox.
                ServicesManager.instance().downloadCurrentEnvironmentUsers({ (users) -> Void in
                    guard let unwrappedUsers = users else {
                        CommonMethods.sharedInstance.hideHud()
                        CommonMethods.sharedInstance.showAlertWithMessage("No users downloaded", withController: self)
                        return
                    }
                    CommonMethods.sharedInstance.showAlertWithMessage("SA_STR_COMPLETED".localized, withController: self)
                    self.setupUsers(unwrappedUsers, strEmail: strEmail)
                    CommonMethods.sharedInstance.hideHud()
                    }, errorBlock: { (error) -> Void in
                        CommonMethods.sharedInstance.hideHud()
                        CommonMethods.sharedInstance.showAlertWithMessage(error.localizedDescription, withController: self)
                })
            }
            return nil
        }
    }
    
    func setupUsers(users: [QBUUser], strEmail : String) {
        var aryUser = [QBUUser]()
        for i in 0 ..< users.count {
            let userValue = users[i]
            print(userValue.login)
            if strEmail == userValue.login {
                aryUser.append(userValue)
            }
        }
        print(aryUser)
        self.users = aryUser
        self.createChatCtrl(strEmail)
    }
    
    func createChatCtrl(strEmail : String) {
        let completion = {[weak self] (response: QBResponse?, createdDialog: QBChatDialog?) -> Void in
            if createdDialog != nil {
                print(createdDialog)
                self?.openNewDialog(createdDialog, strEmail: strEmail)
            }
            guard let unwrappedResponse = response else {
                print("Error empty response")
                return
            }
            if let error = unwrappedResponse.error {
                print(error.error)
                SVProgressHUD.showInfoWithStatus(error.error?.localizedDescription)
            } else {
                SVProgressHUD.showSuccessWithStatus("STR_DIALOG_CREATED".localized)
            }
        }
        self.createChat(nil, users: self.users, completion: completion)
        CommonMethods.sharedInstance.hideHud()
    }
    
    func createChat(name: String?, users:[QBUUser], completion: ((response: QBResponse?, createdDialog: QBChatDialog?) -> Void)?) {
        SVProgressHUD.showWithStatus("SA_STR_LOADING".localized, maskType: SVProgressHUDMaskType.Clear)
        print(users)
        ServicesManager.instance().chatService.createPrivateChatDialogWithOpponent(users.first!, completion: { (response: QBResponse?, chatDialog: QBChatDialog?) -> Void in
            completion?(response: response, createdDialog: chatDialog)
        })
    }
    
    func openNewDialog(dialog: QBChatDialog!, strEmail : String) {
        CommonMethods.sharedInstance.hideHud()
        let ctrl = ChatViewCtrl(nibName: "ChatViewCtrl", bundle: nil)
        ctrl.dialog = dialog
        ctrl.strReceiverEmail = strEmail
        self.navigationController?.pushViewController(ctrl, animated: true)
    }
    
    
    // MARK: - UITableView Methods And Functions
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.aryUsers.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(AllUserListCellIdentifier, forIndexPath: indexPath) as! AllUserListCell
        cell.selectionStyle = .None
        let curRow = self.aryUsers[indexPath.row]
        cell.lblName.text = curRow.fullName
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let curRow = self.aryUsers[indexPath.row]
        self.openChat(curRow.login!)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
