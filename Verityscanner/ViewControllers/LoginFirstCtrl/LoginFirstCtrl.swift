//
//  LoginFirstCtrl.swift
//  Verityscanner
//
//  Created by Chinkal on 8/23/17.
//  Copyright © 2017 certified. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FBSDKLoginKit
import FBSDKCoreKit
import Accounts
import GoogleSignIn
import TwitterKit
import SafariServices
import Alamofire
import SwiftyJSON
import Quickblox
import QuickbloxWebRTC

enum SOCIAL_ACC : Int {
    case GOOGLE   = 0
    case FACEBOOK = 1

}

class LoginFirstCtrl: UIViewController, FBSDKLoginButtonDelegate, GIDSignInUIDelegate, GIDSignInDelegate {

    // UIView Lifecycle methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if (CommonMethods.sharedInstance.usrState == USER_STATE.LOGGED_IN.rawValue) {
            if (CommonMethods.sharedInstance.userModel?.socialType == "0") {
                GIDSignIn.sharedInstance().signInSilently()
            } else {
                let viewCtrl = HomeCtrl(nibName: "HomeCtrl", bundle: nil)
                self.navigationController?.pushViewController(viewCtrl, animated: true)
            }
        }
    }

    // MARK: - All Functions
    
    func setUI() {
        GIDSignIn.sharedInstance().delegate   = self
        GIDSignIn.sharedInstance().uiDelegate = self
    }
    
    // MARK: - IBAction methods and functions

    @IBAction func btnSigninAction(sender: UIButton) {
        let viewCtrl = LoginCtrl(nibName: "LoginCtrl", bundle: nil)
        self.navigationController?.pushViewController(viewCtrl, animated: false)
    }
    
    @IBAction func btnToSAction(sender: UIButton) {
    }
    
    // MARK: - Facebook Login Delegate Methods
    
    @IBAction func btnFacebookAction(sender: UIButton) {
        //Firebase Log Event
        FIRAnalytics.logEventWithName(FB_LOGIN_EVENT, parameters: [
            "name": FB_LOGIN_EVENT_NAME,
            "full_text": ""
            ])
        
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logInWithReadPermissions(["email", "user_birthday", "public_profile"], fromViewController: self) { (result, error) -> Void in
            
            CommonMethods.sharedInstance.showHudWithTitle(STR_LOADING)
            
            if let isCancelled = result?.isCancelled {
                if isCancelled {
                    CommonMethods.sharedInstance.hideHud()
                    return
                }
            }
            
            if error != nil {
                CommonMethods.sharedInstance.hideHud()
                CommonMethods.sharedInstance.showAlertWithMessage(error.localizedDescription, withController: self)
                return
            }
            
            guard let currentAccessToken = FBSDKAccessToken.currentAccessToken()?.tokenString,
                let credential : FIRAuthCredential? = FIRFacebookAuthProvider.credentialWithAccessToken(currentAccessToken)
                else {
                    CommonMethods.sharedInstance.hideHud()
                    CommonMethods.sharedInstance.showAlertWithMessage("Facebook access token not available.", withController: self)
                    return
            }
            
            self.getFacebookUserDetailsFromAccesstoken(currentAccessToken, credential: credential)
        }
    }
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        CommonMethods.sharedInstance.showHudWithTitle(STR_LOADING)
        
        if result.isCancelled {
            CommonMethods.sharedInstance.hideHud()
            return
        }
        
        if error != nil {
            CommonMethods.sharedInstance.hideHud()
            CommonMethods.sharedInstance.showAlertWithMessage(error.localizedDescription, withController: self)
            return
        }
        
        guard let currentAccessToken = FBSDKAccessToken.currentAccessToken()?.tokenString,
            let credential : FIRAuthCredential? = FIRFacebookAuthProvider.credentialWithAccessToken(currentAccessToken)
            else {
                CommonMethods.sharedInstance.hideHud()
                CommonMethods.sharedInstance.showAlertWithMessage("Facebook access token not available.", withController: self)
                return
        }
        
        getFacebookUserDetailsFromAccesstoken(currentAccessToken, credential: credential)
        
    }
    
    func getFacebookUserDetailsFromAccesstoken(accessToken:String, credential:FIRAuthCredential?) {
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email, birthday"])
        
        graphRequest.startWithCompletionHandler { (comnnection, object, error) in
            if error != nil {
                CommonMethods.sharedInstance.hideHud()
                CommonMethods.sharedInstance.showAlertWithMessage(error.localizedDescription, withController: self)
                return
            }
            
            if object != nil {
                let userDetails = NSMutableDictionary()
                userDetails.setValue(SOCIAL_ACC.FACEBOOK.rawValue, forKey: "socialType")
                userDetails.setValue("", forKey: "profileImage")
                
                if let imageURL = object.valueForKey("picture")?.valueForKey("data")?.valueForKey("url") as? String {
                    //Download image from imageURL
                    userDetails.setValue(imageURL, forKey: "profileImage")
                }
                
                userDetails.setValue( object["name"] ?? "", forKey: "name")
                userDetails.setValue(object["email"] ?? "", forKey: "email")
                userDetails.setValue(SOCIAL_ACC.FACEBOOK.rawValue, forKey: "socialType")
                userDetails.setValue("YES", forKey: "isSocial")
                userDetails.setValue("", forKey: "password")
                userDetails.setValue("", forKey: "birthDay")
                userDetails.setValue("", forKey: "mobileNumber")
                userDetails.setValue("", forKey: "firebaseId")
                self.loginWithFirebase(credential!, forAccount: SOCIAL_ACC.FACEBOOK.rawValue, userDetails:userDetails )
                print("Facebook user details = \(userDetails)")
            } else {
                CommonMethods.sharedInstance.hideHud()
                CommonMethods.sharedInstance.showAlertWithMessage("Facebook Error: unable to get user details.", withController: self)
            }
        }
    }
    
    func loginButtonWillLogin(loginButton: FBSDKLoginButton!) -> Bool {
        return true
    }
    
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!) {
        print("User logged out of facebook")
    }
    
    // MARK: - Google Login Delegate Methods
    
    
    @IBAction func btnGmailAction(sender: UIButton) {
        //Firebase Log Event
        FIRAnalytics.logEventWithName(GOOGLE_LOGIN_EVENT, parameters: [
            "name": GOOGLE_LOGIN_EVENT_NAME,
            "full_text": ""
            ])
        GIDSignIn.sharedInstance().signIn()
    }
    
    func signIn(signIn: GIDSignIn!, didSignInForUser user: GIDGoogleUser!, withError error: NSError?) {
        CommonMethods.sharedInstance.showHudWithTitle(STR_LOADING)
        
        if error != nil {
            CommonMethods.sharedInstance.hideHud()
            if (error!.code != -5) {
                CommonMethods.sharedInstance.showAlertWithMessage(error!.localizedDescription, withController: self)
            }
            return
        }
        
        guard let googleUser = user,
            let  authentication : GIDAuthentication? = googleUser.authentication,
            let credential : FIRAuthCredential? = FIRGoogleAuthProvider.credentialWithIDToken(authentication!.idToken,accessToken: authentication!.accessToken)
            else {
                CommonMethods.sharedInstance.hideHud()
                CommonMethods.sharedInstance.showAlertWithMessage("Google Error: unable to get user details.", withController: self)
                return
        }
        
        let userDetails = NSMutableDictionary()
        let profileImage = user.profile.imageURLWithDimension(400).absoluteString
        userDetails.setValue(user.profile.name, forKey: "name")
        userDetails.setValue(user.profile.email ?? "", forKey: "email")
        userDetails.setValue(SOCIAL_ACC.GOOGLE.rawValue, forKey: "socialType")
        userDetails.setValue(profileImage, forKey: "profileImage")
        userDetails.setValue("", forKey: "password")
        userDetails.setValue("", forKey: "birthDay")
        userDetails.setValue("", forKey: "mobileNumber")
        userDetails.setValue("YES", forKey: "isSocial")
        userDetails.setValue("", forKey: "firebaseId")
        
        self.loginWithFirebase(credential!, forAccount: SOCIAL_ACC.GOOGLE.rawValue, userDetails:userDetails )
        print("Google user details = \(userDetails)")
    }
    
    func signIn(signIn: GIDSignIn!, didDisconnectWithUser user: GIDGoogleUser!, withError error: NSError!) {
    }
    
    func signIn(signIn: GIDSignIn!, presentViewController viewController: UIViewController!) {
        self.presentViewController(viewController, animated: true, completion: nil)
    }
    
    func signIn(signIn: GIDSignIn!, dismissViewController viewController: UIViewController!) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func signInWillDispatch(signIn: GIDSignIn!, error: NSError!) {
        CommonMethods.sharedInstance.hideHud()
    }

    // MARK: - Login with Firebase and CRM
    
    func loginWithFirebase(credential:FIRAuthCredential, forAccount:Int, userDetails:NSDictionary?) {
        if (isValidUserDetails(userDetails!)) {
            FIRAuth.auth()?.signInWithCredential(credential) { (user, error) in
                if error != nil {
                    CommonMethods.sharedInstance.hideHud()
                    CommonMethods.sharedInstance.showAlertWithMessage(error!.localizedDescription, withController: self)
                    return
                }
                self.userWithFireBaseUser(user, forAccount: forAccount, userDetails: userDetails)
            }
        } else {
            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                CommonMethods.sharedInstance.hideHud()
            })
        }
    }
    
    func userWithFireBaseUser(firUser:FIRUser?, forAccount : Int, userDetails:NSDictionary?) {
        //Call CRM login api
        if (CommonMethods.sharedInstance.isConnected) {
            CommonMethods.sharedInstance.showHudWithTitle(STR_LOADING)
            print(userDetails)
            Alamofire.request(.POST, SIGNIN_URL, parameters: userDetails! as? [String : AnyObject], encoding:.JSON).validate().responseJSON { response in
                print("--------- Request URL - %@", response.request?.URL)
                CommonMethods.sharedInstance.hideHud()
                
                switch response.result {
                case .Success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        print("JSON: \(json)")
                        if (json["success"].numberValue == 1) {
                            if let userData = json["profile"].dictionary {
                                let myuser = User().initWithDictionary(userData)
                                CommonMethods.sharedInstance.userModel = myuser
                                CommonMethods.sharedInstance.setUserState(USER_STATE.LOGGED_IN.rawValue)
                                CommonMethods.sharedInstance.saveUser()
                                CommonMethods.sharedInstance.hideHud()
                                self.CallQuickBloxLogin()
                                let ctrl = HomeCtrl(nibName: "HomeCtrl", bundle: nil)
                                self.navigationController?.pushViewController(ctrl, animated: true)
                            }
                        } else {
                            if let message = json["message"].string {
                                CommonMethods.sharedInstance.showAlertWithMessage(message, withController: self)
                            } else {
                                CommonMethods.sharedInstance.showAlertWithMessage(INVALID_RESPONSE, withController: self)
                            }
                        }
                    }
                case .Failure(let error):
                    print(error)
                    
                    if let data = response.data {
                        print("Response data: \(NSString(data: data, encoding: NSUTF8StringEncoding)!)")
                        //let strError = String(data: data, encoding: NSUTF8StringEncoding)!
                        CommonMethods.sharedInstance.showAlertWithMessage(error.localizedDescription, withController: self)
                    }
                }
            }
        } else {
            CommonMethods.sharedInstance.showAlertWithMessage(NO_INTERNET, withController: self)
        }
    }

    func isValidUserDetails(userDetails:NSDictionary) -> Bool {
        let email = userDetails["email"] as! String
        if email.characters.count == 0 {
            CommonMethods.sharedInstance.showAlertWithMessage("Please give access to your email address for login.", withController: self)
            return false
        }
        return true
    }

    func CallQuickBloxLogin() {
        let newUser = QBUUser()
        newUser.login = CommonMethods.sharedInstance.userModel?.email
        newUser.password = KQB_USER_DEFAULT_PASSWORD
        newUser.fullName = CommonMethods.sharedInstance.userModel?.name
        newUser.tags = ["Verity"]
        QBRequest.signUp(newUser, successBlock: { (response, user) in
            print(response)
            appDel!.connectUserWithQuickbloxChat()
            print("user Successfully login in chat.")
        }) { (error) in
            if error.error?.error?.localizedDescription == "Request failed: client error (422)" {
                print("user already exists in quick blox database.")
                appDel!.connectUserWithQuickbloxChat()
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
