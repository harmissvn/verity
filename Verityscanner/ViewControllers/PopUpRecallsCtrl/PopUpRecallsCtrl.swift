//
//  PopUpRecallsCtrl.swift
//  Verityscanner
//
//  Created by Chinkal on 6/1/17.
//  Copyright © 2017 certified. All rights reserved.
//

import UIKit
import PopupController

class PopUpRecallsCtrl: UIViewController, PopupContentViewController {
    
    @IBOutlet var viewMain            : UIView!
    @IBOutlet var btnUSDARecalls      : UIButton!
    @IBOutlet var btnAllRecallsRSS    : UIButton!
    @IBOutlet var btnChildrenRecall   : UIButton!
    
    var strEmpty     : String = ""
    
    var closeHandler: ((str : String) -> Void)?
    
    var popupctrl    : UIPopoverController? = nil
    
    // MARK: - UIView Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    // MARK: - PopController Methods and Functions
    
    func sizeForPopup(popupController: PopupController, size: CGSize, showingKeyboard: Bool) -> CGSize {
        return CGSizeMake((SCREEN_WIDTH-40), 152)
    }
    
    // MARK: - All Functions
    
    func setUI() {
        self.viewMain.layer.cornerRadius = 10
        self.viewMain.backgroundColor = UIColor.whiteColor().colorWithAlphaComponent(0.8)
        
    }
    
    // MARK: - IBActions and Functions
    
    @IBAction func btnUSDARecallsAction(sender: UIButton) {
        let ctrl = USDACtrl(nibName: "USDACtrl", bundle: nil)
        ctrl.strFeedType = "usda"
        ctrl.strTitle = "USDA Recall"
        self.navigationController?.pushViewController(ctrl, animated: true)
        self.closeHandler!(str: self.strEmpty)
    }
    
    @IBAction func btnAllRecallsRSSAction(sender: UIButton) {
        let ctrl = USDACtrl(nibName: "USDACtrl", bundle: nil)
        ctrl.strFeedType = "allrecall"
        ctrl.strTitle = "All Recalls RSS"
        self.navigationController?.pushViewController(ctrl, animated: true)
        self.closeHandler!(str: self.strEmpty)
    }
    
    @IBAction func btnChildrenRecallAction(sender: UIButton) {
        let ctrl = USDACtrl(nibName: "USDACtrl", bundle: nil)
        ctrl.strFeedType = "children"
        ctrl.strTitle = "Recalls for Children"
        self.navigationController?.pushViewController(ctrl, animated: true)
        self.closeHandler!(str: self.strEmpty)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
