//
//  USDACtrl.swift
//  Verityscanner
//
//  Created by Chinkal on 5/19/17.
//  Copyright © 2017 certified. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import DZNEmptyDataSet

class USDACtrl: UIViewController, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    @IBOutlet weak var navView      : UIView!
    @IBOutlet weak var lblTitle     : UILabel!
    @IBOutlet weak var btnBack      : UIButton!
    
    @IBOutlet var tblView           : UITableView!
    
    var intSelectIndex : Int = 0
    var strFeedType    : String = ""
    var strTitle       : String = ""
    
    var request: Alamofire.Request?
    var aryUSDA = [USDA]()
    
    let cellIdentifier = "USDACell"
    
    // MARK: - UIView Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        callUSDAApi()
    }
    
    // MARK: - All Functions
    
    func setUI() {
        tblView.estimatedRowHeight = 108.0
        tblView.rowHeight = UITableViewAutomaticDimension
        tblView.registerNib(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
    
        self.lblTitle.text = self.strTitle
    }
    
    func callUSDAApi() {
        if (CommonMethods.sharedInstance.isConnected) {
            CommonMethods.sharedInstance.showHudWithTitle(STR_LOADING)

            let parameters = [
                "feedtype"   : strFeedType
            ]
            
            print(parameters)
 
            self.request = Alamofire.request(.POST, GET_RSS_FEED_URL, parameters: parameters , encoding:.JSON).validate().responseJSON { response in
                print("--------- Request URL - %@", response.request?.URL)
                CommonMethods.sharedInstance.hideHud()

                switch response.result {
                case .Success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        print("JSON: \(json)")
                        if (json["success"].numberValue == 1) {
                            if let resultArray = json["RssFeeds"].array {
                                for i in 0 ..< resultArray.count {
                                    let curDic = resultArray[i].dictionary
                                    let temp = USDA().initWithDictionary(curDic!)
                                    self.aryUSDA.append(temp)
                                }
                            }
                        } else {
                            if let message = json["message"].string {
                                CommonMethods.sharedInstance.showAlertWithMessage(message, withController: self)
                            } else {
                                CommonMethods.sharedInstance.showAlertWithMessage(INVALID_RESPONSE, withController: self)
                            }
                        }
                    }
                case .Failure(let error):
                    print(error)
                    if let data = response.data {
                        print("Response data: \(NSString(data: data, encoding: NSUTF8StringEncoding)!)")
                        if error.code != NSURLErrorCancelled {
                            //let strError = String(data: data, encoding: NSUTF8StringEncoding)!
                            CommonMethods.sharedInstance.showAlertWithMessage(error.localizedDescription, withController: self)
                        }
                    }
                }
                dispatch_async(dispatch_get_main_queue()) {
                    self.tblView.reloadData()
                }
            }
        } else {
            CommonMethods.sharedInstance.showAlertWithMessage(NO_INTERNET, withController: self)
        }
    }
    
    // MARK: - UITableView Methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return aryUSDA.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! USDACell
        cell.selectionStyle = .None
        
        let curRow = aryUSDA[indexPath.row]
        
        cell.lblTitle.text = curRow.title
        cell.lblDesc.text = curRow.desc
        cell.lblDate.text = curRow.pubDate
        
        cell.setNeedsLayout()
        cell.setNeedsDisplay()
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.intSelectIndex = indexPath.row
        let curRow = aryUSDA[indexPath.row]
        let viewCtrl = USDADetailCtrl(nibName: "USDADetailCtrl", bundle: nil)
        viewCtrl.dicUSDA = curRow
        viewCtrl.strTitle = "\(strTitle) Detail"
        self.navigationController?.pushViewController(viewCtrl, animated: true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
