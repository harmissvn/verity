//
//  USDADetailCtrl.swift
//  Verityscanner
//
//  Created by Chinkal on 5/19/17.
//  Copyright © 2017 certified. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class USDADetailCtrl: UIViewController, UIWebViewDelegate, UIDocumentInteractionControllerDelegate {
    
    @IBOutlet var webView          : UIWebView!
    @IBOutlet var lblTitle         : UILabel!
    @IBOutlet var lblUSDATitle     : UILabel!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var btnBack          : UIButton!
    
    var strLink         : String = ""
    var strUSDATitle    : String = ""
    var strTitle        : String = ""

    var dicUSDA = USDA()
    
    // MARK: - UIView Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblTitle.text = strTitle
        self.lblUSDATitle.text = dicUSDA.title
        self.clearCache()
        webView.delegate = self
        let requestObj = NSURLRequest(URL: NSURL(string: dicUSDA.link)!)
        webView.loadRequest(requestObj)
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        webView.delegate = nil
        webView.stopLoading()
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        self.clearCache()
    }
    
    // MARK: - All Functions
    
    func clearCache() {
        // Remove and disable all URL Cache, but doesn't seem to affect the memory
        NSURLCache.sharedURLCache().removeAllCachedResponses()
        NSURLCache.sharedURLCache().diskCapacity = 0
        NSURLCache.sharedURLCache().memoryCapacity = 0
    }
    
    func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
    
    // MARK: - UIWebView Delegate Methods
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        activityIndicator.startAnimating()
        return true
    }
    
    func webViewDidFinishLoad(theWebView: UIWebView) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        activityIndicator.stopAnimating()
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        activityIndicator.startAnimating()
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        activityIndicator.stopAnimating()
    }
    
    // MARK: - IBActions and Functions
    
    @IBAction func btnBackAction(sender: UIButton) {
        if(webView.canGoBack) {
            webView.goBack()
        } else {
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            activityIndicator.stopAnimating()
            webView.stopLoading()
            webView.delegate = nil
            self.navigationController!.popViewControllerAnimated(true)
            self.clearCache()
        }
    }
    
    @IBAction func btnForwordAction(sender: UIButton) {
        webView.goForward()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
