//
//  RegisterCtrl.swift
//  Verityscanner
//
//  Created by Hardik Dineshchandra Mistry on 26/09/16.
//  Copyright © 2016 certified. All rights reserved.
//

import UIKit
import Firebase
import Quickblox
import Alamofire
import SwiftyJSON
import FBSDKLoginKit
import QuickbloxWebRTC
import IQKeyboardManagerSwift

class RegisterCtrl: UIViewController, UITextFieldDelegate, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var btnBack      : UIButton!
    @IBOutlet weak var btnRegister  : UIButton!
    @IBOutlet var btnCamera         : UIButton!
    @IBOutlet var btnProfile        : UIButton!
    
    @IBOutlet var imgProfile        : UIImageView!

    @IBOutlet weak var navView      : UIView!
    @IBOutlet weak var viewFooter   : UIView!
    @IBOutlet var viewHeader        : UIView!

    @IBOutlet weak var tblRegister  : UITableView!
    @IBOutlet weak var datePicker   : UIDatePicker!
    
    var rowsArray = [Row]()
    
    var imageChanged         : Bool = false

    let NAME                   = "Name"
    let BIRTHDAY               = "Birthday"
    let MOBILE_NO              = "Mobile Number"
    let EMAIL                  = "Email"
    let PASSWORD               = "Password"
    let CONFIRM_PASSWORD       = "Confirm Password"
    let REFERRAL_CODE          = "Referral Code"
    
    let cellIdentifier = "TFCell"
    
    var imagePicker          : UIImagePickerController? = nil
    var popover              : UIPopoverController!

    // MARK: - UIView Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        loadTableViewData()
        self.imagePicker = UIImagePickerController()
        self.imagePicker?.delegate = self

    }
    
    // MARK: - All Functions
    
    func setUI() {
        self.imgProfile.layer.cornerRadius = self.imgProfile.frame.size.width/2
        self.imgProfile.layer.borderWidth  = 1.0
        self.btnCamera.imageView?.contentMode = .Center
        
        self.btnCamera.layer.cornerRadius = self.btnCamera.frame.size.width/2
        self.btnCamera.layer.borderWidth  = 0.5
        self.btnCamera.layer.borderColor = UIColor.whiteColor().CGColor

        btnCamera.tintColor = UIColor.whiteColor()
        let cameraImg = UIImage.fontAwesomeIconWithCode("fa-camera", textColor: UIColor.whiteColor(), size: CGSize(width: 20, height: 20), backgroundColor: UIColor.blackColor())
        self.btnCamera.setImage(cameraImg, forState: .Normal)
    }

    func loadTableViewData() {
        btnRegister.layer.cornerRadius = 5.0
        btnRegister.addShadowView()
        tblRegister.tableFooterView = viewFooter
        tblRegister.tableHeaderView = viewHeader
        tblRegister.registerNib(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        
        rowsArray.removeAll()
        let row0: [String : AnyObject] = ["placeholder": NAME, "value": ""]
        let row1: [String : AnyObject] = ["placeholder": BIRTHDAY, "value": ""]
        let row2: [String : AnyObject] = ["placeholder": MOBILE_NO, "value": ""]
        let row3: [String : AnyObject] = ["placeholder": EMAIL, "value": ""]
        let row4: [String : AnyObject] = ["placeholder": PASSWORD, "value": ""]
        let row5: [String : AnyObject] = ["placeholder": CONFIRM_PASSWORD, "value": ""]
        let row6: [String : AnyObject] = ["placeholder": REFERRAL_CODE, "value": CommonMethods.sharedInstance.referralCode]
        
        let tempArray = [row0, row1, row2, row3, row4, row5, row6]
        for i in 0 ..< tempArray.count {
            let temp = Row().initWithDictionary(tempArray[i])
            rowsArray.append(temp)
        }
        tblRegister.reloadData()
    }
    
    func CallQuickBloxLogin() {
        let newUser = QBUUser()
        newUser.login = CommonMethods.sharedInstance.userModel?.email
        newUser.password = KQB_USER_DEFAULT_PASSWORD
        newUser.fullName = CommonMethods.sharedInstance.userModel?.name
        newUser.tags = ["Verity"]
        QBRequest.signUp(newUser, successBlock: { (response, user) in
            print("user Successfully login in chat.")
        }) { (error) in
            print("user not login in chat.")
        }
    }
    
    func openPhotoAlbum() {
        let controller = UIImagePickerController()
        controller.delegate = self
        controller.sourceType = .PhotoLibrary
        if UI_USER_INTERFACE_IDIOM() == .Pad {
            if self.popover.popoverVisible {
                self.popover.dismissPopoverAnimated(false)
            }
            self.popover = UIPopoverController(contentViewController: controller)
        }
        else {
            self.presentViewController(controller, animated: true, completion: { _ in })
        }
    }
    
    func showCamera() {
        let controller = UIImagePickerController()
        controller.delegate = self
        controller.sourceType = .Camera
        if UI_USER_INTERFACE_IDIOM() == .Pad {
            if self.popover.popoverVisible {
                self.popover.dismissPopoverAnimated(false)
            }
            self.popover = UIPopoverController(contentViewController: controller)
        }
        else {
            self.presentViewController(controller, animated: true, completion: { _ in })
        }
    }

    // MARK: - IBAction Methods And Functions

    @IBAction func btnCameraAction(sender: UIButton) {
        self.imagePicker!.allowsEditing = false
        let actionSheet = UIActionSheet(title: nil, delegate: self, cancelButtonTitle: nil, destructiveButtonTitle: nil, otherButtonTitles: ("Photo Album"))
        
        if UIImagePickerController.isSourceTypeAvailable(.Camera) {
            actionSheet.addButtonWithTitle("Camera")
        }
        actionSheet.addButtonWithTitle("Cancel")
        actionSheet.cancelButtonIndex = actionSheet.numberOfButtons - 1
        actionSheet.showFromToolbar(self.navigationController!.toolbar)
    }
    
    func actionSheet(actionSheet: UIActionSheet, clickedButtonAtIndex buttonIndex: Int) {
        let buttonTitle = actionSheet.buttonTitleAtIndex(buttonIndex)
        if (buttonTitle == "Photo Album") {
            self.openPhotoAlbum()
        } else if (buttonTitle == "Camera") {
            self.showCamera()
        }
    }
    
    // MARK: - UITableView Methods And Functions
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rowsArray.count
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! TFCell
        cell.selectionStyle = .None
        
        cell.textField.tag = indexPath.row
        cell.textField.keyboardType = .Default
        cell.textField.secureTextEntry = false
        cell.textField.delegate = self
        
        cell.textField.inputView = nil
        cell.textField.reloadInputViews()
        
        cell.textField.setCustomDoneTarget(self, action:#selector(RegisterCtrl.doneAction(_:)))
        
        let curRow = rowsArray[indexPath.row]
        
        if (curRow.placeholder == MOBILE_NO) {
            cell.textField.keyboardType = .PhonePad
        } else if (curRow.placeholder == BIRTHDAY) {
            cell.textField.inputView = datePicker
        }
        else if (curRow.placeholder == EMAIL) {
            cell.textField.keyboardType = .EmailAddress
        } else if (curRow.placeholder == PASSWORD || curRow.placeholder == CONFIRM_PASSWORD) {
            cell.textField.secureTextEntry = true
        }
        
        cell.textField?.placeholder = curRow.placeholder
        cell.textField?.text = curRow.value
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    // MARK: - IBAction Methods and Functions
    
    func isValid() -> Bool {
        let email: Row = rowsArray[3]
        let pass: Row = rowsArray[4]
        let confirmPass: Row = rowsArray[5]
        
        if email.value.characters.count == 0 {
            CommonMethods.sharedInstance.showAlertWithMessage("Please enter your email address.", withController: self)
            return false
        }
        else if !email.value.isValidEmail() {
            CommonMethods.sharedInstance.showAlertWithMessage("Please enter valid email address.", withController: self)
            return false
        }
        else if pass.value.characters.count == 0 {
            CommonMethods.sharedInstance.showAlertWithMessage("Please enter password.", withController: self)
            return false
        }
        else if confirmPass.value.characters.count == 0 {
            CommonMethods.sharedInstance.showAlertWithMessage("Please enter confirm password.", withController: self)
            return false
        }
        else if pass.value != confirmPass.value {
            CommonMethods.sharedInstance.showAlertWithMessage("Password does not match.", withController: self)
            return false
        }
        return true
    }
    
    func firebaseRegister(userDetails:NSDictionary) {
        let email = userDetails["email"] as! String
        let pass = userDetails["password"] as! String
        
        CommonMethods.sharedInstance.showHudWithTitle(STR_LOADING)
        
        FIRAuth.auth()?.createUserWithEmail(email, password: pass, completion: { (user:FIRUser?, error:NSError?) in
            if error != nil {
                CommonMethods.sharedInstance.hideHud()
                CommonMethods.sharedInstance.showAlertWithMessage(error!.localizedDescription, withController: self)
                return
            }
            self.callRegisterApi(userDetails)
        })
    }
    
    func callRegisterApi(userDetails:NSDictionary) {
        Alamofire.request(.POST, SIGNIN_URL, parameters: (userDetails as NSDictionary) as? [String : AnyObject], encoding:.JSON).validate().responseJSON { response in
            print("--------- Request URL - %@", response.request?.URL)
            CommonMethods.sharedInstance.hideHud()
            
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print("JSON: \(json)")
                    if (json["success"].numberValue == 1) {
                        if let userData = json["profile"].dictionary {
                            let myuser = User().initWithDictionary(userData)
                            CommonMethods.sharedInstance.userModel = myuser
                            CommonMethods.sharedInstance.setUserState(USER_STATE.LOGGED_IN.rawValue)
                            CommonMethods.sharedInstance.saveUser()
                            CommonMethods.sharedInstance.hideHud()
                            self.CallQuickBloxLogin()
                            let ctrl = HomeCtrl(nibName: "HomeCtrl", bundle: nil)
                            self.navigationController?.pushViewController(ctrl, animated: true)
                        }
                    } else {
                        if let message = json["message"].string {
                            CommonMethods.sharedInstance.showAlertWithMessage(message, withController: self)
                        } else {
                            CommonMethods.sharedInstance.showAlertWithMessage(INVALID_RESPONSE, withController: self)
                        }
                    }
                }
            case .Failure(let error):
                print(error)
                if let data = response.data {
                    print("Response data: \(NSString(data: data, encoding: NSUTF8StringEncoding)!)")
                    //let strError = String(data: data, encoding: NSUTF8StringEncoding)!
                    CommonMethods.sharedInstance.showAlertWithMessage(error.localizedDescription, withController: self)
                }
            }
        }
    }
    
    @IBAction func btnRegisterClicked(_: AnyObject) {
        if (isValid()) {
            if (CommonMethods.sharedInstance.isConnected) {
                let name        : Row = rowsArray[0]
                let birthday    : Row = rowsArray[1]
                let mobile      : Row = rowsArray[2]
                let email       : Row = rowsArray[3]
                let pass        : Row = rowsArray[4]
                let referralCode: Row = rowsArray[6]
                
                let userDetails = NSMutableDictionary()
                userDetails.setValue("", forKey: "socialType")
                userDetails.setValue(name.value, forKey: "name")
                userDetails.setValue(email.value , forKey: "email")
                userDetails.setValue("", forKey: "profileImage")
                userDetails.setValue("NO", forKey: "isSocial")
                userDetails.setValue(pass.value, forKey: "password")
                userDetails.setValue(birthday.value, forKey: "birthDay")
                userDetails.setValue(mobile.value, forKey: "mobileNumber")
                userDetails.setValue("", forKey: "firebaseId")
                userDetails.setValue(referralCode.value ?? "", forKey: "referalCode")
                firebaseRegister(userDetails)
            } else {
                CommonMethods.sharedInstance.showAlertWithMessage(NO_INTERNET, withController: self)
            }
        }
    }
    
    @IBAction func datePickerValueChanged(_: UIDatePicker) {
        let curRow = rowsArray[1]
        curRow.value = CommonMethods.sharedInstance.nsStringFromDate(datePicker.date, andToFormatString: NEW_DATE_FORMAT)
        
        let indexPath = NSIndexPath(forRow:1, inSection:0)
        let cell : TFCell? = tblRegister.cellForRowAtIndexPath(indexPath) as! TFCell?
        cell?.textField.text = curRow.value
    }
    
    func doneAction (textField : UITextField?) {
        if (textField?.tag == 1) {
            let curRow = rowsArray[1]
            curRow.value = CommonMethods.sharedInstance.nsStringFromDate(datePicker.date, andToFormatString: NEW_DATE_FORMAT)
            
            let indexPath = NSIndexPath(forRow:1, inSection:0)
            let cell : TFCell? = tblRegister.cellForRowAtIndexPath(indexPath) as! TFCell?
            cell?.textField.text = curRow.value
        }
    }
    
    // MARK: - UIImagePickerController Delegate Methods
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        self.imageChanged = true
        var imagenow = info[UIImagePickerControllerOriginalImage] as? UIImage
        imagenow = resizeImage(imagenow!, newWidth: 600)
        self.imgProfile.image = imagenow
        if UI_USER_INTERFACE_IDIOM() == .Pad {
            if self.popover.popoverVisible {
                self.popover.dismissPopoverAnimated(false)
            }
        } else {
            picker.dismissViewControllerAnimated(true, completion: {() -> Void in
            })
        }
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight))
        image.drawInRect(CGRectMake(0, 0, newWidth, newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }

    // MARK: - UITextFieldDelegate Methods
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    {
        if let swRange = textField.text!.rangeFromNSRange(range) {
            let newString = textField.text!.stringByReplacingCharactersInRange(swRange, withString: string)
            let curRow = rowsArray[textField.tag]
            curRow.value = newString as String
        }
        return true
    }
    
    func textFieldShouldClear(textField: UITextField) -> Bool {
        let curRow = rowsArray[textField.tag]
        curRow.value = "" as NSString as String
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}
