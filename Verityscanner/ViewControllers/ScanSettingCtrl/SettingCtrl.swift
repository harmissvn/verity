//
//  SettingCtrl.swift
//  Certified
//
//  Created by harmis on 07/07/16.
//  Copyright © 2016 harmis. All rights reserved.
//

import UIKit

class SettingCtrl: UIViewController {
    
    @IBOutlet var lblCountry : UILabel!
    @IBOutlet var lblLanguage : UILabel!
    @IBOutlet var switchAutoScan : UISwitch!
    
    var isFromScan = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(NSUserDefaults.standardUserDefaults().valueForKey(COUNTRY_CODE))
        switchAutoScan.on =  NSUserDefaults.standardUserDefaults().valueForKey(AUTOSCAN) as! Bool
    }
    
    // MARK: - IBAction Methods and Functions
    
    @IBAction func switchAutoScanClikced(_ : UISwitch){
        NSUserDefaults.standardUserDefaults().setBool(switchAutoScan.on, forKey: AUTOSCAN)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
