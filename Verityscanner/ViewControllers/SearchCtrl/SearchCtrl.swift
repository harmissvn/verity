//
//  SearchCtrl.swift
//  Verityscanner
//
//  Created by Hardik Dineshchandra Mistry on 29/08/16.
//  Copyright © 2016 certified. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AlamofireImage
import DZNEmptyDataSet
import Firebase

class SearchCtrl: UIViewController, UITextFieldDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate {
    
    @IBOutlet weak var btnTopStar : UIButton!
    @IBOutlet weak var tblSearch  : UITableView!
    @IBOutlet weak var tfSearch   : UITextField!
    @IBOutlet weak var viewHeader : UIView!
    @IBOutlet weak var adBannerView  : GADBannerView!

    var productsArray = [Product]()
    var request: Alamofire.Request?
    var total  :Int = -1
    var intSelectIndex : Int = -1
    
    let cellIdentifier = "ProductCell"
    
    var isLoading:Bool = false
    var searchText : String?
    var completionHandler : (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAdBanner() //Ad-Mob Setup

        // NSNotificationCenter For RateProductCtrl
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.receiveNotification), name: "addRatingNotification", object: nil)
        
        // NSNotificationCenter For SubmitReviewCtrl
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.receiveDicProductNotification), name: "addDicProductNotification", object: nil)
        
        //navView.addDropShadowToView()
        
        if searchText != nil {
            tfSearch.text = searchText!
            callSearchApi()
        }
        
        self.automaticallyAdjustsScrollViewInsets = false
        self.viewHeader.backgroundColor = TOP_BLUE_COLOR
        
        btnTopStar.tintColor = UIColor.whiteColor()
        let starImage = UIImage.fontAwesomeIconWithCode("fa-star-o", textColor: UIColor.whiteColor(), size: CGSize(width: 60, height: 60), backgroundColor: UIColor.clearColor())
        btnTopStar.setImage(starImage, forState: .Normal)

        tfSearch.layer.borderColor = UIColor.grayColor().CGColor
        tfSearch.layer.borderWidth = 1.0
        tfSearch.layer.cornerRadius = 5.0
        tfSearch.addLeftPadding()
        
        tblSearch.estimatedRowHeight = 44.0
        tblSearch.rowHeight = UITableViewAutomaticDimension
        
        tblSearch.registerNib(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
    }
    
    func setupAdBanner() {
        //#if RELEASE
        adBannerView.adUnitID = AD_MOB_UNIT_ID
        //#else
        //adBannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716" //AD_MOB_UNIT_ID
        //#endif
        adBannerView.rootViewController = self
        let request = GADRequest()
        request.testDevices = [kGADSimulatorID]
        adBannerView.loadRequest(request)
    }

    func receiveNotification(notification: NSNotification) {
        let notInfo = notification.object
        let curRating = notInfo as! Rating
        for i in 0 ..< self.productsArray.count {
            // let rating = Rating()
            let curValue = self.productsArray[i]
            if  curValue.productId == curRating.strId  {
                curValue.avgRating =  curRating.ratingValue
                curValue.isRate = "1"
            }
        }
        self.tblSearch.reloadData()
    }
    
    func receiveDicProductNotification(notification: NSNotification) {
        print(intSelectIndex)
        let notInfo = notification.object
        let curProduct = notInfo as? Product
        self.productsArray[intSelectIndex] = curProduct!
    }
    
    // MARK: - DZNEmptyDataSet Methods
    
    func emptyDataSetShouldDisplay(scrollView: UIScrollView!) -> Bool {
        return self.productsArray.count == 0 ? true : false
    }
    
    func emptyDataSetShouldAllowScroll(scrollView: UIScrollView!) -> Bool {
        return true
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        let attrs = [NSFontAttributeName : kSystemRegularFonts(21.0)]
        let messageStr = NSMutableAttributedString(string:"No results found.", attributes:attrs)
        return messageStr
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if (tfSearch.text?.characters.count == 0) {
            tfSearch.becomeFirstResponder()
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.request?.cancel()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
    }
    
    // MARK: - IBAction Methods and Functions
    
//    @IBAction func btnBackAction(sender: AnyObject) {
//        for controller: UIViewController in self.navigationController!.viewControllers {
//            //Do not forget to import AnOldViewController.h
//            if (controller is HomeCtrl) {
//                self.navigationController!.popToViewController(controller, animated: true)!
//            }
//        }
//    }
    
    @IBAction func btnSearhClicked(_: AnyObject) {
        tfSearch.resignFirstResponder()
        self.total = -1
        self.productsArray.removeAll()
        tblSearch.reloadData()
        callSearchApi()
    }
    
    func callSearchApi() {
        if (CommonMethods.sharedInstance.isConnected) {
            
            if (tfSearch.text == "") {
                CommonMethods.sharedInstance.showAlertWithMessage("Please enter product to search.", withController: self)
                return
            }
            
            if (self.productsArray.count == self.total) {
                return
            }
            
            isLoading = true
            tblSearch.reloadData()
            
            let parameters:NSMutableDictionary = NSMutableDictionary()
            parameters.setValue(tfSearch.text?.trim, forKey: "searchword")
            parameters.setValue(self.productsArray.count, forKey: "offset")
            parameters.setValue(CommonMethods.sharedInstance.userModel?.userId, forKey: "userId")
            
            print(parameters)
            
            var newProducts = [Product]()
            
            self.request = Alamofire.request(.POST, SEARCH_PRODUCT_URL, parameters: (parameters as NSDictionary) as? [String : AnyObject], encoding:.JSON).validate().responseJSON { response in
                print("--------- Request URL - %@", response.request?.URL)
                
                switch response.result {
                case .Success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        print("JSON: \(json)")
                        if (json["success"].numberValue == 1) {
                            
                            if json["total"].exists() {
                                self.total = json["total"].int!
                            }
                            
                            if let resultArray = json["result"].array {
                                for i in 0 ..< resultArray.count {
                                    let curDic = resultArray[i].dictionary
                                    let temp = Product().initWithDictionary(curDic!)
                                    newProducts.append(temp)
                                    if self.productsArray.contains(temp) {
                                        print("--------------------------DUPLICATE ID \(temp.productId)---------------------")
                                    }
                                    self.productsArray.append(temp)
                                }
                            }
                        } else {
                            print(123)
                            if let message = json["message"].string {
                                CommonMethods.sharedInstance.showAlertWithMessage(message, withController: self)
                            } else {
                                CommonMethods.sharedInstance.showAlertWithMessage(INVALID_RESPONSE, withController: self)
                            }
                        }
                    }
                case .Failure(let error):
                    print(error)
                    if let data = response.data {
                        print("Response data: \(NSString(data: data, encoding: NSUTF8StringEncoding)!)")
                        if error.code != NSURLErrorCancelled {
                            //let strError = String(data: data, encoding: NSUTF8StringEncoding)!
                            CommonMethods.sharedInstance.showAlertWithMessage(error.localizedDescription, withController: self)
                        }
                    }
                }
                
                self.isLoading = false
                self.tblSearch.emptyDataSetSource = self
                self.tblSearch.emptyDataSetDelegate = self
                
                dispatch_async(dispatch_get_main_queue()) {
                    self.tblSearch.reloadData()
                }
                
                
                /*
                 var indexPathsToDelete = [AnyObject]()
                 for object: Object in newObjects {
                 if !currentObjects.contains(object) {
                 var row = newObjects.indexOf(object)
                 var indexPath = NSIndexPath(forRow: row, inSection: 0)
                 indexPathsToDelete.append(indexPath)
                 }
                 }
                 */
                /*
                 var indexPathsToAdd = [NSIndexPath]()
                 for object: Product in newProducts {
                 //if !newObjects.contains(object) {
                 let row = self.productsArray.indexOf(object)
                 let indexPath = NSIndexPath(forRow: row!, inSection: 0)
                 indexPathsToAdd.append(indexPath)
                 //}
                 }
                 self.tblSearch.beginUpdates()
                 //tblSearch.deleteRowsAtIndexPaths(indexPathsToDelete, withRowAnimation: .Automatic)
                 self.tblSearch.insertRowsAtIndexPaths(indexPathsToAdd, withRowAnimation: .Automatic)
                 self.tblSearch.endUpdates()
                 */
            }
        } else {
            CommonMethods.sharedInstance.showAlertWithMessage(NO_INTERNET, withController: self)
        }
    }
    
    // MARK: - UITableView Methods
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isLoading ? productsArray.count + 1 : productsArray.count
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        let lastRowIndex = tableView.numberOfRowsInSection(0)
        if total != -1 && productsArray.count < total && !isLoading {
            if indexPath.row == lastRowIndex - 1 {
                self.callSearchApi()
                print("--------------------------------- Load More Api Called")
            }
        }
        
        if cell is ProductCell {
            //print("-------- Product cell found.")
            (cell as! ProductCell).lblUPCCode.font = kSystemRegularFonts(17.0)
            (cell as! ProductCell).lblProductName.font = kSystemRegularFonts(16.0)
            (cell as! ProductCell).lblCategory.font = kSystemRegularFonts(16.0)
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell!
        if indexPath.row == productsArray.count {
            //Last Row
            let loadMoreId = "LoadMoreCell"
            //cell = UITableViewCell(style: .Default, reuseIdentifier: "loadMoreId")
            if cell == nil {
                cell = UITableViewCell(style: .Default, reuseIdentifier: loadMoreId)
                cell.backgroundColor = UIColor.clearColor()
                cell.selectionStyle = .None
                
                let indicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
                indicator.tag = 99
                indicator.color = BLUE_COLOR
                indicator.backgroundColor = UIColor.clearColor()
                indicator.frame = CGRectMake(UIScreen.mainScreen().bounds.size.width / 2 - 10, 12.0, 20.0, 20.0)
                indicator.hidesWhenStopped = false
                indicator.startAnimating()
                cell.contentView.addSubview(indicator)
                
                let lblLoading = UILabel(frame: CGRectMake(0, 0, UIScreen.mainScreen().bounds.size.width, 44))
                lblLoading.tag = 100
                lblLoading.hidden = true
                lblLoading.textAlignment = .Center
                lblLoading.font = UIFont.systemFontOfSize(17.0)
                lblLoading.text = "Loading more..."
                lblLoading.textColor = BLUE_COLOR
                cell.contentView.addSubview(lblLoading)
            }
            
            let indicator = (cell.contentView.viewWithTag(99)! as! UIActivityIndicatorView)
            indicator.startAnimating()
            let lblLoading = (cell.contentView.viewWithTag(100)! as! UILabel)
            if productsArray.count > 0 {
                lblLoading.hidden = false
                indicator.hidden = true
            }
            else {
                lblLoading.hidden = true
                indicator.hidden = false
            }
        }
        else {
            cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! ProductCell
            cell.selectionStyle = .None
            (cell as! ProductCell).imgProduct.image = nil
            
            let curRow = productsArray[indexPath.row]
            
            let upcPlaceholder = "UPC : "
            let strUpcCode = upcPlaceholder + curRow.upcCode
            
            let range = (strUpcCode as NSString).rangeOfString(upcPlaceholder)
            let fullRange = (strUpcCode as NSString).rangeOfString(strUpcCode)
            let attributedUPCCode = NSMutableAttributedString(string:strUpcCode)
            attributedUPCCode.addAttribute(NSForegroundColorAttributeName, value: GREEN_COLOR , range: range)
            attributedUPCCode.addAttribute(NSFontAttributeName, value:kSystemRegularFonts(17.0), range: fullRange)
            (cell as! ProductCell).lblUPCCode.attributedText = attributedUPCCode
            
            (cell as! ProductCell).activityIndicator.startAnimating()
            
            dispatch_async(dispatch_get_main_queue()) {
                (cell as! ProductCell).lblProductName.text = curRow.productName
                (cell as! ProductCell).lblCategory.text = "Category : " + curRow.category
                
                cell.setNeedsLayout()
                cell.setNeedsDisplay()
            }
            //(cell as! ProductCell).lblProductName.font = kSystemRegularFonts(16.0)
            //(cell as! ProductCell).lblCategory.font = kSystemRegularFonts(16.0)
            
            if let url = NSURL(string: curRow.thumbImage) {
                (cell as! ProductCell).imgProduct.af_setImageWithURL(
                    url,
                    placeholderImage: UIImage.init(named: "noImageThumb"),
                    filter: nil,
                    imageTransition: .CrossDissolve(0.2),
                    completion: { response in
                        (cell as! ProductCell).activityIndicator.stopAnimating()
                    }
                )
            }
        }
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.intSelectIndex = indexPath.row
        print(self.intSelectIndex)
        let curRow = productsArray[indexPath.row]
        let viewCtrl = SearchDetailsCtrl(nibName: "SearchDetailsCtrl", bundle: nil)
        viewCtrl.curProduct = curRow
        self.navigationController?.pushViewController(viewCtrl, animated: true)
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.min
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.min
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - UITextField Methods and Functions
    
    func textField(textField: UITextField,shouldChangeCharactersInRange range: NSRange,replacementString string: String) -> Bool {
        if range.location == 0 && string == " " {
            return false
        }
        return true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.total = -1
        self.productsArray.removeAll()
        tblSearch.reloadData()
        callSearchApi()
        return true
    }
}
