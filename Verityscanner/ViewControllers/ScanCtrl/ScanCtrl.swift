//
//  ScanCtrl.swift
//  Certified
//
//  Created by harmis on 08/07/16.
//  Copyright © 2016 harmis. All rights reserved.
//

import UIKit
import RSBarcodes_Swift
import SwiftyJSON
import AVFoundation
import Firebase

class ScanCtrl: RSCodeReaderViewController {
    
    var isProductScanned : Bool = false
    var strProductCode = ""
    var isScanStart = false
    
    @IBOutlet var btnDoneScan : UIButton!
    @IBOutlet  var scanGif : UIImageView!
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        if (NSUserDefaults.standardUserDefaults().valueForKey(AUTOSCAN) as! Bool) == true {
            self.isScanStart = true
            self.scanGif.hidden = false
        } else {
            self.isScanStart = false
            self.scanGif.hidden = true
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let jeremyGif = UIImage.gifWithName("laser")
        scanGif.image = jeremyGif
        
        self.focusMarkLayer.strokeColor = UIColor.redColor().CGColor
        self.cornersLayer.strokeColor = UIColor.yellowColor().CGColor
        
        for subview in self.view.subviews {
            self.view.bringSubviewToFront(subview)
        }
        
        self.view.bringSubviewToFront(scanGif)
        self.barcodesHandler = { barcodes in
            if self.isScanStart == true {
                print(self.isProductScanned)
                if self.isProductScanned == false {
                    self.scanGif.hidden = true
                    self.isProductScanned = true
                    print(barcodes[0].stringValue)
                    self.getServiceDetail(barcodes[0].stringValue)
                }
            }
        }
    }
    
    // MARK: - IBAction Methods and Functions
    
    func getServiceDetail(str:String) {
        
        //Firebase Log Event
        FIRAnalytics.logEventWithName(SCANING_EVENT, parameters: [
            "name": SCANING_EVENT_NAME,
            "full_text": ""
            ])
        
        dispatch_async(dispatch_get_main_queue(),{
            self.getDataGet(CHECK_PRODUCT_URL + str , withParameter: nil) { (result) in
                if result["status"].stringValue == "true" {
                    let ctrl = SearchCtrl(nibName: "SearchCtrl", bundle: nil)
                    ctrl.searchText =  str
                    ctrl.completionHandler = {
                        self.isProductScanned = false
                        self.scanGif.hidden = false
                    }
                    self.navigationController?.pushViewController(ctrl, animated: true)
                    
                } else {
                    let ctrl = UPCCtrl(nibName: "UPCCtrl", bundle: nil)
                    ctrl.searchText =  str
                        self.isProductScanned = false
                        self.scanGif.hidden = false
                    self.navigationController?.pushViewController(ctrl, animated: true)
                    
                    //                    self.showAlertWithMessage("No Product Found.")
                    //                    self.isProductScanned = false
                    //                    self.scanGif.hidden = false
                }
            }
        })
    }
    
    @IBAction func btnDoneClikced() {
        isScanStart = true
    }
    
    @IBAction func closeScan() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func btnBottomSettingClicked() {
        dispatch_async(dispatch_get_main_queue(),{
            let ctrl = SettingCtrl(nibName: "SettingCtrl", bundle: nil)
            ctrl.isFromScan = true
            self.navigationController?.pushViewController(ctrl, animated: true)
        })
    }
    
    @IBAction func flashToggle() {
        let device = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        if (device.hasTorch) {
            do {
                try device.lockForConfiguration()
                if (device.torchMode == AVCaptureTorchMode.On) {
                    device.torchMode = AVCaptureTorchMode.Off
                } else {
                    try device.setTorchModeOnWithLevel(1.0)
                }
                device.unlockForConfiguration()
            } catch {
                print(error)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
