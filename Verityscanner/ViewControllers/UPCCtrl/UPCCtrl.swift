//
//  UPCCtrl.swift
//  Verityscanner
//
//  Created by harmis on 03/03/17.
//  Copyright © 2017 certified. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import Foundation
import MobileCoreServices
import FontAwesome_swift
import JVFloatLabeledTextField

class UPCCtrl: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet var viewUPC       : UIView!
    @IBOutlet var viewTitle     : UIView!
    
    @IBOutlet var txtUPC        : JVFloatLabeledTextField!
    @IBOutlet var txtTitle      : JVFloatLabeledTextField!
    
    @IBOutlet var btnSelectImg  : UIButton!
    @IBOutlet var btnSubmit     : UIButton!
    @IBOutlet var btnCancel     : UIButton!
    @IBOutlet var btnProfile    : UIButton!
    
    @IBOutlet var imgView       : UIImageView!
    @IBOutlet var heightImgView : NSLayoutConstraint!
    
    var imagePicker     : UIImagePickerController? = nil
    var searchText      : String?
    var imageChanged    : Bool = false
    var isBackHome      : Bool = false
    
    var completionHandler : (() -> ())?
    
    // MARK: - UIView Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.imagePicker = UIImagePickerController()
        self.imagePicker?.delegate = self
        setImage()
        setUI()
    }
    
    // MARK: - All Functions
    
    func setImage() {
        let cameraImage = UIImage.fontAwesomeIconWithCode("fa-camera", textColor: UIColor.blackColor(), size: CGSize(width: 30, height: 30), backgroundColor: UIColor.clearColor())
        btnSelectImg.setImage(cameraImage, forState: .Normal)
    }
    
    func setUI() {
        self.txtUPC.text = searchText
        self.btnSubmit.layer.cornerRadius = 3.0
        self.btnSubmit.addShadowView()
        
        self.btnCancel.layer.cornerRadius = 3.0
        self.btnCancel.addShadowView()
        
        self.btnSelectImg.layer.cornerRadius = 3.0
        self.btnSelectImg.addShadowView()
        
        viewTitle.layer.cornerRadius = 3.0
        viewTitle.layer.borderWidth = 1.0
        viewTitle.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        viewUPC.layer.cornerRadius = 3.0
        viewUPC.layer.borderWidth = 1.0
        viewUPC.layer.borderColor = UIColor.lightGrayColor().CGColor
    }
    
    func isValid() -> Bool {
        if self.imageChanged == false {
            CommonMethods.sharedInstance.showAlertWithMessage("Please select image.", withController: self)
            return false
        } else if self.txtTitle.text == "" {
            CommonMethods.sharedInstance.showAlertWithMessage("Please enter product title.", withController: self)
            return false
        }
        return true
    }
    
    func alertAction(strMessage : String) {
        let alertController = UIAlertController(title: APP_NAME, message: strMessage, preferredStyle:UIAlertControllerStyle.Alert)
        
        alertController.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default)
        { action -> Void in
            let ctrl = SearchCtrl(nibName: "SearchCtrl", bundle: nil)
            ctrl.searchText = self.txtUPC.text
            self.navigationController?.pushViewController(ctrl, animated: true)
            })
        self.presentViewController(alertController, animated: true, completion: nil)
    }
    
    // MARK: - IBAction Methods And Functions
    
    @IBAction func btnBackAction(sender: UIButton) {
        if isBackHome == true {
            for controller: UIViewController in self.navigationController!.viewControllers {
                if (controller is HomeCtrl) {
                    self.navigationController!.popToViewController(controller, animated: true)!
                }
            }
        } else {
            self.navigationController?.popViewControllerAnimated(true)
        }
    }
    
    @IBAction func btnSubmitAction(sender: UIButton) {
        if (isValid()) {
            if (CommonMethods.sharedInstance.isConnected) {
                CommonMethods.sharedInstance.showHudWithTitle("Loading...")
                /*
                 userId,
                 name,
                 specialization,
                 description,
                 state,
                 address,
                 lang,
                 profile_image (for shop logo image),
                 status
                 */
                
                let parameters = [
                    "UPCcode"    : self.txtUPC.text,
                    "productName": self.txtTitle.text,
                    "userId"     : CommonMethods.sharedInstance.userModel?.userId
                ]
                
                let multiPart = ["json_content" : CommonMethods.sharedInstance.JSONStringify((parameters as? [String : String])!)]
                
                print("Parameter", parameters)
                // Begin upload
                Alamofire.upload(.POST, ADD_NEW_PRODUCT_URL,
                                 multipartFormData: { multipartFormData in
                                    
                                    // import image to request
                                    
                                    if (self.imageChanged) {
                                        let imageFromImageView = self.imgView.image!
                                        if let imageData = UIImageJPEGRepresentation(imageFromImageView, 0.5) {
                                            multipartFormData.appendBodyPart(data: imageData, name: "productImage", fileName: "test.png", mimeType: "image/png")
                                        }
                                    }
                                    
                                    // import parameters
                                    
                                    for (key, value) in multiPart {
                                        multipartFormData.appendBodyPart(data: value.dataUsingEncoding(NSUTF8StringEncoding)!, name: key)
                                    }
                    }, // you can customise Threshold if you wish. This is the alamofire's default value
                    encodingMemoryThreshold: Manager.MultipartFormDataEncodingMemoryThreshold,
                    encodingCompletion: { encodingResult in
                        switch encodingResult {
                        case .Success(let upload, _, _):
                            upload.responseJSON { response in
                                if let data = response.data {
                                    print("Response data: \(NSString(data: data, encoding: NSUTF8StringEncoding)!)")
                                }
                                if let value = response.result.value {
                                    let json = JSON(value)
                                    print("JSON: \(json)")
                                    if (json["success"].numberValue == 1) {
                                        self.alertAction("Product Added Sucessfully.")
                                    } else {
                                        let message = json["message"].stringValue
                                        CommonMethods.sharedInstance.showAlertWithMessage(message, withController: self)
                                    }
                                } else {
                                    CommonMethods.sharedInstance.showAlertWithMessage((response.result.error?.localizedDescription)!, withController: self)
                                }
                                CommonMethods.sharedInstance.hideHud()
                            }
                        case .Failure(let encodingError):
                            CommonMethods.sharedInstance.hideHud()
                            print(encodingError)
                        }
                })
            } else {
                CommonMethods.sharedInstance.showAlertWithMessage(NO_INTERNET, withController: self)
            }
        }
    }
    
    @IBAction func btnSelectImgAction(sender: UIButton) {
        self.imagePicker!.allowsEditing = false
        
        let alert = UIAlertController(title: APP_NAME, message: nil , preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        let libButton = UIAlertAction(title: "Select photo from library", style: UIAlertActionStyle.Default) { (alert) -> Void in
            self.imagePicker!.sourceType = .PhotoLibrary
            self.imagePicker!.editing = false
            self.presentViewController(self.imagePicker!, animated: true, completion: nil)
        }
        
        if (UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)){
            let cameraButton = UIAlertAction(title: "Take a picture", style: UIAlertActionStyle.Default) { (alert) -> Void in
                self.imagePicker!.sourceType = .Camera
                self.imagePicker!.editing = false
                self.presentViewController(self.imagePicker!, animated: true, completion: nil)
            }
            alert.addAction(cameraButton)
        } else {
            NSLog("Camera not available")
        }
        
        let cancelButton = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel) { (alert) -> Void in
        }
        alert.addAction(libButton)
        alert.addAction(cancelButton)
        
        if (IS_IPAD) {
            // show action sheet
            alert.popoverPresentationController!.sourceRect = sender.frame;
            alert.popoverPresentationController!.sourceView = sender;
        }
        
        if UI_USER_INTERFACE_IDIOM() == .Phone {
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            let popup: UIPopoverPresentationController = alert.popoverPresentationController!
            popup.sourceView = sender.superview
            popup.sourceRect = sender.frame
            popup.permittedArrowDirections  = UIPopoverArrowDirection.Any
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    // MARK: - UIImagePickerController Delegate Methods
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        self.imageChanged = true
        let imagenow = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.imgView.image = resizeImage(imagenow!, newWidth: 300)
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight))
        image.drawInRect(CGRectMake(0, 0, newWidth, newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return newImage!
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
