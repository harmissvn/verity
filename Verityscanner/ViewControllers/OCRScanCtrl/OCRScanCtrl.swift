//
//  OCRScanCtrl.swift
//  Verityscanner
//
//  Created by Chinkal on 5/23/17.
//  Copyright © 2017 certified. All rights reserved.
//

import UIKit
import SwiftyJSON

class OCRScanCtrl: UIViewController {
    
    @IBOutlet var lblTitle          : UILabel!
    @IBOutlet var lblTextsResults   : UILabel!
    @IBOutlet var imgScan           : UIImageView!
    
    @IBOutlet var btnBack           : UIButton!
    @IBOutlet var viewResults       : UIView!
    
    var strBinary : String = ""
    var scanImgUrl : UIImage!
    
    // MARK: - UIView Life Cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        createRequest(strBinary)
    }
    
    // MARK: - All Functions

    func setUI(){
        self.imgScan.image = self.scanImgUrl
    }
    
    func createRequest(strBinary: String) {
        // Create our request URL
        let urlString = "https://vision.googleapis.com/v1/images:annotate?key="
        let API_KEY = "AIzaSyDQe2EsLCGVNvelPqHKuKj6IqEyG55x8bg"
        let requestString = "\(urlString)\(API_KEY)"
        let url = NSURL(string: requestString)!
        
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "POST"
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue(NSBundle.mainBundle().bundleIdentifier!, forHTTPHeaderField: "X-Ios-Bundle-Identifier")
        
        // Build our API request
        let jsonRequest = [
            "requests": [
                "image": [
                    "content": strBinary
                ],
                "features": [
                    [
                        "type": "TEXT_DETECTION",
                        "maxResults": 20
                    ],
                    [
                        "type": "LABEL_DETECTION",
                        "maxResults": 20
                    ],
                    [
                        "type": "FACE_DETECTION",
                        "maxResults": 20
                    ]
                ]
            ]
        ]
        
        let jsonObject = JSON(jsonRequest)
        
        // Serialize the JSON
        guard let data = try? jsonObject.rawData() else {
            return
        }
        
        request.HTTPBody = data
        
        // Run the request on a background thread
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {() -> Void in
            self.runRequestOnBackgroundThread(request)
        })
    }
    
    func runRequestOnBackgroundThread(request: NSMutableURLRequest) {
        // run the request
        let task = NSURLSession.sharedSession().dataTaskWithRequest(request) { (data, response, error) in
            guard let data = data where error == nil else {
                print(error?.localizedDescription ?? "")
                return
            }
            self.analyzeResults(data)
        }
        
        task.resume()
    }
    
    func analyzeResults(dataToParse: NSData) {
        
        // Update UI on the main thread
        dispatch_async(dispatch_get_main_queue()) {
            
            // Use SwiftyJSON to parse results
            let json = JSON(data: dataToParse)
            let errorObj: JSON = json["error"]
            
            CommonMethods.sharedInstance.hideHud()
            
            // Check for errors
            if (errorObj.dictionaryValue != [:]) {
                self.lblTextsResults.text = "Error code \(errorObj["code"]): \(errorObj["message"])"
            } else {
                // Parse the response
                print(json)
                let responses: JSON = json["responses"][0]
                
                // Get label annotations
                var textAnnotations: JSON  = (responses["textAnnotations"])
                let numTexts = textAnnotations.count
                var texts = [String]()
                if numTexts > 0 {
                    var textResults = "I found these things: \n \n"
                    for index in 0..<numTexts {
                        let text = textAnnotations[index]["description"].stringValue
                        texts.append(text)
                    }
                    for text in texts {
                        // if it's not the last item add a comma
                        if (texts[0] == text) {
                            
                            if texts[texts.count - 1] != text {
                                textResults += "\(text) "
                            } else {
                                textResults += "\(text)"
                            }
                            
                            print("index 0")
                        } else {
//                            if texts[texts.count - 1] != text {
//                                textResults += "\(text) "
//                            } else {
//                                textResults += "\(text)"
//                            }
                        }
                    }
                    self.lblTextsResults.text = textResults
                } else {
                    self.lblTextsResults.text = "No texts found"
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
