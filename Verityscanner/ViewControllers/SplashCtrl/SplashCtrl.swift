//
//  SplashCtrl.swift
//  Verityscanner
//
//  Created by harmis on 29/08/16.
//  Copyright © 2016 certified. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON
import FirebaseInstanceID
import FirebaseMessaging

class SplashCtrl: UIViewController, CLLocationManagerDelegate {
    
    @IBOutlet var gifImage : UIImageView!
    @IBOutlet var splashImage : UIImageView!
    
    var placesArray = [Place]()
    
    var isLocationUpdated:Bool = false
    var isLocationFailed:Bool = false
    
    let locationManager = CLLocationManager()
    var curLocation: CLLocation!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        isLocationUpdated = false
        
        //Set gif loader
        let jeremyGif = UIImage.gifWithName("GIFVerity")
        gifImage.image = jeremyGif
        
        //Initialize location service
        self.getLocation()
    }
    
    // MARK: - Location Methods and Delegates
    
    func getLocation() {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            if (isLocationUpdated == false) {
                print("\n---------------------- Found user's location: \(location)")
                print("\n-------Horizontal Accuracy \(location.horizontalAccuracy)")
                
                if (location.horizontalAccuracy > 0.0 && location.horizontalAccuracy != 1414.0) {
                    // test that the horizontal accuracy does not indicate an invalid measurement
                    manager.stopUpdatingLocation()
                    isLocationUpdated = true
                    self.curLocation = location
                    self.getAllNearByPlaces()
                }
            }
        }
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        print("---------------------- Failed to find user's location: \(error.localizedDescription)")
        manager.stopUpdatingLocation()
        if (isLocationFailed == false) {
            isLocationFailed = true
            setSplashWithName("normal")
        }
    }
    
    // TODO: Please, finish implementation

    
    // MARK: - UI Update methods
    
    func runAfterDelay(delay: NSTimeInterval, block: dispatch_block_t) {
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay * Double(NSEC_PER_SEC)))
        dispatch_after(time, dispatch_get_main_queue(), block)
    }
    
    func setSplashWithName(name:String) {
        splashImage.image = UIImage.init(named: name)
        UIView.animateWithDuration(0.5, delay: 0.5, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.splashImage.alpha = 1.0
            self.gifImage.alpha = 0.0
        }, completion: nil)
        
        runAfterDelay(3) { 
            self.openLoginScreen()
        }
    }
    
    func openLoginScreen() {
        let viewCtrl = LoginFirstCtrl(nibName: "LoginFirstCtrl", bundle: nil)
        self.navigationController?.pushViewController(viewCtrl, animated: false)
    }
    
    // MARK: - API calls and related methods
    
    func getAllNearByPlaces() {
        if (CommonMethods.sharedInstance.isConnected) {
            
            let dispatchGroup: dispatch_group_t = dispatch_group_create()
            let types = [strKroger, strCostco, strPublix, strWawa, strWalmart, strTrader, strThefresh, strWholefoods, strStartBucks, strChipotle, strAlfalfas, strCosmoProf, strCvsPharmacy, strDunkinDonuts, strGNCLiveWell, strPaneraBread, strPetSupermarket, strSaloncentric, strWalgreens]
            
            for type in types {
                //Enter group and run request
                dispatch_group_enter(dispatchGroup)
                self.getNearByStore(type, group: dispatchGroup)
            }
            
            dispatch_group_notify(dispatchGroup, dispatch_get_main_queue(), {
                
                if (self.placesArray.count > 0) {
                    self.placesArray.sortInPlace({ $0.distance < $1.distance })
                    
                    //let distanceArray = self.placesArray.map({$0.distance! as Double})
                    for place in self.placesArray {
                        print("\n-------------------Places sorted by distane = \(place.jsonObject())")
                    }
                    
                    var splashName = "normal"
                    
                    switch self.placesArray[0].place {
                    case strCostco: splashName = "costco"
                    case strThefresh: splashName = "freshMarket"
                    case strKroger: splashName = "krogermart"
                    case strPublix: splashName = "publix"
                    case strTrader: splashName = "traderjoes"
                    case strWalmart: splashName = "walmart"
                    case strWawa: splashName = "wawa"
                    case strWholefoods: splashName = "wholefoods"
                    case strStartBucks: splashName = "starbucks"
                    case strChipotle: splashName = "chipotle"
                        
                    case strAlfalfas: splashName = "alfalfas"
                    case strCosmoProf: splashName = "cosmoProf"
                    case strCvsPharmacy: splashName = "cvsPharmacy"
                    case strDunkinDonuts: splashName = "dunkinDonuts"
                    case strGNCLiveWell: splashName = "gncLiveWell"
                    case strPaneraBread: splashName = "paneraBread"
                    case strPetSupermarket: splashName = "petSupermarket"
                    case strSaloncentric: splashName = "salonCentric"
                    case strWalgreens: splashName = "walgreens"
                        
                    default:break
                    }
                    self.setSplashWithName(splashName)
                } else {
                    self.setSplashWithName("normal")
                }
            });
        }
        else {
            self.setSplashWithName("normal")
        }
    }
    
    func getNearByStore(place:String, group: dispatch_group_t) {
        //Live Location
        let curLattitude = self.curLocation.coordinate.latitude
        let curLongiture = self.curLocation.coordinate.longitude
        
        //Static Location for testing
        //let curLattitude = 26.3679835
        //let curLongiture = -80.0765358
        
        var strUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(curLattitude),\(curLongiture)&radius=\(DISTANCE_METERS)&type=store&name=\(place)&sensor=true&key=\(GOOGLE_PLACE_KEY)"
        
        if (place == strChipotle) {//Set type = restaurant
            strUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(curLattitude),\(curLongiture)&radius=\(DISTANCE_METERS)&type=restaurant&name=\(place)&sensor=true&key=\(GOOGLE_PLACE_KEY)"
        } else if (place == strCvsPharmacy) { //Set type = pharmacy
            strUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(curLattitude),\(curLongiture)&radius=\(DISTANCE_METERS)&type=pharmacy&name=\(place)&sensor=true&key=\(GOOGLE_PLACE_KEY)"
        } else if (place == strDunkinDonuts || place == strPaneraBread) { //Set type = food
            strUrl = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=\(curLattitude),\(curLongiture)&radius=\(DISTANCE_METERS)&type=food&name=\(place)&sensor=true&key=\(GOOGLE_PLACE_KEY)"
        }
        
        //pet_store
        print("\n ----------------------Places URL = \(strUrl)\n")
        
        //Costco Location = (42.0150599, -87.7829346)
        //The Fresh Market = (42.0807986, -87.7587472)
        //Kroger Mart = (40.1208193, -83.0902709)
        //Publix Location = (26.386930, -80.078047)
        //Trader Joe's = (38.0640464, -78.4905779)
        //Walmart Location = (26.436384, -80.12261)
        //Wawa Location = (33.8924922, -83.3739964)
        //Whole Foods Location = (39.06826, -77.4840521)
        //Starbucks Location = (28.6662752, -77.1253246)
        //Chipotle Grill Location = (26.365584, -80.078941)
        
        //Alfalfa's Market Location = (39.9870078, -105.1344862)
        //CosmoProf Location = (39.8552137, -105.0809277) // (25.7329385, -80.3450386)
        //CVS Pharmacy Location = (40.0227183, -105.2563465)
        //Dunkin Donut's Location = (40.0338396, -105.2588705) //Ahmedabad - (23.0245552, 72.5563425)
        //GNC's Location = (25.7495414, -80.2600533)
        //Panera Bread's Location = (25.7496781, -80.2574439)
        //Pet Supermarket's Location = (37.9620864, 23.6762804)
        //Salon Centric's Location = (36.12321, -115.2795002)
        //Wallgreen's Location = (33.5289889, -111.9258436)
        
        Alamofire.request(.GET, strUrl, parameters:nil, encoding:.JSON).validate().responseJSON { response in
            switch response.result {
            case .Success:
                if let value = response.result.value {
                    let json = JSON(value)
                    print("----------- Response for \(place)")
                    
                    if (json["results"].exists()) {
                        print("\n JSON = \(json)")
                        let results = json["results"].array
                        print("\n--------------------------- \(results!.count) Places found near \(place)")
                        var validStores = 0
                        for i in 0 ..< results!.count {
                            let curDic = results![i].dictionary
                            let temp = Place().initWithDictionary(curDic!)
                            
                            var isValidStore = false
                            switch place {
                            case strCostco:
                                if (temp.name.containsIgnoringCase("costco")) {
                                    isValidStore = true
                                }
                            case strThefresh:
                                if (temp.name.containsIgnoringCase("fresh") && temp.name.containsIgnoringCase("market")) {
                                    isValidStore = true
                                }
                            case strKroger:
                                if (temp.name.containsIgnoringCase("kroger")) {
                                    isValidStore = true
                                }
                            case strPublix:
                                if (temp.name.containsIgnoringCase("publix")) {
                                    isValidStore = true
                                }
                            case strTrader:
                                if (temp.name.containsIgnoringCase("trader")) {
                                    isValidStore = true
                                }
                            case strWalmart:
                                if (temp.name.containsIgnoringCase("walmart")) {
                                    isValidStore = true
                                }
                            case strWawa:
                                if (temp.name.containsIgnoringCase("wawa")) {
                                    isValidStore = true
                                }
                            case strWholefoods:
                                if (temp.name.containsIgnoringCase("whole") && temp.name.containsIgnoringCase("foods")) {
                                    isValidStore = true
                                }
                            case strStartBucks:
                                if (temp.name.containsIgnoringCase("starbucks")) {
                                    isValidStore = true
                                }
                            case strChipotle:
                                if (temp.name.containsIgnoringCase("chipotle") && temp.name.containsIgnoringCase("grill")) {
                                    isValidStore = true
                                }
                            case strAlfalfas:
                                if (temp.name.containsIgnoringCase("alfalfa")) {
                                    isValidStore = true
                                }
                            case strCosmoProf:
                                if (temp.name.containsIgnoringCase("cosmoprof")) {
                                    isValidStore = true
                                }
                            case strCvsPharmacy:
                                if (temp.name.containsIgnoringCase("cvs") && temp.name.containsIgnoringCase("pharmacy")) {
                                    isValidStore = true
                                }
                            case strDunkinDonuts:
                                if (temp.name.containsIgnoringCase("dunkin") && temp.name.containsIgnoringCase("donut")) {
                                    isValidStore = true
                                }
                            case strGNCLiveWell:
                                if (temp.name.containsIgnoringCase("gnc")) {
                                    isValidStore = true
                                }
                            case strPaneraBread:
                                if (temp.name.containsIgnoringCase("panera") && temp.name.containsIgnoringCase("bread")) {
                                    isValidStore = true
                                }
                            case strPetSupermarket:
                                if (temp.name.containsIgnoringCase("pet") && temp.name.containsIgnoringCase("supermarket")) {
                                    isValidStore = true
                                }
                            case strSaloncentric:
                                if (temp.name.containsIgnoringCase("salon") && temp.name.containsIgnoringCase("centric")) {
                                    isValidStore = true
                                }
                            case strWalgreens:
                                if (temp.name.containsIgnoringCase("walgreens")) {
                                    isValidStore = true
                                }
                            default:break
                            }
                            
                            if (isValidStore) {
                                validStores += 1
                                temp.place = place
                                temp.calculateDistance(self.curLocation)
                                self.placesArray.append(temp)
                            }
                        }
                        print("\n--------------------------- \(validStores) Places valid near \(place)")
                    }
                }
            case .Failure(let error):
                print("\(error.localizedDescription) failure")
            }
            
            //Leave group
            dispatch_group_leave(group)
        }
    }
}
