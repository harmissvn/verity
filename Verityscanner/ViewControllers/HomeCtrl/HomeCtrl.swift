//
//  HomeCtrl.swift
//  Verityscanner
//
//  Created by harmis on 29/08/16.
//  Copyright © 2016 certified. All rights reserved.
//

import UIKit
import GoogleMobileAds
import Firebase
import PopupController
import Alamofire
import SwiftyJSON
import Bolts
import FontAwesome_swift

class HomeCtrl: UIViewController {
    
    @IBOutlet weak var collectionHome: UICollectionView!
    @IBOutlet weak var imgVerityLogo : UIImageView!
    @IBOutlet weak var navView       : UIView!
    @IBOutlet weak var adBannerView  : GADBannerView!
    @IBOutlet weak var lblWelcomeUser : UILabel!

    @IBOutlet weak var collectionHeight: NSLayoutConstraint!
    
    var strSettingLogo   : String = ""
    var strProductCode   : String = ""
    var isProductScanned : Bool = false
    
    var rowsArray = [Row]()
    
    var dialog      : QBChatDialog?
    var users       : [QBUUser] = []
    var dataSource  : UsersDataSource!
    var session     : QBRTCSession?
    
    var userNumber = 0
    
    let cellIdentifier                    = "HomeCell"
    let collectionFooterIdentifier        = "SocialCell"
    let collectionFooterRecallsIdentifier = "RecallsCell"
    
    enum MenuItem: String {
        case SCAN     = "Scan"
        case SEARCH   = "Search"
        case ABOUT_US = "About Us"
        case SOCIAL   = "Social"
        case RECALLS  = "Recalls"
        case CHAT     = "Chat"
    }
    
    let row0 = ["value": MenuItem.SCAN.rawValue    , "image": "Verity/HomeScreen/scan@2x.png"]
    let row1 = ["value": MenuItem.SEARCH.rawValue  , "image": "Verity/HomeScreen/search@2x.png"]
    let row2 = ["value": MenuItem.ABOUT_US.rawValue, "image": "Verity/HomeScreen/about-us@2x.png"]
    let row3 = ["value": MenuItem.SOCIAL.rawValue  , "image": "social"]
    let row4 = ["value": MenuItem.RECALLS.rawValue , "image": "recalls"]
    let row5 = ["value": MenuItem.CHAT.rawValue    , "image": "chat"]
    
    // MARK: - UIView Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setFirebaseStorage()
        setFireBase()
        setupAdBanner() //Ad-Mob Setup
        setUICollectionView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        let width:CGFloat = (collectionHome.frame.size.width-30)/3
        var collectionHeight = width + 133.0 + 15.0
        if (IS_IPAD) {
            collectionHeight = width + 241.0 + 15.0
        }
        self.collectionHeight.constant = collectionHeight
        self.view.layoutIfNeeded()
    }
    
    // MARK: - All Functions
    
    func setUICollectionView() {
        if CommonMethods.sharedInstance.qb_user == nil {
            appDel!.connectUserWithQuickbloxChat()
            appDel!.connectUserWithQuickblox()
        } else {
            print(CommonMethods.sharedInstance.qb_user!)
        }
                
        collectionHome.registerNib(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
        collectionHome.registerNib(UINib(nibName: collectionFooterIdentifier, bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: collectionFooterIdentifier)
        collectionHome.registerNib(UINib(nibName: collectionFooterRecallsIdentifier, bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionFooter, withReuseIdentifier: collectionFooterRecallsIdentifier)
    }
    
    func setFirebaseStorage() {
        let storageRef = FIRStorage.storage().reference()
        //For HomeLogo
        let homeLogoReference = storageRef.child("Verity/HomeScreen/homeLogoTop@2x.png")
        homeLogoReference.dataWithMaxSize((1 * 1024 * 1024)) { (data, error) in
            if error == nil {
                self.imgVerityLogo.image = UIImage(data: data!)
            } else {
                print(error?.localizedDescription)
            }
        }
        //For Setting Button
        
        if IS_IPAD {
            self.strSettingLogo = "Verity/Common/settings@2x.png"
        } else {
            self.strSettingLogo = "Verity/Common/settings.png"
        }
        let settingReference = storageRef.child(self.strSettingLogo)
        settingReference.dataWithMaxSize((1 * 1024 * 1024)) { (data, error) in
            if error == nil {
            } else {
                print(error?.localizedDescription)
            }
        }
    }
    
    func setFireBase() {
        self.setupDefaultConfig()
        self.fetchRemoteConfig()
    }
    
    func setupDefaultConfig() {
        let defaultValues = [
            "homeBackgroundColor" : "#ffffff",
            "homeHeaderColor"     : "#3BB9F9",
            "homeWelcomTitleColor": "#5B78BA"
        ]
        FIRRemoteConfig.remoteConfig().setDefaults(defaultValues)
        afterFireBaseCall()
    }
    
    func fetchRemoteConfig() {
        var expirationDuration : NSTimeInterval = 3600
        if FIRRemoteConfig.remoteConfig().configSettings.isDeveloperModeEnabled {
            expirationDuration = 0.0
        }
        
        FIRRemoteConfig.remoteConfig().fetchWithExpirationDuration(expirationDuration, completionHandler: {(status, error) -> Void in
            if (error == nil) {
                print("Config fetched!",status)
                self.afterFireBaseCall()
                FIRRemoteConfig.remoteConfig().activateFetched()
            } else {
                print("Config not fetched")
                print("Error \(error!.localizedDescription)")
            }
        })
    }
    
    func hexStringToUIColor(hex:String) -> UIColor {
        var cString:String = hex.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet() as NSCharacterSet).uppercaseString
        if (cString.hasPrefix("#")) {
            cString = cString.substringFromIndex(cString.startIndex.advancedBy(1))
        }
        if ((cString.characters.count) != 6) {
            return UIColor.grayColor()
        }
        var rgbValue:UInt32 = 0
        NSScanner(string: cString).scanHexInt(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func afterFireBaseCall() {
        let afterFireBase = FIRRemoteConfig.remoteConfig()
        
        let backgroundColorValue  = afterFireBase.configValueForKey("homeBackgroundColor").stringValue!
        self.view.backgroundColor = hexStringToUIColor(backgroundColorValue)
        
        let headerColor = afterFireBase.configValueForKey("homeHeaderColor").stringValue!
        self.navView.backgroundColor = hexStringToUIColor(headerColor)
        
        let userTextColorValue  = afterFireBase.configValueForKey("homeWelcomTitleColor").stringValue!
        self.lblWelcomeUser.textColor = hexStringToUIColor(userTextColorValue)

        self.setUI()
    }
    
    func setUI() {
        self.lblWelcomeUser.text = "Welcome Back, " + (CommonMethods.sharedInstance.userModel?.name)!
        //self.navView.addDropShadowToView()
        imgVerityLogo.layer.cornerRadius = 5.0
        
        let row0 = ["value": "Scan"    , "image": "Verity/HomeScreen/scan@2x.png"]
        let row1 = ["value": "Search"  , "image": "Verity/HomeScreen/search@2x.png"]
        let row2 = ["value": "About Us", "image": "Verity/HomeScreen/about-us@2x.png"]
        let row3 = ["value": "Chat"    , "image": "chat"]
        let row4 = ["value": "Recalls" , "image": "recalls"]
        let row5 = ["value": "Social"  , "image": "social"]
        
        
        let tempArray = [row0, row1, row2, row3, row4, row5]
        for i in 0 ..< tempArray.count {
            let temp = Row().initWithDictionary(tempArray[i])
            rowsArray.append(temp)
        }
    }
    
    func setupAdBanner() {
        //#if RELEASE
        adBannerView.adUnitID = AD_MOB_UNIT_ID
        //#else
        //adBannerView.adUnitID = "ca-app-pub-3940256099942544/2934735716" //AD_MOB_UNIT_ID
        //#endif
        adBannerView.rootViewController = self
        let request = GADRequest()
        request.testDevices = [kGADSimulatorID]
        adBannerView.loadRequest(request)
    }
    
    //    func openScanner() {
    //        //Firebase Log Event
    //        FIRAnalytics.logEventWithName(SCAN_BUTTON_EVENT, parameters: [
    //            "name": SCAN_BUTTON_EVENT_NAME,
    //            "full_text": ""
    //            ])
    //
    //        let scanCtrl = ScanCtrl(nibName: "ScanCtrl", bundle: nil)
    //        self.navigationController?.pushViewController(scanCtrl, animated: true)
    //    }
    
    func openScanner() {
        let viewCtrl = PopUpScanCtrl(nibName: "PopUpScanCtrl", bundle: nil)
        let popup = PopupController
            .create(self)
            .customize(
                [
                    .Animation(.SlideUp),
                    .Scrollable(true),
                    .BackgroundStyle(.BlackFilter(alpha: 0.5))
                ]
            )
            .show(viewCtrl)
        viewCtrl.closeHandler = { str  in
            popup.dismiss()
        }
    }
    
    func openSearch() {
        //Firebase Log Event
        FIRAnalytics.logEventWithName(SEARCH_BUTTON_EVENT, parameters: [
            "name": SEARCH_BUTTON_EVENT_NAME,
            "full_text": ""
            ])
        
        let viewCtrl = SearchCtrl(nibName: "SearchCtrl", bundle: nil)
        self.navigationController?.pushViewController(viewCtrl, animated: true)
    }
    
    func openAboutUs() {
        //Firebase Log Event
        FIRAnalytics.logEventWithName(ABOUT_US_BUTTON_EVENT, parameters: [
            "name": ABOUT_BUTTON_EVENT_NAME,
            "full_text": ""
            ])
        let viewCtrl = AboutUsCtrl(nibName: "AboutUsCtrl", bundle: nil)
        self.navigationController?.pushViewController(viewCtrl, animated: true)
    }
    
    func openSocial() {
        let ctrl = SocialCtrl(nibName: "SocialCtrl", bundle: nil)
        self.navigationController?.pushViewController(ctrl, animated: true)
    }
    
    func openRecall() {
        let viewCtrl = PopUpRecallsCtrl(nibName: "PopUpRecallsCtrl", bundle: nil)
        let popup = PopupController
            .create(self)
            .customize(
                [
                    .Animation(.SlideUp),
                    .Scrollable(true),
                    .BackgroundStyle(.BlackFilter(alpha: 0.5))
                ]
            )
            .show(viewCtrl)
        viewCtrl.closeHandler = { str  in
            popup.dismiss()
        }
    }
    
    // MARK: - Chat All Functions
    
    func openChat() {
        if CommonMethods.sharedInstance.userModel?.email == "adam@certified.bz" {
            let ctrl = AllUserListCtrl(nibName: "AllUserListCtrl", bundle: nil)
            self.navigationController?.pushViewController(ctrl, animated: true)
        } else {
            appDel!.connectUserWithQuickbloxChat()
            if (CommonMethods.sharedInstance.isConnected) {
                CommonMethods.sharedInstance.showHudWithTitle(STR_LOADING)
                self.getUsersWithID()
                appDel?.registerForRemoteNotificationChat()
            } else {
                CommonMethods.sharedInstance.showAlertWithMessage(NO_INTERNET, withController: self)
            }
        }
    }
    
    func getUsersWithID() {
        ServicesManager.instance().usersService.getUsersWithLogins(["adam@certified.bz"]).continueWithBlock { (task: BFTask) -> AnyObject? in
            print(task.result?.count)
            print(task)
            if task.result?.count ?? 0 > 0 {
                guard let users = ServicesManager.instance().sortedUsers() else {
                    print("No cached users")
                    return nil
                }
                self.setupUsers(users)
            } else {
                CommonMethods.sharedInstance.showHudWithTitle("SA_STR_LOADING_USERS".localized)
                // Downloading users from Quickblox.
                ServicesManager.instance().downloadCurrentEnvironmentUsers({ (users) -> Void in
                    guard let unwrappedUsers = users else {
                        CommonMethods.sharedInstance.hideHud()
                        CommonMethods.sharedInstance.showAlertWithMessage("No users downloaded", withController: self)
                        return
                    }
                    CommonMethods.sharedInstance.showAlertWithMessage("SA_STR_COMPLETED".localized, withController: self)
                    self.setupUsers(unwrappedUsers)
                    CommonMethods.sharedInstance.hideHud()
                    }, errorBlock: { (error) -> Void in
                        CommonMethods.sharedInstance.hideHud()
                        CommonMethods.sharedInstance.showAlertWithMessage(error.localizedDescription, withController: self)
                })
            }
            return nil
        }
    }
    
    func setupUsers(users: [QBUUser]) {
        var aryUser = [QBUUser]()
        for i in 0 ..< users.count {
            let userValue = users[i]
            print(userValue.login)
            if "adam@certified.bz" == userValue.login {
                aryUser.append(userValue)
            }
        }
        print(aryUser)
        self.users = aryUser
        self.createChatCtrl()
    }
    
    func createChatCtrl() {
        let completion = {[weak self] (response: QBResponse?, createdDialog: QBChatDialog?) -> Void in
            if createdDialog != nil {
                print(createdDialog)
                self?.openNewDialog(createdDialog)
            }
            guard let unwrappedResponse = response else {
                print("Error empty response")
                return
            }
            if let error = unwrappedResponse.error {
                print(error.error)
                SVProgressHUD.showInfoWithStatus(error.error?.localizedDescription)
            } else {
                SVProgressHUD.showSuccessWithStatus("STR_DIALOG_CREATED".localized)
            }
        }
        self.createChat(nil, users: self.users, completion: completion)
        CommonMethods.sharedInstance.hideHud()
    }
    
    func createChat(name: String?, users:[QBUUser], completion: ((response: QBResponse?, createdDialog: QBChatDialog?) -> Void)?) {
        SVProgressHUD.showWithStatus("SA_STR_LOADING".localized, maskType: SVProgressHUDMaskType.Clear)
        print(users)
        ServicesManager.instance().chatService.createPrivateChatDialogWithOpponent(users.first!, completion: { (response: QBResponse?, chatDialog: QBChatDialog?) -> Void in
            completion?(response: response, createdDialog: chatDialog)
        })
    }
    
    func openNewDialog(dialog: QBChatDialog!) {
        CommonMethods.sharedInstance.hideHud()
        let ctrl = ChatViewCtrl(nibName: "ChatViewCtrl", bundle: nil)
        ctrl.dialog = dialog
        self.navigationController?.pushViewController(ctrl, animated: true)
    }
    
    // MARK: - UICollectionView Delegates & Datasources
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return rowsArray.count
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        let width:CGFloat = (collectionView.frame.size.width-30)/3
        return CGSize(width: width, height:width)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier, forIndexPath: indexPath) as! HomeCell
        let curRow = rowsArray[indexPath.item]
        
        if indexPath.row == 0 || indexPath.row == 1 || indexPath.row == 2 {
            let storageRef = FIRStorage.storage().reference()
            //For ScanLogo
            let scanReference = storageRef.child(curRow.image)
            scanReference.dataWithMaxSize((1 * 1024 * 1024)) { (data, error) in
                if error == nil {
                    cell.imgView.image = UIImage(data:data!)
                } else {
                    print(error?.localizedDescription)
                }
            }
        } else {
            if (CommonMethods.sharedInstance.userModel?.socialType == "0") {
                cell.lblTitle.text = curRow.value as String
                cell.imgView.image = UIImage(named: curRow.image)
            } else {
                if indexPath.row == 3 || indexPath.row == 4  {
                    cell.lblTitle.text = curRow.value as String
                    cell.imgView.image = UIImage(named: curRow.image)
                } else {
                    cell.backgroundColor = UIColor.clearColor()
                    cell.userInteractionEnabled = false
                }
            }
        }
        return cell
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        let curRow = rowsArray[indexPath.item]
        switch curRow.value {
        case MenuItem.SCAN.rawValue    : openScanner()
        case MenuItem.SEARCH.rawValue  : openSearch()
        case MenuItem.ABOUT_US.rawValue: openAboutUs()
        case MenuItem.SOCIAL.rawValue  : openSocial()
        case MenuItem.RECALLS.rawValue : openRecall()
        case MenuItem.CHAT.rawValue    : openChat()
        default:break
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
