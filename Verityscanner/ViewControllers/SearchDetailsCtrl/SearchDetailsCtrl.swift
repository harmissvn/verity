//
//  SearchDetailsCtrl.swift
//  Verityscanner
//
//  Created by Hardik Dineshchandra Mistry on 08/09/16.
//  Copyright © 2016 certified. All rights reserved.
//

import UIKit
import MapKit
import Alamofire
import SwiftyJSON
import AlamofireImage
import Cosmos


class SearchDetailsCtrl: UIViewController, UITextViewDelegate {
    
    @IBOutlet weak var tblSearchDetails : UITableView!
    @IBOutlet weak var imgProduct       : UIImageView!
    @IBOutlet weak var mapView          : MKMapView!
    @IBOutlet var constImgHeight        : NSLayoutConstraint!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet var viewHeader            : UIView!
    @IBOutlet var viewFooter            : UIView!
    @IBOutlet var viewViewDesc          : UIView!
    
    @IBOutlet var lblTitle              : UILabel!
    @IBOutlet var lblDate               : UILabel!
    @IBOutlet var lblDesc               : UILabel!
    @IBOutlet var lblName               : UILabel!
    @IBOutlet var lblVote               : UILabel!
    
    @IBOutlet weak var viewRating       : CosmosView!
    @IBOutlet weak var viewRatingComment: CosmosView!
    
    @IBOutlet var btnAllReview          : UIButton!
    @IBOutlet var btnAllReviewView      : UIButton!
    @IBOutlet var btnSubmitReview       : UIButton!
    @IBOutlet var btnRateThisProduct    : UIButton!
    
    @IBOutlet var heightViewDesc        : NSLayoutConstraint!
    @IBOutlet var heightBtnRate         : NSLayoutConstraint!
    @IBOutlet var heightLineAllReview   : NSLayoutConstraint!
    @IBOutlet var heightMap             : NSLayoutConstraint!
    @IBOutlet var heightMapLine         : NSLayoutConstraint!
    
    var curProduct:Product? = nil
    
    let cellIdentifier = "SearchCell"
    let certificateIdentifier = "CertificationCell"
    
    // MARK: - ViewLife Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // NSNotificationCenter For SubmitReviewCtrl
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(self.receiveNotification), name: "addDicProductNotification", object: nil)
        
        setupUI()
    }
    
    // MARK: - All Functions
    
    func receiveNotification(notification: NSNotification) {
        let notInfo = notification.object
        self.curProduct = notInfo as? Product
        setupUI()
    }
    
    func sizeHeaderToFit() {
        guard let header = tblSearchDetails.tableHeaderView else { return }
        header.setNeedsLayout()
        header.layoutIfNeeded()
        let height = header.systemLayoutSizeFittingSize(UILayoutFittingCompressedSize).height
        header.frame.size.height = height
        tblSearchDetails.tableHeaderView = header
    }
    
    func setupUI() {
        tblSearchDetails.estimatedRowHeight = 60.0
        tblSearchDetails.rowHeight = UITableViewAutomaticDimension
        
        tblSearchDetails.registerNib(UINib(nibName: cellIdentifier, bundle: nil), forCellReuseIdentifier: cellIdentifier)
        tblSearchDetails.registerNib(UINib(nibName: certificateIdentifier, bundle: nil), forCellReuseIdentifier: certificateIdentifier)
        self.tblSearchDetails.tableHeaderView = self.viewHeader
        
        if let product = self.curProduct {
            self.activityIndicator.startAnimating()
            if let url = NSURL(string: product.productImage) {
                imgProduct.af_setImageWithURL(
                    url,
                    placeholderImage: UIImage.init(named: "noImageBig"),
                    filter: nil,
                    imageTransition: .CrossDissolve(0.2),
                    completion: { response in
                        self.activityIndicator.stopAnimating()
                        
                        /*if response.result.isSuccess {
                         let image = response.result.value
                         let xRatio = self.imgProduct.frame.size.width/image!.size.width
                         let yRatio = self.imgProduct.frame.size.height/image!.size.height
                         let minRatio = min(xRatio, yRatio)
                         
                         let width = image!.size.width * minRatio
                         let height = image!.size.height * minRatio
                         
                         print("Server size \(image?.size)")
                         print("Imageview size \(self.imgProduct.frame.size)")
                         print("New width = \(width) & New height = \(height)")
                         //self.constImgHeight.constant = height
                         }*/
                })
            }
            self.setFooterValue(product)
        }
        tblSearchDetails.reloadData()
    }
    
    func setFooterValue(product : Product) {
        
        var viewHeightDesc : CGFloat = 0.0
        
        self.viewRating.rating        = Double(product.avgRating)!
        self.viewRatingComment.rating = Double(product.avgRating)!
        self.lblVote.text             = "(\(product.rateUserCount + " Vote"))"
        
        if product.isRate == "0" { // Rate Button Hide or Show
            self.btnRateThisProduct.layer.cornerRadius = 3.0
            self.btnRateThisProduct.addShadowView()
            heightBtnRate.constant = 40
        } else {
            self.btnRateThisProduct.hidden = true
            heightBtnRate.constant = 0.0
        }
        
        if product.intReviewHidden == 1 { // desc view Hide or Show
            heightLineAllReview.constant = 1.0
            self.lblTitle.text = product.dicReview.reviewTitle
            self.lblName.text  = product.dicReview.userName
            self.lblDesc.text  = product.dicReview.reviewText
            self.lblDate.text  = product.dicReview.reviewDate
            self.viewRatingComment.rating = Double(product.dicReview.reviewValue)!
            
            let heightLblTitle = self.lblTitle.requiredHeight()
            let heightLblName  = self.lblName.requiredHeight()
            let heightLblDesc  = self.lblDesc.requiredHeight()
            let heightLblDate  = self.lblDate.requiredHeight()
            
            viewHeightDesc = heightLblTitle + heightLblName + heightLblDesc + heightLblDate + 50
            heightViewDesc.constant = viewHeightDesc
        } else {
            heightLineAllReview.constant = 0.0
            heightViewDesc.constant      = 0.0
        }
        
        if product.isReview == "0" { // Footer button Hide or Show
            btnAllReview.userInteractionEnabled     = true
            btnSubmitReview.userInteractionEnabled  = true
            btnAllReviewView.userInteractionEnabled = false
            
            btnAllReviewView.hidden = true
            
            btnAllReview.layer.cornerRadius = 3.0
            btnAllReview.addShadowView()
            
            btnSubmitReview.layer.cornerRadius = 3.0
            btnSubmitReview.addShadowView()
        } else {
            btnAllReview.userInteractionEnabled     = false
            btnSubmitReview.userInteractionEnabled  = false
            btnAllReviewView.userInteractionEnabled = true
            
            btnAllReview.hidden     = true
            btnSubmitReview.hidden  = true
            btnAllReviewView.hidden = false
            
            btnAllReviewView.layer.cornerRadius = 3.0
            btnAllReviewView.addShadowView()
        }
        
        let location = self.curProduct?.productLocation
        if (location != nil) {
            heightMapLine.constant = 1.0
            heightMap.constant = 184.0
            let center = CLLocationCoordinate2D(latitude: location!.coordinate.latitude, longitude: location!.coordinate.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            self.mapView.setRegion(region, animated: false)
            
            let dropPin = MKPointAnnotation()
            dropPin.coordinate = center
            dropPin.title = self.curProduct?.productName
            mapView.addAnnotation(dropPin)
        } else {
            heightMapLine.constant = 0.0
            heightMap.constant = 0.0
        }
        
        //tblSearchDetails.tableFooterView = viewFooter
        
        let footerView = self.viewFooter
        var footerFrame = footerView!.frame
        footerFrame.size.height = IS_IPAD ? 404 + viewHeightDesc : 300.0
        footerView!.frame = footerFrame
        tblSearchDetails.tableFooterView = self.viewFooter
    }
    
    // MARK: - IBAction Methods And Functions
    
    @IBAction func btnSubmitReviewAction(sender : UIButton) {
        let ctrl = SubmitReviewCtrl(nibName: "SubmitReviewCtrl", bundle: nil)
        ctrl.curProductData = self.curProduct
        self.navigationController?.pushViewController(ctrl, animated: true)
    }
    
    @IBAction func btnAllReviewAction(sender : UIButton) {
        let ctrl = ReviewCtrl(nibName: "ReviewCtrl", bundle: nil)
        ctrl.strProductId = (self.curProduct?.productId)!
        self.navigationController?.pushViewController(ctrl, animated: true)
    }
    
    @IBAction func btnRateThisProductAction(sender : UIButton) {
        let viewCtrl = RateProductCtrl(nibName: "RateProductCtrl", bundle: nil)
        viewCtrl.modalPresentationStyle = UIModalPresentationStyle.OverCurrentContext
        viewCtrl.productData = self.curProduct
        viewCtrl.closeHandler = { ratingData in
            self.viewRating.rating = Double(ratingData.ratingValue)!
            NSNotificationCenter.defaultCenter().postNotificationName("addRatingNotification", object: ratingData)
            self.heightBtnRate.constant = 0.0
            self.btnRateThisProduct.hidden = true
            self.tblSearchDetails.reloadData()
        }
        self.presentViewController(viewCtrl, animated: true, completion: nil)
    }
    
    
    // MARK: - UITableView Methods and Functions
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.curProduct?.productDetails.count)! ?? 0
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        let curRow = self.curProduct?.productDetails[indexPath.row]
        if (curRow?.type == "0") {
            (cell as! SearchCell).lblPlaceholder.font = kSystemRegularFonts(17.0)
            (cell as! SearchCell).tvValue.font = kSystemRegularFonts(17.0)
        } else {
            (cell as! CertificationCell).lblPlaceholder.font = kSystemRegularFonts(17.0)
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let curRow = self.curProduct?.productDetails[indexPath.row]
        if (curRow?.type == "0") {
            
            let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier, forIndexPath: indexPath) as! SearchCell
            cell.tvValue.delegate = self
            cell.selectionStyle = .None
            cell.lblPlaceholder.text = curRow!.placeholder ?? "NA"
            cell.tvValue.text = curRow!.value ?? "NA"
            return cell
        } else {
            let cell = tableView.dequeueReusableCellWithIdentifier(certificateIdentifier, forIndexPath: indexPath) as! CertificationCell
            cell.selectionStyle = .Default
            
            cell.lblPlaceholder.text = curRow!.placeholder ?? "NA"
            cell.imgCertificate.image = nil
            
            //cell.btnCertificate.setBackgroundImage(nil, forState: .Normal)
            //cell.btnCertificate.addTarget(self, action: #selector(btnCertificateClicked(_:)), forControlEvents: .TouchUpInside)
            //cell.btnCertificate.setTitle(curRow?.redirectUrl, forState: .Normal)
            //cell.btnCertificate.imageView?.contentMode = .ScaleAspectFit
            
            if let curURL = curRow?.value {
                cell.imgCertificate.af_setImageWithURL(NSURL(string: curURL)!)
            }
            return cell
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        let curRow = self.curProduct?.productDetails[indexPath.row]
        if (curRow?.type == "1") {
            if let redirectUrl = curRow?.redirectUrl {
                let viewCtrl = WebviewCtrl(nibName: "WebviewCtrl", bundle: nil)
                viewCtrl.strURL = redirectUrl
                viewCtrl.titleStr = "Certification"
                self.navigationController?.pushViewController(viewCtrl, animated: true)
            }
        }
    }
    
    func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.min
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat.min
    }
    
    func btnCertificateClicked(sender: UIButton) {
        if let redirectUrl = sender.titleForState(.Normal) {
            let viewCtrl = WebviewCtrl(nibName: "WebviewCtrl", bundle: nil)
            viewCtrl.strURL = redirectUrl
            viewCtrl.titleStr = "Certification"
            self.navigationController?.pushViewController(viewCtrl, animated: true)
        }
    }
    
    func textViewDidChange(textView: UITextView) {
        let currentOffset = tblSearchDetails.contentOffset
        UIView.setAnimationsEnabled(false)
        tblSearchDetails.beginUpdates()
        tblSearchDetails.endUpdates()
        UIView.setAnimationsEnabled(true)
        tblSearchDetails.setContentOffset(currentOffset, animated: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension UILabel{
    
    func requiredHeight() -> CGFloat{
        
        let label:UILabel = UILabel(frame: CGRectMake(0, 0, self.frame.width, CGFloat.max))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.ByWordWrapping
        label.font = self.font
        label.text = self.text
        
        label.sizeToFit()
        
        return label.frame.height
    }
}
