//
//  RateProductCtrl.swift
//  Verityscanner
//
//  Created by harmis on 27/02/17.
//  Copyright © 2017 certified. All rights reserved.
//

import UIKit
import Cosmos
import Alamofire
import SwiftyJSON
import AlamofireImage
import IQKeyboardManagerSwift

class RateProductCtrl: UIViewController {
    
    @IBOutlet var imgView   : UIImageView!
    @IBOutlet var lblName   : UILabel!
    
    @IBOutlet var viewMain  : UIView!
    @IBOutlet var viewRating: CosmosView!
    
    @IBOutlet var btnCancel: UIButton!
    @IBOutlet var btnSubmit: UIButton!
    
    var productData    : Product? = nil
    var dicRating      = Rating()
    var strRatingValue : String = ""

    var closeHandler: ((ratingData: Rating) -> Void)?
    
    // MARK: - UIView Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    // MARK: - All Functions
    
    func setUI() {
        
        if let curURL = productData?.thumbImage {
            self.imgView.af_setImageWithURL(NSURL(string: curURL)!)
        }
        self.lblName.text = productData!.productName
        
        self.imgView.layer.cornerRadius   = 5.0
        self.btnCancel.layer.cornerRadius = 3.0
        self.btnSubmit.layer.cornerRadius = 3.0
        self.viewMain.layer.cornerRadius  = 10.0
        
        self.btnSubmit.addShadowView()
        self.btnCancel.addShadowView()
        
        self.viewRating.didTouchCosmos = { rating in
            switch rating {
            case 1.0: self.strRatingValue = "1"
            case 2.0: self.strRatingValue = "2"
            case 3.0: self.strRatingValue = "3"
            case 4.0: self.strRatingValue = "4"
            case 5.0: self.strRatingValue = "5"
            default:
                break
            }
        }
    }
    
    
    @IBAction func btnCancelAction(sender : UIButton) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func btnSubmitAction(sender : UIButton) {
        if self.strRatingValue == "" {
            CommonMethods.sharedInstance.showAlertWithMessage("Please give rating.", withController: self)
        } else {
            if (CommonMethods.sharedInstance.isConnected) {
                CommonMethods.sharedInstance.showHudWithTitle("Loading...")
                /*
                 ratingValue
                 productId
                 userId */
                
                let parameters = [
                    "userId"       : CommonMethods.sharedInstance.userModel?.userId,
                    "productId"    : productData?.productId,
                    "ratingValue"  : self.strRatingValue
                ]
                print("----------Parameter",parameters)
                
                Alamofire.request(.POST, GIVE_RATING, parameters: (parameters as? [String : String]) , encoding:.JSON).validate().responseJSON { response in
                    CommonMethods.sharedInstance.hideHud()
                    switch response.result {
                    case .Success:
                        if let value = response.result.value {
                            let json = JSON(value)
                            NSLog("\n------------------- Response: \(json)")
                            if (json["success"].numberValue == 1) {
                                let curDic = json["result"].dictionary
                                self.dicRating = Rating().initWithDictionary(curDic!)
                                self.closeHandler!(ratingData: self.dicRating)
                                self.dismissViewControllerAnimated(true, completion: nil)
                            } else {
                                let message = json["message"].stringValue
                                CommonMethods.sharedInstance.showAlertWithMessage(message, withController: self)
                            }
                        }
                    case .Failure(let error):
                        if let data = response.data {
                            print("Response data: \(NSString(data: data, encoding: NSUTF8StringEncoding)!)")
                            if error.code != NSURLErrorCancelled {
                                CommonMethods.sharedInstance.showAlertWithMessage(error.localizedDescription, withController: self)
                            }
                        }
                    }
                }
            } else {
                CommonMethods.sharedInstance.showAlertWithMessage(NO_INTERNET, withController: self)
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
