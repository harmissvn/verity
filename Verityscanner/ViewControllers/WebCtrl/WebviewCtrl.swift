//
//  WebviewCtrl.swift
//  Cellphone Refills
//
//  Created by Hardik Dineshchandra Mistry on 18/07/16.
//  Copyright © 2016 Adminsys. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class WebviewCtrl: UIViewController , UIWebViewDelegate, UIDocumentInteractionControllerDelegate {
    
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var navView: UIView!
    @IBOutlet var objWebView: UIWebView!
    @IBOutlet var activityIndicator: UIActivityIndicatorView!
    @IBOutlet var btnClose: UIButton!
    
    var strURL: String = ""
    var titleStr: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        self.lblTitle.text = self.titleStr
        self.clearCache()
        
        objWebView.delegate = self
        objWebView.scalesPageToFit = true
        objWebView.loadRequest(NSURLRequest(URL: NSURL(string: self.strURL as String)!))
    }
    
    // MARK: - View Life Cycle Methods
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        self.navView.addDropShadowToView()
        objWebView.delegate = nil
        objWebView.stopLoading()
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        //objWebView = nil
        self.clearCache()
    }
    
    func delay(delay:Double, closure:()->()) {
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue(), closure)
    }
    
    func clearCache() {
        // Remove and disable all URL Cache, but doesn't seem to affect the memory
        NSURLCache.sharedURLCache().removeAllCachedResponses()
        NSURLCache.sharedURLCache().diskCapacity = 0
        NSURLCache.sharedURLCache().memoryCapacity = 0
    }
    
    // MARK: - UIWebView Delegate Methods
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        activityIndicator.startAnimating()
        return true
    }
    
    func webViewDidFinishLoad(theWebView: UIWebView) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        activityIndicator.stopAnimating()
    }
    
    func webViewDidStartLoad(webView: UIWebView) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
        activityIndicator.startAnimating()
    }
    
    func webView(webView: UIWebView, didFailLoadWithError error: NSError) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        activityIndicator.stopAnimating()
    }
    
    // MARK: - IBAction Methods and Functions
    
    @IBAction func btnBackClicked(_: AnyObject) {
        if (objWebView.canGoBack) {
            objWebView.goBack()
            btnClose.hidden = false
        } else {
            UIApplication.sharedApplication().networkActivityIndicatorVisible = false
            activityIndicator.stopAnimating()
            objWebView.stopLoading()
            objWebView.delegate = nil
            self.navigationController!.popViewControllerAnimated(true)
            self.clearCache()
        }
    }
    
    @IBAction func btnCloseClicked(_: AnyObject) {
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
        activityIndicator.stopAnimating()
        objWebView.stopLoading()
        objWebView.delegate = nil
        self.navigationController!.popViewControllerAnimated(true)
        self.clearCache()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
