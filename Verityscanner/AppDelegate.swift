//
//  AppDelegate.swift
//  Verityscanner
//
//  Created by harmis on 29/08/16.
//  Copyright © 2016 certified. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FBSDKLoginKit
import FBSDKCoreKit
import Accounts
import GoogleSignIn
import GoogleMobileAds
import Fabric
import TwitterKit
import FirebaseMessaging
import IQKeyboardManagerSwift
import UserNotifications
import FirebaseInvites
import FirebaseDynamicLinks

import Quickblox
import QuickbloxWebRTC

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate, FIRMessagingDelegate, QBRTCClientDelegate {
    
    var window: UIWindow?
    var navCtrl : UINavigationController?
    var credential : FIRAuthCredential?
    
    var tabController           : UITabBarController?
    
    var homeNavController       : UINavigationController?
    var searchNavController     : UINavigationController?
    var scanNavController       : UINavigationController?
    var chatNavController       : UINavigationController?
    var settingNavController    : UINavigationController?
    
    let customURLScheme = "dynamicScheme"
    
    var loginWithCredential : ((FIRAuthCredential?, NSError?) -> ())?
    
    func registerForPushNotifications(application: UIApplication) {
        
        if #available(iOS 10.0, *) {
            let authOptions : UNAuthorizationOptions = [.Alert, .Badge, .Sound]
            UNUserNotificationCenter.currentNotificationCenter().requestAuthorizationWithOptions(
                authOptions,
                completionHandler: {_,_ in })
            
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.currentNotificationCenter().delegate = self
            // For iOS 10 data message (sent via FCM)
            FIRMessaging.messaging().remoteMessageDelegate = self
            
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
            application.registerUserNotificationSettings(settings)
            application.registerForRemoteNotifications()
        }
        
        let notificationSettings = UIUserNotificationSettings(
            forTypes: [.Badge, .Sound, .Alert], categories: nil)
        application.registerUserNotificationSettings(notificationSettings)
        application.registerForRemoteNotifications()
    }
    
    func quickBloxAddSetails() {
        // QuickBlox Details
        QBSettings.setApplicationID(UInt(kAPPLICATION_ID))
        QBSettings.setAuthKey(kAUTH_KEY)
        QBSettings.setAuthSecret(kAUTH_SECRET)
        QBSettings.setAccountKey(kACCOUNT_KEY)
        
        // enabling carbons for chat
        QBSettings.setCarbonsEnabled(true)
        
        // Enables Quickblox REST API calls debug console output.
        QBSettings.setLogLevel(QBLogLevel.Debug)
        
        // Enables detailed XMPP logging in console output.
        QBSettings.enableXMPPLogging()
        QBSettings.setChatDNSLookupCacheEnabled(true)
        
        // QuickBlox Call And Video
        QBSettings.setLogLevel(QBLogLevel.Nothing)
        QBSettings.setAutoReconnectEnabled(true)
        
        //QuickbloxWebRTC preferences
        
        QBRTCConfig.setAnswerTimeInterval(KQB_ANSWER_TIME_INTERVAL)
        //QBRTCConfig.setDisconnectTimeInterval(KQB_RTC_DISCONNECT_TIME_INTERVAL)
        QBRTCConfig.setDialingTimeInterval(KQB_DIALING_TIME_INTERVAL)
        QBRTCConfig.setStatsReportTimeInterval(1.0)
        QBRTCClient.initializeRTC()

    }
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        //Setup Push Notification
        registerForPushNotifications(application)
        
        //Firebase Configure
        FIRApp.configure()
        FIROptions.defaultOptions().deepLinkURLScheme = self.customURLScheme
        
        IQKeyboardManager.sharedManager().enable = true
        IQKeyboardManager.sharedManager().disabledToolbarClasses = [SearchCtrl.self]
        
        //AdMob Configuration
        GADMobileAds.configureWithApplicationID(AD_MOB_APP_ID);
        
        quickBloxAddSetails()
        
        //Firebase push token observer
        NSNotificationCenter.defaultCenter().addObserverForName(kFIRInstanceIDTokenRefreshNotification, object: self, queue: nil) { (notification) in
            if let token = FIRInstanceID.instanceID().token() {
                NSLog("---------------------------- Firebase Token = \(token)")
                self.connectToFcm()
            }
        }

        window = UIWindow(frame: UIScreen.mainScreen().bounds)
        
        let currentState: Int = NSUserDefaults.standardUserDefaults().integerForKey(kUserState)
        if currentState ==  USER_STATE.LOGGED_IN.rawValue {
            CommonMethods.sharedInstance.userModel = CommonMethods.sharedInstance.loadUser()
            CommonMethods.sharedInstance.setUserState(USER_STATE.LOGGED_IN.rawValue)
            self.connectUserWithQuickblox()
            self.connectUserWithQuickbloxChat()
        } else {
            CommonMethods.sharedInstance.initialize()
        }
        
        setTabbarController(false)

        //Facebook Login
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        //Google Login
        GIDSignIn.sharedInstance().clientID = FIRApp.defaultApp()?.options.clientID
        
        // Twitter
        Twitter.sharedInstance().startWithConsumerKey("0I5gBROGbR9kdx7PZ2MWkwWom", consumerSecret: "b6g4ZN1VDjs4Wx714BvWFZfrkMpEexveeHysyloLHnEzW4CS1Z")
        
        Fabric.with([Twitter.sharedInstance()])
        Fabric.sharedSDK().debug = false
        
        // AutoScan Flag
        if (NSUserDefaults.standardUserDefaults().valueForKey(AUTOSCAN) == nil) {
            NSUserDefaults.standardUserDefaults().setBool(true, forKey: AUTOSCAN)
        }
        
        let splashCtrl = SplashCtrl(nibName: "SplashCtrl", bundle: nil)
        navCtrl = UINavigationController(rootViewController: splashCtrl)
        //navCtrl = UINavigationController(rootViewController: tabController!)
        navCtrl?.navigationBarHidden = true
        window?.rootViewController = navCtrl
        navCtrl?.navigationBar.translucent = true
        window?.makeKeyAndVisible()
        
        if let inviteUrl = launchOptions?[UIApplicationLaunchOptionsURLKey] as? NSURL {
            //App opened from invite url
            NSLog("\n-------------- UIApplicationLaunchOptionsURLKey App opened from invite url %@", inviteUrl)
            self.handleFirebaseInviteDeeplink(inviteUrl)
        }
        
        if let activityDic = launchOptions?[UIApplicationLaunchOptionsUserActivityDictionaryKey] as? [NSObject : AnyObject],
            let userActivity = activityDic["UIApplicationLaunchOptionsUserActivityKey"] as? NSUserActivity {
            
            NSLog("\n-------------- UIApplicationLaunchOptionsUserActivityKey App opened from user activity url")
 
            if let incomingURL = userActivity.webpageURL {
                let linkHandled = FIRDynamicLinks.dynamicLinks()!.handleUniversalLink(incomingURL) { [weak self] (dynamiclink, error) in
                    guard let strongSelf = self else { return }
                    
                    if let dyLink = dynamiclink, let _ = dynamiclink?.url {
                        if let url = dyLink.url {
                            NSLog("\n------------------Invite received continueUserActivity: %@ ", url)
                            strongSelf.handleFirebaseInviteDeeplink(url)
                        }
                    }
                }
                NSLog("\n------------------ UIApplicationLaunchOptionsUserActivityKey status %d", linkHandled)
            }
        }
        
        return true
    }
    
    func connectToFcm() {
        FIRMessaging.messaging().connectWithCompletion { (error) in
            if (error != nil) {
                print("Unable to connect with FCM. \(error)")
            } else {
                print("Connected to FCM.")
            }
        }
    }
    
    func tokenRefreshNotificaiton(notification: NSNotification) {
        if let token = FIRInstanceID.instanceID().token() {
            NSLog("---------------------------- Firebase Token = \(token)")
        }
        connectToFcm()
    }
    
    //MARK: Push Notification Methods
    
    func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject],
                     fetchCompletionHandler completionHandler: (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        
        // Print message ID.
        // NSLog("----------------------------------Message ID: \(userInfo["gcm.message_id"]!)")
        NSLog("----------------------------------Backgroud Push Notification Content %@", userInfo)
        
        FIRMessaging.messaging().appDidReceiveMessage(userInfo)
        
        if let message = userInfo["aps"]?["alert"] as? String {
            let alertMsg = message ?? "No message for push"
            let  alertController = UIAlertController(title: APP_NAME, message:alertMsg, preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            }))
            self.window?.rootViewController!.presentViewController(alertController, animated: true, completion: nil)
        }
        
        if let customPush = userInfo["notification"]?["body"] as? String {
            let alertMsg = customPush ?? "No message for push"
            let  alertController = UIAlertController(title: APP_NAME, message:alertMsg, preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            }))
            self.window?.rootViewController!.presentViewController(alertController, animated: true, completion: nil)
        }
        
//        if(application.applicationState == UIApplicationState.Inactive)
//        {
//            print("Inactive")
//            //Show the view with the content of the push
//            completionHandler(.NewData)
//            
//        }else if (application.applicationState == UIApplicationState.Background){
//            
//            print("Background")
//            //Refresh the local model
//            completionHandler(.NewData)
//            
//        }else{
//            
//            print("Active")
//            //Show an in-app banner
//            completionHandler(.NewData)
//        }
    }

    func applicationReceivedRemoteMessage(remoteMessage: FIRMessagingRemoteMessage) {
        NSLog("----------------------------------iOS 10 Backgroud Push Notification Content %@", remoteMessage.appData)
        
        FIRMessaging.messaging().appDidReceiveMessage(remoteMessage.appData)
        
        if let message = remoteMessage.appData["aps"]?["alert"] as? String {
            let alertMsg = message ?? "No message for push"
            let  alertController = UIAlertController(title: APP_NAME, message:alertMsg, preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            }))
            self.window?.rootViewController!.presentViewController(alertController, animated: true, completion: nil)
        }
        
        if let customPush = remoteMessage.appData["notification"]?["body"] as? String {
            let alertMsg = customPush ?? "No message for push"
            let  alertController = UIAlertController(title: APP_NAME, message:alertMsg, preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            }))
            self.window?.rootViewController!.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.Prod)
        
        if let token = FIRInstanceID.instanceID().token() {
            NSLog("------------------- Firebase Token %@", token)
        }
        /*let tokenChars = UnsafePointer<CChar>(deviceToken.bytes)
        var tokenString = ""
        for i in 0..<deviceToken.length {
            tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
        }
        print("tokenString: \(tokenString)")*/
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        print(error)
    }
    
    /*func application(application: UIApplication, didRegisterUserNotificationSettings notificationSettings: UIUserNotificationSettings) {
        if notificationSettings.types != .None {
            application.registerForRemoteNotifications()
        }
    }
    
    func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.Sandbox)
    }
    
    func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
        NSLog("Failed to register:", error)
    }
    */
    
    /*func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
        NSLog("----------------------------------Foreground Push Notification Content %@", userInfo)
        FIRMessaging.messaging().appDidReceiveMessage(userInfo)
        if let message = userInfo["aps"]!["alert"] as? String {
            let alertMsg = message ?? "No message for push"
            let  alertController = UIAlertController(title: APP_NAME, message:alertMsg, preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
            }))
            self.window?.rootViewController!.presentViewController(alertController, animated: true, completion: nil)
        }
    }*/
    
    @available(iOS 9.0, *)
    func application(application: UIApplication, openURL url: NSURL, options: [String : AnyObject])
        -> Bool {
            NSLog("\n------------------Handling a link through openURL!!!")
            return self.application(application, openURL: url, sourceApplication: options[UIApplicationOpenURLOptionsSourceApplicationKey] as? String, annotation: "")
    }
    
    func application(application: UIApplication, openURL url: NSURL, sourceApplication: String?, annotation: AnyObject) -> Bool {
        let dynamicLink = FIRDynamicLinks.dynamicLinks()?.dynamicLinkFromCustomSchemeURL(url)
        if let dynamicLink = dynamicLink {
            // Handle the deep link. For example, show the deep-linked content or
            // apply a promotional offer to the user's account.
            // ...
            if let url = dynamicLink.url {
                NSLog("\n------------------Invite received FIRDynamicLinks: %@ ", url)
                self.handleFirebaseInviteDeeplink(url)
                return true
            }
        }
        
        if let invite = FIRInvites.handleURL(url, sourceApplication:sourceApplication, annotation:annotation) as? FIRReceivedInvite {
            let matchType = (invite.matchType == FIRReceivedInviteMatchType.Weak) ? "Weak" : "Strong"
            print("\n------------------Invite received from: \(sourceApplication) Deeplink: \(invite.deepLink)," + "Id: \(invite.inviteId), Type: \(matchType)")
            
            NSLog("\n------------------Invite received Deeplink: %@ ", invite.deepLink)
            NSLog("\n------------------Invite received Id: %@ ", invite.inviteId)
            NSLog("\n------------------Invite received Type: %@ ", matchType)
            
            //if (matchType == "Strong") {
                NSLog("\n-------------- Invite Deep Link %@", invite.deepLink)
                if !invite.deepLink.isEmpty {
                    let url = NSURL(string: invite.deepLink)
                    self.handleFirebaseInviteDeeplink(url!)
                    return true
                }
            //}
        }
        
        if (GIDSignIn.sharedInstance().handleURL(url, sourceApplication: sourceApplication, annotation: annotation)) {
            return true
        } else if (Twitter.sharedInstance().application(application, openURL: url, options: [NSObject : AnyObject]())) {
            return true
        } else {
            return FBSDKApplicationDelegate.sharedInstance().application(application, openURL: url, sourceApplication: sourceApplication, annotation: annotation)
        }
    }

    func handleFirebaseInviteDeeplink(url:NSURL) {
        //https://tqk28.app.goo.gl/?link=Referralcode&apn=com.certified.verityscanning
        
        //https://tqk28.app.goo.gl/?link=https://t.certified.bz/index.php&apn=com.certified.verityscanning&ibi=com.certified.verityscanning&isi=1124462403&ius=dynamicScheme&refCode=123456
        
        //Key = refCode
        if let referralCode = url.getQueryItemValueForKey("refCode") { //link
            NSLog("\n-------------------- Referral Code = %@", referralCode)
            let currentState: Int = NSUserDefaults.standardUserDefaults().integerForKey(kUserState)
            if currentState == USER_STATE.NOT_LOGGED_IN.rawValue {
                CommonMethods.sharedInstance.referralCode = referralCode ?? ""
                //Navigate to register screen
                let splashCtrl = SplashCtrl(nibName: "SplashCtrl", bundle: nil)
                let loginCtrl = LoginCtrl(nibName: "LoginCtrl", bundle: nil)
                let registerCtrl = RegisterCtrl(nibName: "RegisterCtrl", bundle: nil)
                self.navCtrl?.setViewControllers([splashCtrl, loginCtrl, registerCtrl], animated: false)
            } else {
                NSLog("\n--------------------User logged in Referral Code = %@", referralCode)
            }
        }
    }
    
    //Firebase Dynamic Links
    @available(iOS 8.0, *)
    func application(application: UIApplication, continueUserActivity userActivity: NSUserActivity, restorationHandler: ([AnyObject]?) -> Void) -> Bool {
        
        guard let dynamiclinks = FIRDynamicLinks.dynamicLinks() else {
            return false
        }
        
        if let incomingURL = userActivity.webpageURL {
            let linkHandled = dynamiclinks.handleUniversalLink(incomingURL) { [weak self] (dynamiclink, error) in
                guard let strongSelf = self else { return }
                
                if let dyLink = dynamiclink, let _ = dynamiclink?.url {
                    if let url = dyLink.url {
                        NSLog("\n------------------Invite received continueUserActivity: %@ ", url)
                        strongSelf.handleFirebaseInviteDeeplink(url)
                    }
                }
            }
            return linkHandled
        }
        return false
    }
    
    func applicationDidBecomeActive(application: UIApplication) {
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        FBSDKAppEvents.activateApp()
        connectToFcm()
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the applica tion and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        //FIRMessaging.messaging().disconnect()
        //print("Disconnected from FCM.")
    }

    
    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        let currentState: Int = NSUserDefaults.standardUserDefaults().integerForKey(kUserState)
        if currentState ==  USER_STATE.LOGGED_IN.rawValue {
            connectUserWithQuickbloxChat()
        }
    }
    
    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    //MARK:- TabbarController Methods
    
    func setTabbarController(animated:Bool) {
        
        let homeViewCtrl = HomeCtrl(nibName: "HomeCtrl", bundle: nil)
        homeNavController = UINavigationController(rootViewController: homeViewCtrl)
        homeNavController?.navigationBarHidden = true
        
        let searchViewCtrl = SearchCtrl(nibName: "SearchCtrl", bundle: nil)
        searchNavController = UINavigationController(rootViewController: searchViewCtrl)
        searchNavController?.navigationBarHidden = true
        
        let scanViewCtrl = ScanCtrl(nibName: "ScanCtrl", bundle: nil)
        scanNavController = UINavigationController(rootViewController: scanViewCtrl)
        scanNavController?.navigationBarHidden = true
        
        let chatViewCtrl = AllUserListCtrl(nibName: "AllUserListCtrl", bundle: nil)
        chatNavController = UINavigationController(rootViewController: chatViewCtrl)
        chatNavController?.navigationBarHidden = true
        
        let settingViewCtrl = SettingsCtrl(nibName: "SettingsCtrl", bundle: nil)
        settingNavController = UINavigationController(rootViewController: settingViewCtrl)
        settingNavController?.navigationBarHidden = true
        
        tabController = UITabBarController()
        tabController!.viewControllers = [homeNavController!, searchNavController!, scanNavController!, chatNavController!, settingNavController!]
        
        let item1:UITabBarItem = UITabBarItem(title: "", image: UIImage(named: "homeFooter")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal), selectedImage: UIImage(named: "homeFooter")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal))
        
        item1.imageInsets = UIEdgeInsets(top: 4, left: 0, bottom: -4, right: 0)
        
        let item2:UITabBarItem = UITabBarItem(title: "", image: UIImage(named: "searchFooter")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal), selectedImage: UIImage(named: "searchFooter")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal))
        
        item2.imageInsets = UIEdgeInsets(top: 4, left: 0, bottom: -4, right: 0)
        
        let item3:UITabBarItem = UITabBarItem(title: "", image: UIImage(named: "barcodeFooter")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal), selectedImage: UIImage(named: "barcodeFooter")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal))
        
        item3.imageInsets = UIEdgeInsets(top: 4, left: 0, bottom: -4, right: 0)
        
        let item4:UITabBarItem = UITabBarItem(title: "", image: UIImage(named: "chatFooter")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal), selectedImage: UIImage(named: "chatFooter")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal))
        
        item4.imageInsets = UIEdgeInsets(top: 4, left: 0, bottom: -4, right: 0)
        
        let item5:UITabBarItem = UITabBarItem(title: "", image: UIImage(named: "settingFooter")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal), selectedImage: UIImage(named: "settingFooter")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal))
        
        item5.imageInsets = UIEdgeInsets(top: 4, left: 0, bottom: -4, right: 0)
        
        UITabBar.appearance().translucent             = false
        UITabBar.appearance().selectionIndicatorImage = nil
        UITabBar.appearance().barTintColor            = TAB_BLUE_COLOR
        UITabBar.appearance().shadowImage             = UIImage()
        UITabBar.appearance().backgroundImage         = UIImage()
        
        //print(tabController?.tabBar.frame.size.height)
        
        homeViewCtrl.tabBarItem    = item1
        searchViewCtrl.tabBarItem  = item2
        scanViewCtrl.tabBarItem    = item3
        chatViewCtrl.tabBarItem    = item4
        settingViewCtrl.tabBarItem = item5
        
    }
    
    // MARK: - Quick Blox Login
    
    func connectUserWithQuickblox() {
        UIApplication.sharedApplication().applicationIconBadgeNumber = 0
        if (CommonMethods.sharedInstance.isConnected) {
            let currentState: Int = NSUserDefaults.standardUserDefaults().integerForKey(kUserState)
            if currentState ==  USER_STATE.LOGGED_IN.rawValue {
                let userEmail = (CommonMethods.sharedInstance.userModel?.email)!
                print(userEmail)
                print("User Email", userEmail)
                print("User Password", KQB_USER_DEFAULT_PASSWORD)
                
                QBRequest.logInWithUserLogin(userEmail, password: KQB_USER_DEFAULT_PASSWORD, successBlock: { (response, loginUserDetail) in
                    print(response.success)
                    print("login sccess")
                    print(loginUserDetail?.login);
                    loginUserDetail?.password = KQB_USER_DEFAULT_PASSWORD
                    CommonMethods.sharedInstance.qb_user = loginUserDetail!
                    QBChat.instance().connectWithUser(loginUserDetail!, completion: { (error) in
                        if (error != nil) {
                            print("Audio/Video login Fail")
                            print(error?.localizedDescription)
                        } else {
                            print("Audio/Video login Success")
                            print(loginUserDetail?.login);
                            print(loginUserDetail?.fullName);
                            CommonMethods.sharedInstance.qb_user = loginUserDetail!
                        }
                    })
                    }, errorBlock: { (error) in
                        print(error.error.debugDescription)
                        print("error in quickblox connection.")
                })
            } else {}
        }
    }
    
    func connectUserWithQuickbloxChat() {
        if (CommonMethods.sharedInstance.isConnected) {
            let user = QBUUser()
            user.login = (CommonMethods.sharedInstance.userModel?.email)!
            user.password = KQB_USER_DEFAULT_PASSWORD
            ServicesManager.instance().logInWithUser(user, completion: { (success, errorMessage) in
                CommonMethods.sharedInstance.hideHud()
                if success {
                    print("Chat login Success")
                    self.registerForRemoteNotificationChat()
                } else {
                    print("Chat login Fail")
                    print(errorMessage!)
                }
            })
        }
    }
    
    func connectUserWhenForgroungMode() {
        if (CommonMethods.sharedInstance.isConnected) {
            let user = QBUUser()
            user.login = (CommonMethods.sharedInstance.userModel?.email)!
            user.password = KQB_USER_DEFAULT_PASSWORD
            QBChat.instance().connectWithUser(user, completion: { (error) in
                print(error?.localizedDescription)
            })
        }
    }
    
    func registerForRemoteNotificationChat() {
        // Register for push in iOS 8
        if #available(iOS 8.0, *) {
            let settings = UIUserNotificationSettings(forTypes: [UIUserNotificationType.Alert, UIUserNotificationType.Badge, UIUserNotificationType.Sound], categories: nil)
            UIApplication.sharedApplication().registerUserNotificationSettings(settings)
            UIApplication.sharedApplication().registerForRemoteNotifications()
        } else {
            // Register for push in iOS 7
            UIApplication.sharedApplication().registerForRemoteNotificationTypes([UIRemoteNotificationType.Badge, UIRemoteNotificationType.Sound, UIRemoteNotificationType.Alert])
        }
    }
    
    // MARK: - NotificationServiceDelegate protocol
    
    func notificationServiceDidStartLoadingDialogFromServer() {}
    
    func notificationServiceDidFinishLoadingDialogFromServer() {}
    
    func notificationServiceDidSucceedFetchingDialog(chatDialog: QBChatDialog!) {
        print("\n-----------------------------Open Chat controller")
    }
    
    func notificationServiceDidFailFetchingDialog() {}
}

