//
//  Place.swift
//  Verityscanner
//
//  Created by Hardik Dineshchandra Mistry on 29/08/16.
//  Copyright © 2016 certified. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation

class Place: NSObject {
    
    let ICON                  = "icon"
    let NAME                  = "name"
    let LAT                   = "lat"
    let LONG                  = "lng"
    
    var icon = ""
    var name = ""
    var place = ""
    var lat:Double = 0.0
    var lng:Double = 0.0
    var placeLocation:CLLocation!
    var distance: Double!

    func jsonObject() -> NSMutableDictionary {
        let dictionary:NSMutableDictionary = NSMutableDictionary()
        dictionary.setValue(self.name, forKey: NAME)
        dictionary.setValue("\(self.distance) meters", forKey: "distance")
        dictionary.setValue(self.lat, forKey: LAT)
        dictionary.setValue(self.lng, forKey: LONG)
        return dictionary
    }
    
    func initWithDictionary(dictionary:[String:JSON]) -> Place {
        
        if let item = dictionary[ICON]?.string {
            self.icon = item
        }
        
        if let item = dictionary[NAME]?.string {
            self.name = item
        }
        
        if dictionary["geometry"]!["location"]["lat"].exists() {
            self.lat = dictionary["geometry"]!["location"]["lat"].doubleValue
        }
        
        if dictionary["geometry"]!["location"]["lng"].exists() {
            self.lng = dictionary["geometry"]!["location"]["lng"].doubleValue
        }
        
        if self.lat != 0.0 && self.lng != 0.0 {
            self.placeLocation = CLLocation.init(latitude: self.lat, longitude: self.lng)
        }
        
        return self
    }
    
    //Function to calculate the distance from given location.
    func calculateDistance(fromLocation: CLLocation?) {
        let location = CLLocation(latitude: self.lat, longitude: self.lng)
        self.distance = location.distanceFromLocation(fromLocation!)
    }

}
