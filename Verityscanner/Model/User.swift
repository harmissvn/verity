//
//  User.swift
//  SchoolEasy
//
//  Created by W@rrior on 18/11/15.
//  Copyright © 2015 Organization. All rights reserved.
//

import UIKit
import SwiftyJSON

class User: NSObject, NSCoding  {
 
    let USER_ID             = "userId"
    let NAME                = "name"
    let BIRTH_DAY           = "birthDay"
    let MOBILE_NO           = "mobileNumber"
    let EMAIL               = "email"
    let PROFILE_IMAGE       = "profileImage"
    let FIREBASE_ID         = "firebaseId"
    let SOCIAL_TYPE         = "socialType"
    let REFERAL_CODE        = "referalCode"
    
    var userId       : String = ""
    var name         : String = ""
    var birthDay     : String = ""
    var mobileNumber : String = ""
    var email        : String = ""
    var profileImage : String = ""
    var firebaseId   : String = ""
    var socialType   : String = ""
    var referalCode  : String = ""
    
    func initWithDictionary(dictionary:[String : JSON]) -> User {
        
        if let item = dictionary[REFERAL_CODE]?.string {
            self.referalCode = item
        }
        
        if let item = dictionary[FIREBASE_ID]?.string {
            self.firebaseId = item
        }
        
        if let item = dictionary[SOCIAL_TYPE]?.string {
            self.socialType = item
        }
        
        if let item = dictionary[USER_ID]?.string {
            self.userId = item
        }
        
        if let item = dictionary[NAME]?.string {
            self.name = item
        }
        
        if let item = dictionary[BIRTH_DAY]?.string {
            self.birthDay = item
        }
        
        if let item = dictionary[MOBILE_NO]?.string {
            self.mobileNumber = item
        }
        
        if let item = dictionary[EMAIL]?.string {
            self.email = item
        }
        
        if let item = dictionary[PROFILE_IMAGE]?.string {
            self.profileImage = item
        }
        return self
    }

    override init() {}
    
    // MARK: NSCoding
    
    required init(coder aDecoder: NSCoder) {
        if let item = aDecoder.decodeObjectForKey(REFERAL_CODE) as? String {
            self.referalCode = item
        }
        
        if let item = aDecoder.decodeObjectForKey(FIREBASE_ID) as? String {
            self.firebaseId = item
        }
        
        if let item = aDecoder.decodeObjectForKey(SOCIAL_TYPE) as? String {
            self.socialType = item
        }
        
        if let item = aDecoder.decodeObjectForKey(USER_ID) as? String {
            self.userId = item
        }
        
        if let item = aDecoder.decodeObjectForKey(NAME) as? String {
            self.name = item
        }
        
        if let item = aDecoder.decodeObjectForKey(BIRTH_DAY) as? String {
            self.birthDay = item
        }
        
        if let item = aDecoder.decodeObjectForKey(EMAIL) as? String {
            self.email = item
        }
        
        if let item = aDecoder.decodeObjectForKey(MOBILE_NO) as? String {
            self.mobileNumber = item
        }
        
        if let item = aDecoder.decodeObjectForKey(PROFILE_IMAGE) as? String {
            self.profileImage = item
        }
    }
    
    func encodeWithCoder(coder: NSCoder) {
        coder.encodeObject(self.referalCode, forKey: REFERAL_CODE)
        coder.encodeObject(self.firebaseId, forKey: FIREBASE_ID)
        coder.encodeObject(self.socialType, forKey: SOCIAL_TYPE)
        coder.encodeObject(self.userId, forKey: USER_ID)
        coder.encodeObject(self.name, forKey: NAME)
        coder.encodeObject(self.birthDay, forKey: BIRTH_DAY)
        coder.encodeObject(self.email, forKey: EMAIL)
        coder.encodeObject(self.mobileNumber, forKey: MOBILE_NO)
        coder.encodeObject(self.profileImage, forKey: PROFILE_IMAGE)
    }
}
