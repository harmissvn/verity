//
//  Section.swift
//  SchoolEasy
//
//  Created by W@rrior on 07/12/15.
//  Copyright © 2015 Organization. All rights reserved.
//

import UIKit
import SwiftyJSON

class Section: NSObject {
   
    var title: String = ""
    var index: UInt = 0
    var icon: String = ""
    var isShow: Bool = false
    var rows = [Row]()

    func initWithDictionary(dictionary:[String : AnyObject]) -> Section {
        
        if let valSub = dictionary["isShow"] as? Bool {
            self.isShow = valSub
        }
        
        if let ph = dictionary["title"] as? String {
            self.title = ph
        }
        
        if let ph = dictionary["icon"] as? String {
            self.icon = ph
        }
        
        if let ind = dictionary["index"] as? UInt {
            self.index = ind
        }
        
        if let jsonArr = dictionary["rows"] {
            for i in 0 ..< jsonArr.count {
                let profile = jsonArr.objectAtIndex(i)
                let user = Row().initWithDictionary(profile as! [String : AnyObject])
                rows.append(user)
            }
        }
        return self
    }

}
