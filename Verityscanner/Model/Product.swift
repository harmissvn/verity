//
//  Product.swift
//  Verityscanner
//
//  Created by Hardik Dineshchandra Mistry on 01/09/16.
//  Copyright © 2016 certified. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation

class Product: NSObject {
    /*
     {
     "productImage" : "http:\/\/t.certified.bz\/media\/com_mtree\/images\/listings\/m\/396.jpg",
     "thumbImage" : "http:\/\/t.certified.bz\/media\/com_mtree\/images\/listings\/s\/396.jpg",
     "rateUserCount" : "1",
     "review" : {
     "linkId" : "373",
     "revTitle" : "test",
     "userId" : "377",
     "revText" : "Test",
     "revDate" : "2017-02-25 10:18:49",
     "revId" : "19"
     },
     "category" : "BA - Product of USA",
     "companyUniqueId" : "FL00AA.0001.TEST",
     "isreview" : "1",
     "IsRate" : "0",
     "productName" : "Test Subject Data Collection Fish Fresh Yellow Tail",
     "address" : "980 north federal highway,boca raton,fl,United States,33431",
     "productDesc" : "FL00AA.0001.TEST,QR CODE,02\/22\/2016,08:29:04,2621.57777N,08004.98930WFL00AA.0001.TEST,QR CODE,02\/22\/2016,08:29:09,2621.57740N,08004.98707WFL00AA.0001.TEST,QR CODE,02\/22\/2016,08:29:14,2621.57975N,08004.98772WFL00AA.0001.TEST,QR CODE,02\/22\/2016,08:29:18,2621.58300N,08004.98807W",
     "lat" : "25.182573",
     "productDetails" : [
     {
     "value" : "Test Subject Data Collection Fish Fresh Yellow Tail",
     "placeholder" : "Name",
     "type" : "0"
     },
     {
     "value" : "FL00AA.0001.TEST,QR CODE,02\/22\/2016,08:29:04,2621.57777N,08004.98930WFL00AA.0001.TEST,QR CODE,02\/22\/2016,08:29:09,2621.57740N,08004.98707WFL00AA.0001.TEST,QR CODE,02\/22\/2016,08:29:14,2621.57975N,08004.98772WFL00AA.0001.TEST,QR CODE,02\/22\/2016,08:29:18,2621.58300N,08004.98807W",
     "placeholder" : "Info",
     "type" : "0"
     },
     {
     "value" : "NA",
     "placeholder" : "UPC",
     "type" : "0"
     },
     {
     "value" : "980 north federal highway,boca raton,fl,United States,33431",
     "placeholder" : "Address",
     "type" : "0"
     },
     {
     "value" : "http:\/\/www.madeinusa.net",
     "placeholder" : "Website",
     "type" : "0"
     },
     {
     "value" : "FL00AA.0001.TEST",
     "placeholder" : "Company Unique ID",
     "type" : "0"
     },
     {
     "value" : "100% US Content",
     "placeholder" : "Seal Percentage",
     "type" : "0"
     },
     {
     "value" : "Captain Jack Certified Charters",
     "placeholder" : "Contact Person",
     "type" : "0"
     }
     ],
     "upcCode" : "NA",
     "lng" : "-80.093079",
     "productId" : "373",
     "website" : "http:\/\/www.madeinusa.net",
     "avgRating" : "4.000000",
     "sealPercentage" : "100% US Content"
     }
     */
    
    let IMAGE                 = "productImage"
    let THUMB                 = "thumbImage"
    let RATE_USER_COUNT       = "rateUserCount"
    let IS_REVIEW             = "isReview"
    let IS_RATE               = "isRate"
    let AVG_RATING            = "avgRating"
    let CATEGORY              = "category"
    let COMPANY_UNIQUE_ID     = "companyUniqueId"
    let PRODUCT_NAME          = "productName"
    let ADDRESS               = "address"
    let PRODUCT_DESC          = "productDesc"
    let LAT                   = "lat"
    let UPC                   = "upcCode"
    let LONG                  = "lng"
    let PRODUCT_ID            = "productId"
    let WEBSITE               = "website"
    let SEAL_PRECENT          = "sealPercentage"
    
    let PRODUCT_DETAILS       = "productDetails"
    let DIC_REVIEW            = "review"
    
    var productImage    = ""
    var thumbImage      = ""
    var category        = "NA"
    var companyUniqueId = ""
    var productName     = "NA"
    var address         = ""
    var productDesc     = ""
    var website         = ""
    var sealPercentage  = ""
    var productId       = ""
    var upcCode         = ""
    var rateUserCount   = ""
    var isReview        = ""
    var isRate          = ""
    var avgRating       = ""
    var intReviewHidden = 0
    var lat:Double      = 0.0
    var lng:Double      = 0.0
    
    var productDetails  = [ProductDetail]()
    var dicReview       = AllReview()
    
    var productLocation:CLLocation!
    
    func initWithDictionary(dictionary:[String:JSON]) -> Product {
        
        if let item = dictionary[RATE_USER_COUNT]?.string {
            self.rateUserCount = item
        }
        
        if let item = dictionary[IS_REVIEW]?.string {
            self.isReview = item
        }
        
        if let item = dictionary[IS_RATE]?.string {
            self.isRate = item
        }
        
        if let item = dictionary[AVG_RATING]?.doubleValue {
            self.avgRating = String(item)
        }
        
        if let item = dictionary[COMPANY_UNIQUE_ID]?.string {
            self.companyUniqueId = item
        }
        
        if let item = dictionary[ADDRESS]?.string {
            self.address = item
        }
        
        if let item = dictionary[ADDRESS]?.string {
            self.address = item
        }
        
        if let item = dictionary[PRODUCT_DESC]?.string {
            self.productDesc = item
        }
        
        if let item = dictionary[WEBSITE]?.string {
            self.website = item
        }
        
        if let item = dictionary[SEAL_PRECENT]?.string {
            self.sealPercentage = item
        }
        
        
        if let item = dictionary[PRODUCT_ID]?.string {
            self.productId = item
        }
        
        if let item = dictionary[PRODUCT_NAME]?.string {
            self.productName = item.trim
        }
        
        if let item = dictionary[CATEGORY]?.string {
            self.category = item
        }
        
        if let item = dictionary[UPC]?.string {
            self.upcCode = item
        }
        
        if let item = dictionary[LAT]?.string {
            self.lat = Double(item) ?? 0.0
        }
        
        if let item = dictionary[LONG]?.string {
            self.lng = Double(item) ?? 0.0
        }
        
        if self.lat != 0.0 && self.lng != 0.0 {
            self.productLocation = CLLocation.init(latitude: self.lat, longitude: self.lng)
        }
        
        if let item = dictionary[THUMB]?.string {
            self.thumbImage = item
        }
        
        if let item = dictionary[IMAGE]?.string {
            self.productImage = item
        }
        
        if let jsonArr = dictionary[PRODUCT_DETAILS]?.array {
            for i in 0 ..< jsonArr.count {
                let profile = jsonArr[i].dictionary
                let user = ProductDetail().initWithDictionary(profile!)
                productDetails.append(user)
            }
        }
        
        if let dicJson = dictionary[DIC_REVIEW]?.dictionary {
            self.intReviewHidden = 1
            self.dicReview  = AllReview().initWithDictionary(dicJson)
        } else {
            self.intReviewHidden = 0
        }
        
        return self
    }
    
    override func isEqual(object: AnyObject?) -> Bool {
        if let object = object as? Product {
            return self.productId == object.productId
        } else {
            return false
        }
    }
}
