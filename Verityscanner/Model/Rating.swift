//
//  Rating.swift
//  Verityscanner
//
//  Created by harmis on 27/02/17.
//  Copyright © 2017 certified. All rights reserved.
//

import UIKit
import SwiftyJSON

class Rating: NSObject {
    
    let ID           = "id"
    let RATING_VALUE = "ratingValue"
    
    var strId       = ""
    var ratingValue = ""
    
    func initWithDictionary(dictionary:[String : JSON]) -> Rating {
        
        if let item = dictionary[ID]?.string {
            self.strId = item
        }
        
        if let item = dictionary[RATING_VALUE]?.string {
            self.ratingValue = item
        }
        return self
    }
}
