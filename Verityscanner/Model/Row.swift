//
//  Row.swift
//  SchoolEasy
//
//  Created by W@rrior on 18/11/15.
//  Copyright © 2015 Organization. All rights reserved.
//

import UIKit

class Row: NSObject {
    
    var placeholder: String = ""
    var value      : String = ""
    var type       : String = ""
    var image      : String = ""
    
    var index      : Int  = 0
    var canEdit    : Bool = false
    var dataImage  : NSData!
    
    func initWithDictionary(dictionary:[String : AnyObject]) -> Row {
        
        if let data = dictionary["dataImage"] as? NSData {
            self.dataImage = data
        }

        
        if let ph = dictionary["placeholder"] as? String {
            self.placeholder = ph
        }
        
        if let img = dictionary["image"] as? String {
            self.image = img
        }
        
        if let val = dictionary["value"] as? String {
            self.value = val
        }
        
        if let valSub = dictionary["canEdit"] as? Bool {
            self.canEdit = valSub
        }
        
        if let ind = dictionary["index"] as? Int {
            self.index = ind
        }
        
        if let t = dictionary["type"] as? String {
            self.type = t
        }
        return self
    }
}
