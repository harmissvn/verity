//
//  AllReview.swift
//  Verityscanner
//
//  Created by harmis on 25/02/17.
//  Copyright © 2017 certified. All rights reserved.
//

import UIKit
import SwiftyJSON

class AllReview: NSObject {
    /*
     {
     "productId" : "373",
     "id" : "4",
     "reviewValue" : "4.000000",
     "reviewText" : "this is one of good product",
     "userName" : "harmistechnology",
     "reviewTitle" : "my review",
     "reviewDate" : "2017-02-22 06:25:50"
     }
     */
    
    let PRODUCT_ID    = "productId"
    let ID            = "id"
    let REVIEW_VALUE  = "reviewValue"
    let REVIEW_TEXT   = "reviewText"
    let USER_NAME     = "userName"
    let REVIEW_TITLE  = "reviewTitle"
    let REVIEW_DATE   = "reviewDate"
    
    var productId   : String = ""
    var id          : String = ""
    var reviewValue : String = ""
    var reviewText  : String = ""
    var userName    : String = ""
    var reviewTitle : String = ""
    var reviewDate  : String = ""
    
    func initWithDictionary(dictionary:[String : JSON]) -> AllReview {
       
        if let item = dictionary[PRODUCT_ID]?.string {
            self.productId = item
        }
        
        if let item = dictionary[ID]?.string {
            self.id = item
        }
        
        if let item = dictionary[REVIEW_VALUE]?.string {
            self.reviewValue = item
        }
        
        if let item = dictionary[REVIEW_TITLE]?.string {
            self.reviewTitle = item
        }
        
        if let item = dictionary[USER_NAME]?.string {
            self.userName = item
        }
        
        if let item = dictionary[REVIEW_TEXT]?.string {
            self.reviewText = item
        }
        
        if let item = dictionary[REVIEW_DATE]?.string {
            self.reviewDate = item
        }
        return self
    }
}
