//
//  USDA.swift
//  Verityscanner
//
//  Created by Chinkal on 5/19/17.
//  Copyright © 2017 certified. All rights reserved.
//

import UIKit
import SwiftyJSON

class USDA: NSObject {

    /*
     {
     "title" : "The Osso Good Co. Recalls Beef and Pork Products Produced Without Benefit of Inspection",
     "pubDate" : "2017-05-18T14:30:21-0500",
     "link" : "https:\/\/www.fsis.usda.gov\/wps\/wcm\/connect\/FSIS-Content\/internet\/main\/topics\/recalls-and-public-health-alerts\/recall-case-archive\/archive\/2017\/recall-051-2017-release",
     "description" : "The Osso Good Co., a San Rafael, Calif. establishment, is recalling approximately 1,210 pounds of beef and pork bone broth products that were produced without the benefit of federal inspection."
     },

     */
    
    let TITLE        = "title"
    let PUB_DATE     = "pubDate"
    let LINK         = "link"
    let DESCRIPTION  = "description"

    var title       : String = ""
    var pubDate     : String = ""
    var link        : String = ""
    var desc        : String = ""

    func initWithDictionary(dictionary:[String : JSON]) -> USDA {
        
        if let item = dictionary[TITLE]?.string {
            self.title = item
        }
        
        if let item = dictionary[PUB_DATE]?.string {
            self.pubDate = item
        }

        if let item = dictionary[LINK]?.string {
            self.link = item
        }

        if let item = dictionary[DESCRIPTION]?.string {
            self.desc = item
        }

        return self
    }

    
}
