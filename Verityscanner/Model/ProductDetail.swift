//
//  ProductDetail.swift
//  Verityscanner
//
//  Created by Hardik Dineshchandra Mistry on 14/09/16.
//  Copyright © 2016 certified. All rights reserved.
//

import UIKit
import SwiftyJSON
import CoreLocation

class ProductDetail: NSObject {
    
    let PLACEHOLDER           = "placeholder"
    let TYPE                  = "type"
    let VALUE                 = "value"
    let REDIRECT_URL          = "redirectUrl"
  
    var placeholder = ""
    var type = ""
    var value = ""
    var redirectUrl = ""
    
    func initWithDictionary(dictionary:[String:JSON]) -> ProductDetail {
        
        if let item = dictionary[PLACEHOLDER]?.string {
            self.placeholder = item
        }
        
        if let item = dictionary[TYPE]?.string {
            self.type = item.trim
        }
        
        if let item = dictionary[VALUE]?.string {
            self.value = item.trim
        }
        
        if let item = dictionary[REDIRECT_URL]?.string {
            self.redirectUrl = item
        }
        
        return self
    }
}
