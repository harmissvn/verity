//
//  UITableviewExtension.swift
//  Certified
//
//  Created by harmis on 02/07/16.
//  Copyright © 2016 harmis. All rights reserved.
//
import Foundation
import UIKit

extension UITableView
{
    func getTextFieldArray() -> [String]{
        var arrStrings = [String]()
        for i in 0...self.numberOfSections-1
        {
            for j in 0...self.numberOfRowsInSection(i)-1
            {
                if let cell = self.cellForRowAtIndexPath(NSIndexPath(forRow: j, inSection: i)) {
                    for view in cell.subviews{
                        if let view1 = view as? UITextField{
                            arrStrings.append((view1 as UITextField).text!)
                        }
                    }
                }
            }
        }
        return arrStrings
    }
    func register(str:String)
    {
        self.registerNib(UINib(nibName: str, bundle: nil), forCellReuseIdentifier: str)
    }
}

