//
//  UICollectionviewExtension.swift
//  Certified
//
//  Created by harmis on 06/07/16.
//  Copyright © 2016 harmis. All rights reserved.
//

import Foundation
import UIKit

extension UICollectionView
{
    func register(str:String)
    {
        self.registerNib(UINib(nibName: str, bundle: nil), forCellWithReuseIdentifier: str)
    }
}