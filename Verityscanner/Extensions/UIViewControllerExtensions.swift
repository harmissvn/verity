//
//  UIViewControllerExtensions.swift
//  Certified
//
//  Created by harmis on 02/07/16.
//  Copyright © 2016 harmis. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Alamofire

extension UIViewController{
    func getData(withUrl: String,withParameter: [String : AnyObject]?, completion: (result: JSON) -> Void){
        if(CommonMethods.sharedInstance.isConnected) {
            CommonMethods.sharedInstance.showHudWithTitle("Loading...")
            Alamofire.request(.POST, withUrl, parameters:withParameter , encoding:.JSON).validate().responseJSON { response in
                if let data = response.data {
                    print("Response data: \(NSString(data: data, encoding: NSUTF8StringEncoding)!)")
                    NSLog(String(data: data, encoding: NSUTF8StringEncoding)!)
                }
                CommonMethods.sharedInstance.hideHud()
                switch response.result {
                case .Success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        print("JSON: \(json)")
                        if (json["success"].numberValue == 1)
                        {
                            completion(result: json)
                        } else {
                            let message = json["message"].stringValue
                            CommonMethods.sharedInstance.showAlertWithMessage(message, withController: self)
                        }
                    }
                case .Failure(let error):
                    print(error)
                    CommonMethods.sharedInstance.showAlertWithMessage(error.localizedDescription, withController: self)

                    if let data = response.data {

                        print("Response data: \(NSString(data: data, encoding: NSUTF8StringEncoding)!)")
                    }
                }
            }
        } else {
            CommonMethods.sharedInstance.showAlertWithMessage(NO_INTERNET, withController: self)
        }
    }
    func getDataWithotLoading(withUrl: String,withParameter: [String : AnyObject]?, completion: (result: JSON) -> Void){
        if(CommonMethods.sharedInstance.isConnected) {
//            CommonMethods.sharedInstance.showHudWithTitle("Loading...")
            Alamofire.request(.POST, withUrl, parameters:withParameter , encoding:.JSON).validate().responseJSON { response in
                if let data = response.data {
                    print("Response data: \(NSString(data: data, encoding: NSUTF8StringEncoding)!)")
                    NSLog(String(data: data, encoding: NSUTF8StringEncoding)!)
                }
//                CommonMethods.sharedInstance.hideHud()
                switch response.result {
                case .Success:
                    if let value = response.result.value {
                        let json = JSON(value)
                        print("JSON: \(json)")
                        if (json["success"].numberValue == 1)
                        {
                            completion(result: json)
                        } else {
                            let message = json["message"].stringValue
                            CommonMethods.sharedInstance.showAlertWithMessage(message, withController: self)
                        }
                    }
                case .Failure(let error):
                    print(error)
                    if let data = response.data {
                        print("Response data: \(NSString(data: data, encoding: NSUTF8StringEncoding)!)")
                    }
                }
            }
        } else {
            CommonMethods.sharedInstance.showAlertWithMessage(NO_INTERNET, withController: self)
        }
    }
    func getDataGet(withUrl: String,withParameter: [String : AnyObject]?, completion: (result: JSON) -> Void) {
        if(CommonMethods.sharedInstance.isConnected) {
            CommonMethods.sharedInstance.showHudWithTitle("Loading...")
            Alamofire.request(.GET, withUrl, parameters:withParameter , encoding:.JSON).validate().responseJSON { response in
                if let data = response.data {
                    print("Response data: \(NSString(data: data, encoding: NSUTF8StringEncoding)!)")
                    NSLog(String(data: data, encoding: NSUTF8StringEncoding)!)
                }
                CommonMethods.sharedInstance.hideHud()
                switch response.result {
                case .Success:
                    let json = JSON(response.result.value!)
                    
                    completion(result: json)
                    
//                    if let value = response.result.value {
//                        completion(result: json)
//                        print("JSON: \(json)")
//                        //  self.showAlertWithMessage(json.string!)
                   // }
                case .Failure(let error):
                    print(error)
                    if let data = response.data {
                        print("Response data: \(NSString(data: data, encoding: NSUTF8StringEncoding)!)")
                        self.showAlertWithMessage(error.localizedDescription)
                        completion(result: nil)
                    }
                }
            }
        } else {
            CommonMethods.sharedInstance.showAlertWithMessage(NO_INTERNET, withController: self)
        }
    }
    
    
    func getDataWithMultiPart(withUrl: String,withParameter: [String : AnyObject]? , withImage:UIImage?, completion: (result: JSON) -> Void){
        
        if(CommonMethods.sharedInstance.isConnected) {
            CommonMethods.sharedInstance.showHudWithTitle("Loading...")
            
            Alamofire.upload(.POST, withUrl,
                headers: ["Authorization": "auth_token"],
                multipartFormData: { multipartFormData in
                    multipartFormData.appendBodyPart(data: NSData(), name: "file", fileName: "myImage.png", mimeType: "image/png")
                    for (key, value) in withParameter! {
                        multipartFormData.appendBodyPart(data: value.dataUsingEncoding(NSUTF8StringEncoding)!, name: key)
                    }
                }, // you can customise Threshold if you wish. This is the alamofire's default value
                encodingMemoryThreshold: Manager.MultipartFormDataEncodingMemoryThreshold,
                encodingCompletion: { encodingResult in
                    switch encodingResult {
                    case .Success(let upload, _, _):
                        upload.responseJSON { response in
                            
                            print(response)
                            if let data = response.data {
                                print("Response data: \(NSString(data: data, encoding: NSUTF8StringEncoding)!)")
                            }
                            
                            CommonMethods.sharedInstance.hideHud()
                            switch response.result{
                            case .Success:
                                if let value = response.result.value {
                                    let json = JSON(value)
                                    print("JSON: \(json)")
                                    if (json["success"].numberValue == 1) {
                                        completion(result: json)
                                    } else {
                                        let message = json["message"].stringValue
                                        CommonMethods.sharedInstance.showAlertWithMessage(message, withController: self)
                                    }
                                }
                            case .Failure(let error):
                                print(error)
                                if let data = response.data {
                                    print("Response data: \(NSString(data: data, encoding: NSUTF8StringEncoding)!)")
                                }
                            }
                        }
                    case .Failure(let encodingError):
                        print(encodingError)
                    }
            })
        } else {
            CommonMethods.sharedInstance.showAlertWithMessage(NO_INTERNET, withController: self)
        }
    }
    func pushController<T:UIViewController>(classType : T){
    self.navigationController?.pushViewController(T(nibName:String(T) , bundle: nil), animated: true)
    }
    @IBAction func btnbackClicked(){    
        self.navigationController?.popViewControllerAnimated(true)
        if self is SearchCtrl{
            if (self as! SearchCtrl).searchText != nil{
            
                (self as! SearchCtrl).completionHandler!()

            }
        }
    }
   
    func showAlertWithMessage(message: String){
            let  alertController = UIAlertController(title: APP_NAME, message: message, preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .Default, handler: { (action: UIAlertAction!) in
                print("Handle Ok logic here")
            }))
            self.presentViewController(alertController, animated: true, completion: nil)
        
    }
}
extension JSON{
    
    func saveJson(){
        
       let data = try? self.rawData()
        
        do {
            let jsonArray = try NSJSONSerialization.JSONObjectWithData(data!, options:[])
            NSUserDefaults.standardUserDefaults().setValue(jsonArray, forKey: "Mydata")
            
            print("Array: \(jsonArray)")
        }
        catch {
            print("Error: \(error)")
        }
    }
    
   public static func getJson() -> AnyObject?{
        let data = NSUserDefaults.standardUserDefaults().valueForKey("Mydata")
        return data
    }
}
