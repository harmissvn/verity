//
//  UITextFieldExtension.swift
//  Certified
//
//  Created by harmis on 02/07/16.
//  Copyright © 2016 harmis. All rights reserved.
//

import Foundation
import UIKit

extension UITextField{
    func addLeftPadding(){
        self.clipsToBounds = true
        self.leftViewMode = .Always
        self.leftView = UIView(frame:  CGRectMake(0, 0, 10, 10))
    }
}