//
//  StringExtension.swift
//  Certified
//
//  Created by harmis on 05/07/16.
//  Copyright © 2016 harmis. All rights reserved.
//

import Foundation
import MapKit

extension String {
    var isEmail: Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .CaseInsensitive)
            return regex.firstMatchInString(self, options: NSMatchingOptions(rawValue: 0), range: NSMakeRange(0, self.characters.count)) != nil
        } catch {
            return false
        }
    }
}

extension String
{
    typealias GetLocationFromString = (CLLocationCoordinate2D?) -> ()
    func getLocationFromAddressString(completion : GetLocationFromString){
        let geocoder: CLGeocoder = CLGeocoder()
        geocoder.geocodeAddressString(self,completionHandler: {(placemarks: [CLPlacemark]?, error: NSError?) -> Void in
            if (placemarks?.count > 0) {
                let topResult: CLPlacemark = (placemarks?[0])!
                let placemark: MKPlacemark = MKPlacemark(placemark: topResult)
                completion((placemark.region as? CLCircularRegion)?.center)
            }
        })
    }
}