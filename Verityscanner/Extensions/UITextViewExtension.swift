
//  UITextViewExtension.swift
//  Certified
//
//  Created by harmis on 08/07/16.
//  Copyright © 2016 harmis. All rights reserved.
//

import Foundation
import UIKit
import ObjectiveC
private var xoAssociationKey: UInt8 = 0
extension UITextView {
    var placeholderLabel: UILabel! {
        get {
            return objc_getAssociatedObject(self, &xoAssociationKey) as? UILabel
        }
        set(newValue) {
            objc_setAssociatedObject(self, &xoAssociationKey, newValue, objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
}
extension UITextView : UITextViewDelegate{
    
    func setPlaceHolder(strPlaceHolder : String){
        self.delegate = self
        self.placeholderLabel = UILabel()
        placeholderLabel.text = " " + strPlaceHolder
        placeholderLabel.font = self.font!
        placeholderLabel.sizeToFit()
        self.addSubview(placeholderLabel)
        placeholderLabel.frame.origin = CGPointMake(5, self.font!.pointSize / 2)
        placeholderLabel.textColor = self.textColor
        placeholderLabel.hidden = !self.text.isEmpty
    }
    public func textViewDidChange(textView: UITextView) {
        print(self.text.isEmpty)
        self.placeholderLabel.hidden = !self.text.isEmpty
    }
}